---
title: "[ Task ] Netplan 3 - NAT Network"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...


# Setup

In your VirtualBox (*aka* Hypervisor), create a Nat Network with the next requisites:

* Network : 192.168.13.0/28
* Enable DHCP Server.

In your VM with Oracular, set the network adapters with the next configuration:

| Adapter | Connection |
|---------|------------|
| Adapter 1    | Nat Network| 
| Adapter 2    | Nat Network|
| Adapter 3    | Bridge Network|

Only these adapters must be configured.

# Task 1

Copy the previous files at your `netplan` folder in a safe place (from 
the previous tasks).

Now `netplan` folder, create **three files** (once for adapter) in order 
to configure the adapters with the next configuration scenario:

## Adapter 1

* Filename: device.yaml
* Static IP : 192.168.13.13
* Netmask: 28

## Adapter 2

* Filename: device.yaml
* DHCP 

## Adapter 3

* Filename: device.yaml
* Static IP: Your Host IP + 30.
* Netmask : Your Host Mask.
* Gateway: Your Host Gateway.

# Task 2

Create a Shell Script that using `tar` command, create a *tarball* 
of the `netplan/` folder in `gzip` format and store the configuration
files under `/root/netplan-baks/*`. 

The netplan tarball must be named:

* `netplan-config.YYYYMMDD-HHMM.tar.gz` 

The compressed file must include **only** the `yaml` files, not the 
folder structure.

## Useful commands

* `date`
* `tar`

 



















