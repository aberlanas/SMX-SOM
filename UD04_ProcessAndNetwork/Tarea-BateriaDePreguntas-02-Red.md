---
title: "Bateria de preguntas: Red"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Unit 04
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

# Calentamiento y Operaciones Básicas

Como ya sabéis la orden `ip a`, nos muestra información acerca de la configuración
y estado de las diferentes tarjetas de Red del equipo en el que ejecutamos el comando.

Vamos a hacer algunas operaciones básicas sobre la Red que seguro nos serán útiles a lo
largo de los días por venir.

# Tarea 01

Crea un Script en BASH que obtenga en una variable *sólo* la IP de la primera tarjeta de red física
del equipo y muéstrala por pantalla.

# Tarea 02

Amplia el funcionamiento del script anterior realizando las siguientes comprobaciones:

- Si no tiene IP asignada que muestre un mensaje : "IP UNAVAILABLE".

# Tarea 03

Amplia el funcionamiento del script anterior realizando las siguientes comprobaciones:

- Si no tiene IP asignada que muestre un mensaje : "IP UNAVAILABLE".

# Tarea 04

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY no sea '?' y cuyo PID no tenga un 4 o un 6.

# Tarea 05

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID no tenga un 1 ni un 2.

# Tarea 06

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID sea impar.

# Tarea 07

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID sea par.

# Tarea 08

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's', cuyo PID sea impar y cuyo COMMAND tenga una ruta que contenga *al menos dos carpetas*.

# Tarea 09

Del listado de la salida de `ps -ex`, suma todo el tiempo que de todos los procesos y devuelvelo en HORAS:MINUTOS, en caso de superar las 24 horas que diga el número de días completos.

# Tarea 10

Del listado de la salida de `ps -ex`, cuenta cuantos procesos tienen la palabra 'xfce4' en su COMMAND.

# Tarea 11

Del listado de la salida de `ps -ex`, suma el tiempo en ejecución de los COMMANDS que tengan `snap` o `libexec` en su COMMAND. Devuelvelo en HORAS:MINUTOS, en caso de que superen las 24
horas, mostrar el número de días.

