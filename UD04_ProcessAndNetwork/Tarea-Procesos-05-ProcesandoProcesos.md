---
title: "[ Prácticas ] Procesando Procesos"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Unit 04
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

# Scripts sobre procesos

Vamos a realizar una serie de Scripts con algunas operaciones básicas
sobre los procesos.

## Script 01

Realiza un script que acepte como *único argumento* un nombre de binario (CMD). Muestra por pantalla la RAM consumida por el proceso(s), así como su PID. 
En caso de que no haya ningún proceso con ese binario como ejecutable, mostrar un mensaje de advertencia.

## Script 02

Realiza un script que acepte *sólo 2 argumentos*, el primer argumento será el número de procesos a mostrar y el segundo el porcentaje de RAM mínima que deben
estar ocupando esos procesos, y debéis ordenarlos por consumo de RAM en orden descendente.

Es decir, si se ejecuta el script de esta manera:

```shell
./script02-procesos.sh 3 5%
```

Debe mostrar los primeros tres procesos que cosumen al menos un 5% de la RAM si los ordenamos del que más consume al que menos.

\newpage
## Script 03

Realiza un script que acepte *sólo 2 argumentos*, ambos números enteros naturales comprendidos entre el 1000 y el 99981, muestra todos los procesos que se estén ejecutando
en el equipo cuyos PIDs estén en el rango dado.

Realiza todas las comprobaciones que se te ocurran sobre el rango (orden, máximos, mínimos,...).

## Script 04

Realiza un script que acepte *1 argumento* que debe ser un número, muestra el proceso cuyo PID sea el primero a partir del número dado (él mismo se cuenta).

\newpage
## Script 05

Realiza un script que acepte *1 argumento*, que debe ser o "primo" o "absoluto", en caso de que sea primo, que muestre todos los PIDS y los CMDs de los procesos cuyo PID sea un número primo.
Si es absoluto, debe mostrar aquellos procesos cuyos números del PID sumen más de 15 al sumarse entre ellos.

Por ejemplo:

En esta ejecución:

![ps -aux](./imgs/procesos_01.png)\

Debería dar esto:

```shell
./script05-procesos.sh primo
1 /sbin/init splash
2 [kthreadd]
3 [rcu_gp]
5 [netns]
7 [kworker/0:0H-events_highpri]
11 [rcu_tasks_rude_]
```

\newpage
## Script 06

Realiza un script que acepte *2 argumentos*, el primero debe ser "descendente" o "ascendente" y el segundo un número de procesos. Muestra ese número de procesos ordenados de manera ascende
o descendente por su PID.


