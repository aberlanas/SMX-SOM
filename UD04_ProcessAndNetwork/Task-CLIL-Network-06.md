---
title: "[ Task ] Netplan 2"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

# Introduction


# Netparameters 

| Parameter | Function |
|-----------|----------|
|  IP       | Uniq Identifier in their network|
|  MASK     | Size of the network |
|  GATEWAY  | IP of the Machine that know how to exit|
|  DNS SERVER| IP of the Name-Address translator |

# Example with routes 


```yaml

network:
  version: 2
  renderer: NetworkManager
  ethernets:
    enp0s3:
      addresses:
        - 192.168.5.110/23
      routes: 
        - to: default 
          via: 192.168.4.254
      nameservers:
        addresses: [1.1.1.1, 1.0.0.1]

```

# Task Memorize 8 DNS Servers

Search in internet 8 DNS Servers that complains your mindset and moral values and 
memorize them.

During the exam, your teacher will be ask you about the chosen eight.









