---
title: "[ Resources ] Netplan"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

# Introduction

The purpose of this document is present several resources to understand 
and manage the *netplan* rendered for Networking in GNU/LinuX machines.

# Basic Network Parameters

The basic network parameters for a OS are:

| Name | Purpose |
|------|---------|
| IP   | Unique Identfier in a network for a network card |
| Mask | Number of neighbours on the network|
| Gateway | IP of the machine that is connected to other networks (router). |
| DNS Servers | Domain name translators |

All of these parameters must be set in the OS in order to be connected
to the Internet or other external networks in a correct way.

When you are using *DHCP*, all of this parameters 
are given by the *dhcp-server*, this entity will be 
configured lately.

But it's possible to set all the parameters at the network card
in a *static* way.

# Netplan is our friend

In the task named :  `Task_netplan`, you can read a description and how
to set a simple static IP Configuration for a network card (with little 
errors, and warnings). 

Another useful link are:

* [ Como usar Netplan ](https://aprendolinux.com/como-usar-netplan/)
* [ Netplan from Ubuntu ](https://netplan.readthedocs.io/en/stable/)

Please, be careful when you create the files under `/etc/netplan/` folder.

And remember, if the permissions are *too open*, you can fix this situation
applying the `400` permissions:

```shell
sudo chmod 400 /etc/netplan/*.yaml
```

















