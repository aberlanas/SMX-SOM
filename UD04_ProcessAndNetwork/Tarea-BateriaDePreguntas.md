---
title: "Bateria de preguntas"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Unit 04
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

# Calentamiento y Operaciones Básicas

Como ya sabéis la orden `ps -ex`, nos muestra todos los procesos que se están ejecutando en la máquina.
Esta orden la podemos enviar a un bucle `while` de tal manera que podemos ir procesando en vez de toda la salida
a la vez, podemos ir procesando cada línea.

```shell
#!/bin/bash

ps -ex | while read linea; do
	echo " * Estamos procesando esta linea : $linea"
done


```

# Tarea 01

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY no sea '?'.

# Tarea 02

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY sea '?'.

# Tarea 03

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY no sea '?' y cuyo PID no tenga un 4.

# Tarea 04

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY no sea '?' y cuyo PID no tenga un 4 o un 6.

# Tarea 05

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID no tenga un 1 ni un 2.

# Tarea 06

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID sea impar.

# Tarea 07

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's' y cuyo PID sea par.

# Tarea 08

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's', cuyo PID sea impar y cuyo COMMAND tenga una ruta que contenga *al menos dos carpetas*.

# Tarea 09

Del listado de la salida de `ps -ex`, suma todo el tiempo que de todos los procesos y devuelvelo en HORAS:MINUTOS, en caso de superar las 24 horas que diga el número de días completos.

# Tarea 10

Del listado de la salida de `ps -ex`, cuenta cuantos procesos tienen la palabra 'xfce4' en su COMMAND.

# Tarea 11

Del listado de la salida de `ps -ex`, suma el tiempo en ejecución de los COMMANDS que tengan `snap` o `libexec` en su COMMAND. Devuelvelo en HORAS:MINUTOS, en caso de que superen las 24
horas, mostrar el número de días.

