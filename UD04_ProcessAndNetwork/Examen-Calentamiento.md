---
title: "[ Prácticas ] Calentamiento"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Unit 04
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

# Calentamiento y Operaciones Básicas

Como ya sabéis la orden `ps -ex`, nos muestra todos los procesos que se están ejecutando en la máquina.
Esta orden la podemos enviar a un bucle `while` de tal manera que podemos ir procesando en vez de toda la salida
a la vez, podemos ir procesando cada línea.

```shell
#!/bin/bash

ps -ex | while read linea; do
	echo " * Estamos procesando esta linea : $linea"
done


```

Usando los siguientes comandos dentro del bucle `while` que os he puesto, obtener en **diferentes variables** los diferentes campos que 
aparecen:

* PID
* TTY
* STAT
* TIME
* COMMAND

Ejemplo de ejecución:

Si sobre la ejecución de este `ps -ex` aplicamos nuestro bucle, debe salir así:

![ps -ex](imgs/calentamiento.png)\


```shell
PROCESO DEL LISTADO: 1
PID : 1741
TTY: ?
STAT: Ss
TIME: 0:00
COMMAND: /lib/systemd/systemd --user.....
```

\newpage
# Tarea 01

Haz las operaciones necesarias para que **cada uno** de los procesos aparezca como en el ejemplo. Incrementando en 1 cada vez que procesamos un proceso del listado:

```shell
PROCESO DEL LISTADO 1
....
PROCESO DEL LISTADO 2
....
PROCESO DEL LISTADO 34
```

# Tarea 02

Prepara comparaciones con IF para cada uno de los campos que serán utilizadas en el examen.

Ejemplos:

* Mostrar los procesos cuyo PID < 200.
* Mostrar los procesos cuyo STAT no sea "Ss".
* Mostrar los procesos cuyo COMMAND comienze por `/`.

# Tarea 03

Realizar Shell Script que devuelva el PID y COMMAND de aquellos procesos cuyo PID >= 1500 y <= 2200 y cuyo tiempo de ejecución sea un número PAR de segundos.

# Tarea 04

Realizar un Shell Script que devuelva el PID y COMMAND de aquellos procesos cuyo PID >= 3000, cuyo estado sea diferente de "sS" y cuyo COMMAND contenga la ruta '/bin'.
