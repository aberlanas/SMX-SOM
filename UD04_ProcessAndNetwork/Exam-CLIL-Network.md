---
title: "[ Exam ] Net - Simple Stats"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...


# Exam

Please read all the exam before start.

We have a problem with a machine that has **TWO** ethernets cards (and loopback). We must configure the first device as connected to NAT and the second one to Bridge Adapter.

Using your VM (Cable connected checkbox), code a Shell Script that detects if a cable is 
connected or disconnected while the script is in execution, 
displaying a message with the device which state has changed.
The script must check the status each 2 seconds, and display the hour in HH:MM:SS format, and display the IP for the device, and the number of seconds that the device has an IP address (in accumulated).

Example:

```shell

smx@makina:~/SOM/UD04/$ ./exam.sh
* [11:15:30] The devices connected are: 
    - lo : 127.0.0.1 - 2s
    - eno1 : 10.0.2.15 - 2s
    - eno2 : 192.168.5.123 - 2s
* [11:15:32] The devices connected are: lo, eno1, eno2
    - lo : 127.0.0.1 - 4s
    - eno1 : 10.0.2.15 - 4s
    - eno2 : 192.168.5.123 - 4s
* [11:15:34] eno1 has disconnected!
* [11:15:34] The devices connected are: lo, eno2
    - lo : 127.0.0.1 - 6s
    - eno2 : 192.168.5.123 - 6s
* [11:15:36] eno2 has disconnected!
* [11:15:36] The devices connected are: lo
    - lo : 127.0.0.1 - 8s
* [11:15:38] eno1 has connected ^_^!
* [11:15:38] The devices connected are: lo, eno1
    - lo : 127.0.0.1 - 10s
    - eno1 : 10.0.2.15 - 6s
* [11:15:40] The devices connected are: lo, eno1
    - lo : 127.0.0.1 - 12s
    - eno1 : 10.0.2.15 - 8s
* [11:15:42] eno2 has connected ^_^!
* [11:15:42] The devices connected are: lo, eno1, eno2
    - lo : 127.0.0.1 - 14s
    - eno1 : 10.0.2.15 - 10s
    - eno2 : 192.168.5.123 - 8s
...

```

