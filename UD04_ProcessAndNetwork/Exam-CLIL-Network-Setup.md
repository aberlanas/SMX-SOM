---
title: "[ Exam ] Netplan and Backup Setup"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...


# Setup for the Exam

In your VirtualBox (*aka* Hypervisor), create a Nat Network with the next requisites:

* Network : 192.168.100.0/24
* Enable DHCP Server.

In your VM with Oracular, set the network adapters with the next configuration:

| Adapter      | Connection |
|--------------|------------|
| Adapter 1    | NAT| 
| Adapter 2    | Bridge Network|
| Adapter 3    | Internal Network (netsom)|
| Adapter 4    | Internal Network (netsom)|

All adapters must be configured **before** coding the script.

# Task 1

Copy the previous files at your `netplan` folder in a safe place (from 
the previous tasks).

Now in your `netplan` folder, create **four files** (once for adapter) in order 
to configure the adapters with the next configuration scenario:

## Adapter 1

* Filename: exam-01-NAME-OF-DEVICE.yaml
* DHCP

## Adapter 2

* Filename: exam-02-NAME-OF-DEVICE.yaml
* Static IP : Your Host IP + 40
* Netmask : Your Host Mask for this network.
* Gateway : Your Host Gateway for this network.
* DNS-Servers : 10.239.3.7, 10.239.3.8

## Adapter 3

* Filename: exam-03-NAME-OF-DEVICE-static.yaml
* Static IP: 192.168.44.43
* Netmask : 27

## Adapter 4

* Filename: exam-04-NAME-OF-DEVICE-static.yaml
* Static IP: 192.168.44.44
* Netmask : 27

## Aditional steps 

1. Create folder in the $HOME of your user named: `exam-netplan/`
2. Create folder inside named: `netplan-orig-files/`
3. Copy all the netplan (.yaml) files in this folder.
4. Meditate and be ready for the second part of the exam. If you have 
   enough time, you must practice with `tar` command, date and test arguments. 

 



















