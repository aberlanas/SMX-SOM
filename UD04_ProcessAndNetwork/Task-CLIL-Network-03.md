---
title: "[ Task ] Net - IPs and Pings"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

# Introduction

There are a several tasks which objective are presenting the students some useful concepts about
network commands, installation and configuration.

Could be a good idea storing the commands and its behaviour in paper or any kind of knowledge system.


\newpage
# Task 01 : Getting info about network devices

This is a list of commands (and their parameters) that could be used in order to extract network information in a GNU/LinuX environment. (Android too).

* `ip link show`
* `netstat -i`
* `tcpdump --list-interfaces`
* `ls -l /sys/class/net/`

Using your VM (Cable connected checkbox), code a Shell Script that detects if a cable is 
connected or disconnected while the script is in execution, 
displaying a message with the device which state has changed.
The script must check the status each 2 seconds, and display the hour in HH:MM:SS format.

Example:
```shell
smx@makina:~/SOM/UD04/$ ./task-check-cable.sh
* [18:27:31] The devices connected are: lo, eno1
* [18:27:33] The devices connected are: lo, eno1
* [18:27:35] eno1 has disconnected!
* [18:27:35] The devices connected are: lo
* [18:27:37] eno1 has connected ^_^!
...
```

\newpage
# Task 02 : Only My IP!

Code a Shell Script that checks if the argument given is a valid network adapter for this machine,
and if this is true, display only the IP (if any assigned) of this device.


Examples:

```
smx@makina:~/SOM/UD04/$ ./task-only-ip.sh eth1
 * [ Number of arguments ] : OK.
 * Device not found

```

```
smx@makina:~/SOM/UD04/$ ./task-only-ip.sh eno1
 * [ Number of arguments ] : OK.
 * Device eno1 found!
 * IP : 192.168.5.99

```


