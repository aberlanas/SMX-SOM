#!/bin/bash
#

if [ $# -ne 3 ]; then
	echo " * Three arguments are required"
	echo " $(basename $0) NETWORKIP /MASK IP"
	exit 1
fi

# $1 es una IP valida?
oct1=$(echo $1|cut -d "." -f1)
oct2=$(echo $1|cut -d "." -f2)
oct3=$(echo $1|cut -d "." -f3)
oct4=$(echo $1|cut -d "." -f4)

if [ $oct1 -ge 0 -a $oct1 -le 255 -a $oct2 -ge 0 -a $oct2 -le 255 -a $oct3 -ge 0 -a $oct3 -le 255 -a $oct4 -ge 0 -a $oct4 -le 255 ] ; then
	echo " * Updated "

	echo " * Testeamos $2 ahora"
	rc=0
	echo $2 | grep -q "^/" || rc=1

	if [ $rc -eq 1 ]; then
		echo " * Formato de mascara no valida"
		exit 1
	fi
	mask=$(echo $2| cut -d "/" -f2)
	if [ $mask -gt 32 -o $mask -le 0 ]; then
		echo " * Mascara demasiado grande o pequenya"
		exit 1
	fi	

	# Ahora cambiamos de base
	# todos los octetos.
	b_oct1=$(printf "%08d\n" $(echo "obase=2; $oct1"|bc))
	b_oct2=$(printf "%08d\n" $(echo "obase=2; $oct2"|bc))
	b_oct3=$(printf "%08d\n" $(echo "obase=2; $oct3"|bc))
	b_oct4=$(printf "%08d\n" $(echo "obase=2; $oct4"|bc))
	bIP="$b_oct1$b_oct2$b_oct3$b_oct4"
	echo $bIP

	let aux=0
	grep -o . <<< "$bIP" | while read letter; do
		if [ $aux -gt $mask ];then
			if [ $letter -ne 0 ]; then
				echo " Red Invalida "
				exit 1
			fi
		else
			# echo ${var:0:1}
		fi
		let aux=$aux+1
	done


else
	echo " * ERROR : $1 no es una IP valida"
fi

exit 0
