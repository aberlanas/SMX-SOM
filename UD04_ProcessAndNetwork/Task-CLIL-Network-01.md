---
title: "[ Task ] Network Scripts"
author: [Angel Berlanas Vicente]
date: 
subject: "Process and Network"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

# Task General Description

This is a task for testing Network Concepts.

# Task 01 : Simple Installation

In your Oracular VM, install the next packages: 

 * `tldr`
 * `nmap` 
 * `xcowsay` 
 * `sl`

# Task 02 : Is a valid network?

Code a Shell Script that accepts only 3 arguments. The first one, the network IP, the second one a valid mask in slash format (/24), the third one an IP that could be found in the network given. The script must check if the network IP is valid, if the mask is possible and if the host IP is valid and is inside the network. Use xcowsay to display the final message.  

Examples:

```shell
smx@machine:~/SOM/$./task-02-test-network.sh 192.168.13.0 /24 
 * [ Number of arguments ] : Not valid

smx@machine:~/SOM/$./task-02-test-network.sh 192.168.13.0 /24 192.168.13.125
 * [ Number of arguments ] : OK.
 * Network IP : Valid
 * Network Mask: Valid
 * Network Host : Valid

smx@machine:~/SOM/$./task-02-test-network.sh 192.168.13.1 /24 192.168.13.125
 * [ Number of arguments ] : OK.
 * Network IP : Not Valid
 * Network Mask: Valid
 * Network Host : Check Unavailable

```
\newpage



