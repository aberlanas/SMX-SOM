---
title: "[ Task ] NetChat "
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

# Introduction

Crear 2 scripts, uno cliente y otro server.

## redgato-server.sh

En este Script debe obtener la `IP` de la tarjeta de red conectada a la red del aula.
Preparar el directorio `$HOME/redgato-files/` y borrar su contenido si lo hubiera.

Y escuchar en el puerto indicado como `$1`.

A medida que se reciban ficheros guardarlos en `$HOME/redgato-files/YYYYMMDD-HHMMSS-file`.



## redgato-client.sh

En este Script conectarse a la `IP:PUERTO` indicado como `$1` y preguntar si quiere enviar
alguno de los ficheros situados en la carpeta: 

* `$HOME/redgato-torrent/` 

(crear algunos de ejemplo para poder enviarlos).

Por supuesto comprobar conectividad antes de realizar la conexión.

