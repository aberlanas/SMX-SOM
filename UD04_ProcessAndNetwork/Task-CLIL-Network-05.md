---
title: "[ Task ] Netplan"
author: [Angel Berlanas Vicente]
date: 
subject: "Network"
keywords: [IP, mask]
subtitle: Unit 04
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
...

\tableofcontents

# Introduction

Netplan Functionality and VirtualBox Exercises

In this task, we will explore the Netplan network configuration tool and practice some exercises using VirtualBox.

## Part 1: Netplan Functionality

Netplan is a network configuration utility used in Ubuntu and other Linux distributions. 
It provides a `YAML-based` configuration format to define network interfaces and their settings.

### How Netplan Works:

1. Netplan reads the YAML configuration files located in the `/etc/netplan/` directory.
2. It then translates the YAML configuration into the appropriate network configuration for the system, such as NetworkManager or systemd-networkd.
3. Netplan automatically applies the network configuration without the need for manual intervention.

### Key Netplan Commands:

* `netplan generate`: Generates the network configuration based on the YAML files.
* `netplan apply`: Applies the generated network configuration.
* `netplan try`: Temporarily applies the network configuration for testing purposes.
* `netplan --debug generate`: Generates the network configuration with debug output.
* `netplan --debug apply`: Applies the network configuration with debug output.

Example Netplan YAML Configuration:

```yaml

network:
  version: 2
  renderer: NetworkManager
  ethernets:
    enp0s3:
      dhcp4: true

```

This configuration sets the network interface enp0s3 to use DHCP for IPv4 addressing.

### Configure a Static IP Address with Netplan

1. Inside the VM, open the Netplan configuration file located at `/etc/netplan/01-netcfg.yaml` (warning with **identation**).
2. Modify the configuration to set a static IP address for our network interface. For example:

```yaml

network:
  version: 2
  renderer: NetworkManager
  #renderer: networkd
  ethernets:
    enp0s3:
      addresses:
        - 192.168.5.110/23
      gateway4: 192.168.4.254
      nameservers:
        addresses: [1.1.1.1, 1.0.0.1]

```

This IP only will work in our classroom.

3. Run `netplan generate` to generate the network configuration.
4. Run `netplan apply` to apply the new network settings.
5. Verify the new IP address using the `ip addr` command.


## Task 01

In your Oracular VM, enable 3 network cards, and set the next configuration:

| NetworkCard | Connection |
|:-------:|:----------:|
| 1       | Bridge Adapter|
| 2       | Internal network (intnet)|
| 3       | Internal network (intent)|

Create in the `/etc/netplan/` folder 3 files and using NetworkManager as a renderer
set the three devices in the next way:

### Card 1

Static IP = 100 + Your Host IP.
Mask and Gateway the same that your Host.
Nameservers: `10.239.3.7` and `10.239.3.8`

### Card 2

Static IP = `10.10.10.1`
Mask = /27

### Card 3

Static IP = `10.10.10.3`
Mask = /27

Test if the configuration is set using ping command, netcat,...











