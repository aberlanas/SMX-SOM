---
title: "Extraordinary Examination Exam"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
subtitle: Recover
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Instruciones (En Castellano).

- Leed cada uno de los ejercicios antes de comenzar a hacerlos.
- En caso de no obtener un 5 en cada uno de los ejercicios (puntúan sobre 10), se considerará
  que los objetivos mínimos del módulo no han sido superados.
- Buena suerte.

---

\newpage

# 1 EVA Task

**Creating files**

We have a problem with a The MonkeyIsland Database Filesystem, and our Boss *Lechuck* needs a *script* in order to stress the system, to make the Filesystem to failing.

The Database Filesystem uses three folders:

- `/monkeyisland/start`
- `/monkeyisland/working`
- `/monkeyisland/finish`

We must create a lot files with a custom *extension* in the previous folders, then the Database will process them and we can trace how the Database Filesystem will fail (this is abstracted on the exam).

---

**Your Mission**: Code a shell script that accepts only *4 arguments*, as follows:

- 1 Argument: The extension of the files (**without the dot**).
- 2 Argument: The number of files that will be created.
- 3 Argument: The last level folder of the Database Filesystem (`start`,`working` or `finish`).
- 4 Argument: DEBUG Level

Requeriments:

- The number of the arguments must be checked, if the number is not four, must shows a message with an example of correct usage.
- The *first* argument must not starts with a dot character (`.`).
- The *second* argument must be a integer between 1 and 100.
- The *third* argument must be a string and be `init`,`working` or `finish`.
- The *fourth* argument must be a integer (0 or 1).

If any of these requeriments is not accomplished, the script must fail and will display the correct usage.

## The Behaviour

When all the arguments has been tested, then the script must create the number of files given in the folder chosen. The name of each one must be `piratefile-XXX.EXTENSION` and must displays a message for each file created, for example:

If the DEBUG Level is set to "1", then the script must display a message foreach file created.

```shell
user@machine:~$./task-01.sh dat 3 working 1
* All the Arguments are OK.
* Creating /monkeyisland/working/piratefile-1.dat
* Creating /monkeyisland/working/piratefile-2.dat
* Creating /monkeyisland/working/piratefile-3.dat
```

> [SUBMIT] : The Shell Script named : `Eva01-Exam.sh`


---

## Rubric 

| Item | Weight |
|------|--------|
| Test the arguments correctly | 0-4 |
| Creating the files | 0-4 |
| Structure and Comments | 0-2 |

---

\newpage
# 2 EVA Task

# Melee Island People.

Contrary to popular belief, Monkey Island pirates don't usually allow outsiders to enter their domains. To avoid problems, brawls and fights in general, it has been decided that UNIX groups will be used for the organization of the different parts of the Island. As well as the assignment of pirates to one or more gangs (never more than 3), except for three famous pirates:

- Guybrush
- Swordmaster
- Lookoutmaster

Table of the Pirates of the Monkey Island

| User | Password | Group |
|------|----------|-------|
| guybrush | guybrush| pirates, rookies, candidates|
| lookoutmaster | lookoutmaster | pirates, lookout|
| troll | troll | pirates, candidates |

> [TIP] : This is a Tool Task, you need to create and assign the pirates **only to these listed** groups.

\newpage
# Task 00 : Creating the Frontiers (continuation)

![Melee Island Map](imgs/melee-island.png)\

Using a Shell Script, create in the folder `/TheMonkeyIsland/` the folders of the locations that appear in white color in the previous map. If the folder already exist, do nothing but assign the correct permissions.

In case of whitespaces, reemplace them with "-" and the **'s** from **SwordMaster's** could be safely ignored.

In this script, all the folders must belongs to `root:pirates` and the permisssions must be set to :

- Owner can do everything.
- Group can enter all the folders.
- Others can do nothing.

> [SUBMIT] : The Shell Script named : `Eva02-Exam-00-Permissions.sh`

\newpage
# Task 01 : A little more concretion

Code a shell Script that first of all, test all the Paths from the previous script, and restore the permissions to the
originals.

Then, asign the permissions to the users and groups in order to allow the next situations:

* The Lookoutmaster can read, write and enter at the Lookout Point. The other pirates has no permissions, but the other people can enter it.
* The Troll is the Master of the Bridge, he rules at this point, and no one other permission is allowed to nobody.
* Guybrush is the only one to read and enter the Beach.

![Melee Island Map](imgs/monkey-lookout.png)\

For each permission set, the Script must display a message with the permissions of the Path in Octal mode.

> [SUBMIT] : The Shell Script named : `Eva02-Exam-01-Permissions.sh` and a Screenshot of the execution of this Script in the Installed OS

---

## Rubric 

| Item | Weight |
|------|--------|
| Create users correctly | 0-2 |
| Permissions correct | 0-6 |
| Structure and Comments | 0-2 |

\newpage

# 3 EVA Task

Add two virtual-disks using pre-allocated-size (2GB) to the "IslandOS" machine (SATA). Create a partition in each one and format in ext4 and ntfs filesystems respectively.
Create the *mountpoints* at /srv/island/ with the names:

 - */srv/island/estcuatro/*  - (ext3)
 - */srv/island/enetefese/* - (ntfs)

Modify the `/etc/` **file** with the *filesystems-table*, in order to mount at booting process both disks at their mountpoints.

Create a ShellScript that displays the size of the folders that are the mountpoints from the three harddisks (obtaining info from the output of the `mount` command, in execution time, not hard-coded). The three disks are : **sda,sdb and sdc**.

Then the script must copy the `/etc/**file**` to a backup placed in /root/tablefs_YYYY_MM_DD_hh_mm.txt. For example:

```shell
user@machine:~$./Eva03-Exam.sh
* /dev/sda5 - / - 45G
* /dev/sda5 - /var/snap/firefox/common/host-hunspell - 200M
* /dev/sda1 - /boot/efi - 250M
* /dev/sdb1 - /srv/island/estcuatro/ - 2G
* /dev/sdc1 - /srv/island/enetefese/ - 2G
* Creating /root/tablefs_2024_06_10_09_33.txt
```

> [SUBMIT] : The Shell Script named : `Eva03-Exam.sh` and the file modified with the mountpoints.

## Rubric 

| Item | Weight |
|------|--------|
| Add the disks | 0-1 |
| Create and format partitions | 0-2 |
| Modify the configuration file | 0-1 |
| Script | 0-6 |
