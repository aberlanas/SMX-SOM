---
title: "30 Juegos de Scripts : Bucle While"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

\maketitle
\tableofcontents
\newpage

\newpage
# Bucles: While

El bucle `while` es un bucle que se diferencia al bucle `for` en el que no sabemos *a priori* cuantas 
veces vamos a ejecutar el bucle, sino que se ejecutará mientras que una determinada condición que se 
evalua al comienzo del bucle se cumpla (se *evalue a cierto*).

Es muy importante en este tipo de bucles que el código que pongamos dentro del mismo "camine" hacia que 
en algún momento esa condición deje de cumplirse y el bucle no se repita más. En caso contrario se transformaría
en un *bucle infinito*.

Por ejemplo, se puede utilizar este tipo de bucles para ir sumando valores a un número hasta que se 
alcance un determinado valor. 

```shell
#!/bin/bash

TOTAL=0

while [ $TOTAL -lt 100 ]; do
	echo " Dime un numero a sumarle a TOTAL : "
	read NUMERO
	let TOTAL=$TOTAL+$NUMERO
	echo " Total vale ahora : $TOTAL"
done

echo " Por fin! $TOTAL es mas grande que 100"

exit 0
```

---

\newpage

## buclewhile - Ejercicio 1 : Pequeñas modificaciones

Usando el código anterior, modíficalo para que cuando salga del bucle y muestre el mensaje, nos indique cuantas veces
ha tenido que ejecutarse el bucle.

Ejemplos:

```shell
usuario@maquina:~$./bucleswhile-ejer01.sh
 Dime un numero a sumarle a TOTAL : 
 101
  Total vale ahora : 101
  Por fin! $TOTAL es mas grande que 100
  El bucle se ha ejecutado 1 veces
```

```shell
usuario@maquina:~$./bucleswhile-ejer01.sh
 Dime un numero a sumarle a TOTAL : 
 10
  Total vale ahora : 10
 Dime un numero a sumarle a TOTAL : 
 10
  Total vale ahora : 20
 Dime un numero a sumarle a TOTAL : 
 30
  Total vale ahora : 50
 Dime un numero a sumarle a TOTAL : 
 40
  Total vale ahora : 90
 Dime un numero a sumarle a TOTAL : 
 10
  Total vale ahora : 100
  Por fin! $TOTAL es mas grande que 100
  El bucle se ha ejecutado 5 veces
```

\newpage
## Conceptos

Hemos visto ya en anteriores ejercicios el concepto de "contador", o de "acumulador", que se trata de una variable que vamos 
incrementando, decrementando, etc. dentro del bucle con el fin de llevar una cuenta o sencillamenta para saber
cuando acabar el bucle.

En caso de que el bucle no acabe nunca, se dice que el proceso ha entrado en un *bucle infinito*, que es un fragmento 
de código que se ejecuta de manera repetida sin parar jamás.

A veces es interesante utilizar este tipo de bucles para la resolución de algunos problemas.

Por ejemplo:

```shell
#!/bin/bash

while [ ! -e /tmp/ficheroQueMarcaElFin.tkn ]; do
	echo " Estamos esperando a que el fichero /tmp/ficheroQueMarcaElFin.tkn exista"
	# Esperamos 3 segundos
	sleep 3
done

exit 0
```

En este bucle el while se ejecuta mientras que el fichero **no exista**, y lo que hace es *dormir* durante 3 segundos para 
no bloquear el ordenador.

\newpage
## buclewhile - Ejercicios 2 y 3 : Onirim

En uno de los más famosos solitarios de cartas que podemos encontrar, el **Onirim**, debemos ir encontrando una serie
de puertas para escapar del mundo de los sueños. Estas puertas se encuentran al combinar 3 cartas del mismo color y diferente 
símbolo. Cada vez que combinas 3 cartas, buscas en el mazo una puerta y la sacas del mazo, cuando has encontrado las 10 puertas...
¡enhorabuena!, has vencido.

![Onirim](./imgs/onirim.png)\

Sin embargo, no sólo hay cartas de colores dentro del mazo, sino también las famosas **pesadillas** que te obligan a descartarte
de cartas, y otras cosas.

Vamos *simular* una versión *mini* de este juego de cartas usando un bucle `while`.

- Cada iteración del bucle sacaremos 5 cartas (las simularemos con un dado de 10 caras).
- Si salen 3 cartas entre el 1 y 5 añadiremos una puerta ROJA.
- Si salen 3 cartas entre el 6 y el 9 añadiremos una puerta VERDE.
- Si sale el 0, ha salido una pesadilla. En el momento que nos hayan salido 4 pesadillas, hemos perdido.
- Tras cada tirada, el programa debe preguntar si deseamos seguir jugando o nos conformamos con
  el numero de puertas que hemos encontrado hasta el momento.

\newpage
Os dejo un pequeño esqueleto del programa que os puede servir de ayuda.

```shell
#!/bin/bash

PROJAS=0
PVERDES=0
PESADILLAS=0
SEGUIR="Si"

while [ $PESADILLAS -lt 4 -a $SEGUIR = "Si" ]; do

	JUG_1=$((RANDOM % 10))
	JUG_2=$((RANDOM % 10))
	JUG_3=$((RANDOM % 10))
	JUG_4=$((RANDOM % 10))
	JUG_5=$((RANDOM % 10))
	
	# Comprobacion de pesadillas
	#....
	# FILL IN THE GAPS
	# 
	
	# Comprobacion de puertas rojas
	#....
	# FILL IN THE GAPS
	# 
	
	# Comprobacion de puertas verdes
	#....
	# FILL IN THE GAPS
	# 

	echo " Deseas Seguir? (Si/No)"
	read SEGUIR

done

echo " * Has encontrado puertas verdes: $PVERDES"
echo " * Has encontrado puertas rojas : $PROJAS"

exit 0
```

He valorado que este ejercicio cuenta por dos ya que tiene que realizar varias operaciones.