---
title: "30 Juegos de Scripts : El bucle for"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

\maketitle
\tableofcontents
\newpage

# Bucles: For

Llegamos a una de las partes más interesantes dentro de estos primeros pasos en la programación estructurada. Los **bucles**.

Los *bucles* son unas estructuras que nos permiten *repetir* una serie de operaciones un determinado número de veces o 
*mientras que se cumpla una condición*. Ambos son de dificultad similar, pero por motivos históricos, se suele empezar 
por el primero: el bucle `for`.

En este tipo de bucles, se sabe *desde el comienzo* cuantas veces se va a repetir el bucle y además sabremos cuales son los
valores que irá tomando determinada variable en las sucesivas repeticiones.

Por ejemplo, el código:

```shell
for num in 1 3 5 3 1; do 
	echo " El numero esta vez vale $num "
	sleep 1
done
```

Dará como resultado:

```shell
1
3
5
3
1
```

Entre cada uno de los números el programa se detiene (duerme) durante un segundo, lo que pasa es que el *pdf* no es tan sufrido ^_^.

Para mostrar por ejemplo a tabla de multiplicar del 4, podríamos hacer:

```shell
for num in 1 2 3 4 5 6 7 8 9 10; do 
	let RESULTADO=$num*4
	echo " 4 x $num = $RESULTADO "
done
```

Vamos a presentar algunos conceptos y realizaremos una serie de ejercicios para afianzar los conocimientos.

---

\newpage
## Conceptos: La variable índice

Como ya hemos visto en los ejemplos anteriores, en el bucle for hay una variable que toma una serie de valores, uno diferente para cada una de las 
ejecuciones del bucle (repeticiones).

Para facilitar la lectura, muchas veces aparece esta variable nombrada como `i` de la palabra `index`. Pero podéis utilizar la que más 
se adapte a la solución que estéis aplicando.

En el bucle `for` que hemos visto antes de la tabla de multiplicar, la variable es `num` y va tomando los valores entre el 1 y el 10 que aparecen
en el enunciado del bucle.

La *ejecución* del bucle sería, en palabras *humanas*: 

1. Se le asigna a `num` el valor 1.
2. Se calcula `RESULTADO` multiplicando el valor de `$num` por cuatro.
3. Se muestra el mensaje sustituyendo `$num` y `$RESULTADO` por sus valores respectivos.
4. Finaliza la primera iteración del bucle y se vuelve arriba, ya que quedan valores por asignar.
5. Se le asigna a `num` el valor 2.
6. Se calcula `RESULTADO` multiplicando el valor de `$num` por cuatro.
7. Se muestra el mensaje sustituyendo `$num` y `$RESULTADO` por sus valores respectivos.
8. Finaliza la primera iteración del bucle y se vuelve arriba, ya que quedan valores por asignar.
9. ... hasta que se hayan asignado a `num` todos los valores.

Vamos ahora a hacer un ejercicio sencillo para ir interiorizando esto.

\newpage

## buclefor - Ejercicio 1 : La Tripulación

En la Tripulación, un juego de bazas cooperativo, jugaremos de 2 a 5 jugadores de manera conjunta para la resolución de una serie de misiones que pondrán 
en juego todo nuestro ingenio.

En este juego se juegan una serie de cartas al más puro estilo de juego de bazas, donde el número más alto vence al más bajo. La asistencia al *palo* o *color* es obligatoria, y poco más...

Ah! y lo más *importante* no podéis hablar entre vosotros mientras se resuelven las misiones, toda la comunicación ha de ser utilizando los mecanismos, que el juego provee.

![La Tripulación](./imgs/thecrew01.png){width=30% height=30%}\

Vamos a crear un pequeño script que pregunte al usuario qué número tiene y para cada uno de los números posibles que pueden surgir ( entre el 1 y 9), le diga al usuario si su número gana la baza o no.

Por ejemplo:

```shell
usuario@maquina:~$./buclesfor-ejer01.sh 
Que numero de carta tienes? 
7
Al 1 le ganas.
Al 2 le ganas.
Al 3 le ganas.
Al 4 le ganas.
Al 5 le ganas.
Al 6 le ganas.
Tu tienes el 7.
El 8 te gana.
El 9 te gana.
```

Debes de resolver este ejercicio con un bucle `for`, no con 9 ifs (que os conozco).

\newpage
## Conceptos: Ejecución de comandos en el listado

Para facilitar la escritura de las declaraciones de los bucles `for` y permitirles ser más flexibles, se permite que en la declaración de los diferentes valores que vayamos a tomar, podamos 
ejecutar algún tipo de comando, de tal manera que la variable `indice` irá tomando los valores de las diferentes líneas, palabras, etc (separadas por espacios) que surjan de la ejecución
de los comandos.

Uno de los comandos más habituales es: `seq` que muestra por pantalla diferentes secuencias de números.

```shell
seq 1 10
```

Devuelve

```shell
1
2
3
4
5
6
7
8
9
10
```

Ahora podríamos reescribir el `for` de la tabla de multiplicar de la siguiente manera:

```shell
for num in $(seq 1 10); do 
	let RESULTADO=$num*4
	echo " 4 x $num = $RESULTADO "
done
```

Y sería totalmente equivalente

\newpage

## buclefor - Ejercicio 2 : La Tripulación - Misión custom

Amplia el ejercicio anterior, para que pregunte al usuario cual es el número de cartas posibles en la baraja de un determinado palo y haga el `for` en función de ese resultado.

Por ejemplo:

```shell
usuario@maquina:~$./buclesfor-ejer02.sh 
Cuantas cartas hay del palo?
4
Que numero de carta tienes? 
2
Al 1 le ganas.
Tu tienes el 2.
El 3 te gana.
El 4 te gana.
```

\newpage
## buclefor - Ejercicio 3 : La Tripulación - Comunicaciones Restringidas

En este juego, recordad que no se puede hablar entre los jugadores (al menos no de la partida), así que muchas veces toca hacer cálculos acerca de qué probabilidades tengo 
de ganar a determinadas manos...

![The Crew](./imgs/thecrew02.png){width=40% height=40%}\

Lo que vamos a hacer es *apuntarnos* cuantas bazas ganaría con el número que se le indique, mostrando al final unas pequeñas estadisticas:

Por ejemplo:
```shell
usuario@maquina:~$./buclesfor-ejer03.sh 
Cuantas cartas hay del palo?
5
Que numero de carta tienes? 
3
Al 1 le ganas.
Al 2 le ganas
Tu tienes el 3.
El 4 te gana.
El 5 te gana.
Pierdes 2/4
Ganas 2/4
```

¿Os animais ha mostrar las estadísticas usando porcentajes?.



