---
title: "Evaluación Extraordinaria"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
subtitle: Recover
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Instruciones (En Castellano).

- Leed cada uno de los ejercicios antes de comenzar a hacerlos.
- Buena suerte.

---

\newpage

# Ejercicio 1

Hemos sido contratad@s como tecnic@s de soporte a nivel de microinformatica de un observatorio científico sobre el cambio climático.

Estamos recibiendo datos de los diferentes observatorios estelares de las diferentes partes de España y Portugal. Estos observatorios son:

- `Azores`
- `Teide`
- `Algarve`
- `Cabo-de-Gata-Nijar`
- `Islas-Columbretes`
- `Monte-Perdido`
- `Torre-de-Hercules`
- `Cartagena`

Estos observatorios están enviando una serie de datos muy sencillos y necesitamos ir almacenando esos datos en diferentes carpetas para
luego poder realizar comprobaciones.

Los datos que recibimos de cada observatorio son:

- Temperatura
- Humedad
- Velocidad del viento.

Es decir, si por ejemplo de las **Islas Columbretes** nos indican que recibimos : *23 Grados*, una Humedad del *39%* y la medida ha sido tomada con un viento de *12 metros/s* , el script será ejecutado de la siguiente manera:

```shell
./micro-changes-of-air-density.sh Islas-Columbretes 23 39 12
```

El script deberá crear en la carpeta `/$HOME/Observatorio/Islas-Columbretes` **tres ficheros** (si no existían previamente) y guardar en cada uno de ellos la fecha y hora de la medida y la propia medida en la última línea del fichero (por supuesto la ruta a las carpetas previas también ha de ser comprobada).

**La hora en la que ha sido introducida la medición ha de escribirse en formato : YYYYMMDD-HHmm**.

\newpage

Ejemplo:
```shell
user@machine:~$./micro-changes-of-air-density.sh Islas-Columbretes 23 39 12
- Folder Observatorio not exists ... Creating it
- Folder Islas-Columbretes not exists ... Creating it.
- Storing 20240617-0933 , 23 in /$HOME/Observatorio/Islas-Columbretes/Temperatura.list
- Storing 20240617-0933 , 39 in /$HOME/Observatorio/Islas-Columbretes/Humedad.list
- Storing 20240617-0933 , 12 in /$HOME/Observatorio/Islas-Columbretes/Velocidad.list
```

Veamos ahora los mensajes suponiendo que al minuto siguiente ejecutaramos otra medición (con valores parecidos).


```shell
user@machine:~$./micro-changes-of-air-density.sh Islas-Columbretes 24 40 12
- Folder Observatorio exists ... nothing to do.
- Folder Islas-Columbretes exists ... nothing to do.
- Storing 20240617-0934 , 24 in /$HOME/Observatorio/Islas-Columbretes/Temperatura.list
- Storing 20240617-0934 , 40  in /$HOME/Observatorio/Islas-Columbretes/Humedad.list
- Storing 20240617-0934 , 12 in /$HOME/Observatorio/Islas-Columbretes/Velocidad.list
```

Con el objetivo de tomar medidas de manera "*homogénea*", hemos previsto que cuando cuando una medida se introduzca, se
muestre un resumen del estado de todos los Observatorios con la siguiente información (**por observatorio**):

- Cuantas mediciones han sido tomadas.
- Cuál es la hora de la última medición.
- Cuál es la máxima temperatura registrada (sin la hora).
- Cuál es la menor humedad registrada (sin la hora).
- Cuál es la máxima velocidad del viento registrada (sin la hora).

> [SUBMIT] : The Shell Script named : `micro-changes-of-air-density.sh`

\newpage

## Usuarios y Grupos

Han contratado una serie de Colaboradores en el Equipo de científic@s que nos están ayudando a gestionar las diferentes
mediciones que nos llegan de los observatorios.

| Usuario | Password | Grupos |
|------|----------|-------|
| isla | todoMar | cientificos, oceanos |
| peninsula | todoTierra | cientificos, continentes|

Debes crear los dos usuarios y los grupos en el sistema para poder hacer las siguientes modificaciones al script anterior:

- Si como quinto argumento se le indica `--user` deberá establecer los **permisos adecuados** en todos los ficheros del observatorio sobre el que está
  guardando información (ver más abajo).
- Si se le pasa un Observatorio que no sea de la lista anterior, debe mostrar un mensaje y fallar.
- Si como segundo argumento se le indica `--copy` deberá copiar la carpeta completa `/$HOME/Observatorio` a la carpeta `/$HOME/Backups-Observatorio/YYYY-MM-DD_HH-mm/` y ponerle los permisos `rwxr-x---` a todas las carpetas y ficheros de la *copia* (deberá crear la carpeta con fecha si no existe).

### Permisos adecuados

- Los ficheros y carpetas deberán pertenecer al usuario "islas" o "peninsula", dependiendo de la ubicación del observatorio (la geografía mola).
- El grupo será siempre `cientificos`, excepto en "Teide" que será `oceanos`.
- El propietario debe poder siempre leer, escribir y ejecutar, el grupo sólo leer y otros deberán poder acceder a la información pero no modificarla.

> [SUBMIT] : The Shell Script named : `micro-changes-of-air-density-with-users.sh`.

\newpage

# Ejercicio 2

Añade 3 discos duros virtuales de 2GB a la máquina virtual de IslandOS y crea particiones en cada uno de ellos.

Los tres discos estarán particionados de la siguiente manera :


| Disco Duro | Particiónes | Sistema de ficheros |
|------------|-----------|---------------------|
| `/dev/sdb`   | 2         | ext4 ambas|
| `/dev/sdc`   | 1         | Fat32|
| `/dev/sdd`   | 2         | NTFS ambas |

Para las particiones de `/dev/sdc` y `/dev/sdd` crea 3 puntos de montaje en `/mnt/` llamados:

- `fat`
- `ntfs_uno`
- `ntfs_dos`

Para las particiones de `/dev/sdb` crea sus puntos de montaje en `/observatorio/backups` y `/observatorio/medidas`

Modifica el fichero de `/etc/` que contiene la información de los puntos de montaje y haz que todo se monte en el arranque.

Crea un ShellScript que obtenga el tamaño de los diferentes puntos de montaje de los 4 discos (sí, contamos con `/` también). Deberá hacer las comprobaciones de que los puntos de montaje están creados, que estamos ejecutando el script con privilegios y de que tenemos las particiones correctamente creadas y formateadas.

> [SUBMIT] : The Shell Script named : `filesystem-observatorio.sh` and the file modified with the mountpoints.

