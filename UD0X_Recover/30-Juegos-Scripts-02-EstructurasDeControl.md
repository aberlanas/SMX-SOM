---
title: "30 Juegos de Scripts : Estructuras de Control"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

\maketitle
\tableofcontents
\newpage

# Continuación

*Aviso a Navegantes :*  A partir de aquí, se plantean el resto de ejercicios, así como se presentan unas pequeñas notas 
técnicas que pueden ser consultadas, ampliadas, etc. antes de cada uno de los bloques de 3 ejercicios.


\newpage
# Estructuras de Control I

## Conceptos: Estructuras de Control

Las estructuras de control son instrucciones que se le dan a los lenguajes de programación para la toma de decisiones, las más 
básicas son los llamados `IF`. 

Estas estructuras nos sirven para ejecutar determinados bloques de instrucciones (**código**) en función de que se cumplan (o no) una
serie de requisitos, situaciones, valores, etc.

El dominio de esta estructura es totalmente requerido dentro del mundo de la programación/la administración de sistemas y en un mundo
cada vez más comunicado, nos ayuda a comprender cómo funcionan las cosas.

Los ejercicios que se plantean para dominar esta estructura son tan sólo una pequeñísima muestra de las posibilidades de la misma.

---

Normalmente en los Scripts de Shell en GNU/LinuX podemos hacer uso de las opciones del comando `test`. Que además al leer el manual 
del mismo (`man test`) podemos darnos cuenta de que se trata de propio *corchete* en la sintaxis de la estructura **if**.

```shell
#!/bin/bash

VALOR=3

if [ $VALOR -eq 3 ]; then
	# Se cumple la condicion que se 
	# ha evaluado en el script.
	echo " LA CONDICION SE CUMPLE "
fi

if [ -e /tmp/ ]; then
	echo " El directorio /tmp/ existe"
fi

exit 0
```

\newpage
## EdC : Ejercicio 1

Crea un pequeño Shell script que pida al/a usuari@ por su nombre y edad (en años) , si la edad es mayor o igual a 18 años, debe indicarle
que ya puede ir a matricularse en la Autoescuela (si quiere).

Ejemplos de ejecuciones:

```shell
usuario@maquina:~$./edc01-ejer01.sh 
Hola, dime tu nombre:
Angel
Ahora dime tu edad:
39
Veo que tu edad es mayor de 18 Angel, puedes apuntarte a la autoescuela
Adios
```

```shell
usuario@maquina:~$./edc01-ejer01.sh 
Hola, dime tu nombre:
Pascualin
Ahora dime tu edad:
17
Adios
```

Comandos implicados:

* `read`
* `test`
* `man test`

\newpage

## Conceptos: Operaciones matemáticas

Para la toma de decisiones, tal y como hemos visto antes, muchas veces tenemos que realizar operaciones matemáticas, algunas serán 
tan simples como comprobar si un número es mayor/menor/igual que otro dado, y en otras ocasiones deberemos obtener datos a partir 
de la realización de operaciones matemáticas sobre los datos de entrada.

Algunos ejemplos de operaciones matemáticas serían:

```shell
#!/bin/bash

# SUMA
echo " Suma "
let RESULTADO=4+5
echo $RESULTADO


# RESTA
echo " Resta "
let RESULTADO=3-1
echo $RESULTADO


# MULTIPLICACION
echo " Multiplicacion "
let RESULTADO=3*10
echo $RESULTADO

# DIVISION
echo " Division "
let RESULTADO=10/3
echo $RESULTADO

# RESTO DE LA DIVISION ENTERA
echo " Resto "
let RESULTADO=10%7
echo $RESULTADO

# POTENCIA
echo " Potencia (elevado)"
let RESULTADO=10**2
echo $RESULTADO


exit 0
```

Os animo ha manipular el script presentado de ejemplo y probar resultados diferentes, negativos, divisiones por 0, multiplicaciones por valores límite, curiosidades,...

## EdC : Ejercicio 2 : Rummikub!

Vamos a ir presentando una serie de juegos de mesa a lo largo de los ejercicios que se plantearán, en estos juegos aparecerán mecánica, soluciones, jugadas, que podrán ser
resueltas o asistidas usando pequeños programas realizados mediante shell scripts.

Comenzaremos por el Rummikub.

![Rummikub](imgs/rummikub.png)\

En este juego de mesa se van robando fichas de un *pool* común hasta que puedes *salir*, y a partir de ese momento ya puedes interactuar con todas las fichas de la mesa, hayan sido 
puestas por ti o no. Sin embargo esa *salida* no es *sencilla* de ejecutar, ya que tienes que salir al menos con una cantidad de fichas cuyos valores sumen : **30**.

Asumiendo que cómo máximo puedas salir con 5 fichas, realiza un shell script que pregunte por los valores de las 5 fichas y devuelva si es una jugada de salida válida o no (**que sumen más de 30**).

\newpage

Ejemplo de ejecución:

```shell
usuario@maquina:~$./edc01-ejer02.sh 
Ficha jugada 1:
3
Ficha jugada 2:
4
Ficha jugada 3:
5
Ficha jugada 4:
6
Ficha jugada 5:
7
Suman 25
```

```shell
usuario@maquina:~$./edc01-ejer02.sh 
Ficha jugada 1:
5
Ficha jugada 2:
6
Ficha jugada 3:
7
Ficha jugada 4:
8
Ficha jugada 5:
9
Suman 35
* Jugada de Salida Valida
```


Comandos implicados:

* `read`
* `test`
* `man test`

\newpage

## EdC : Ejercicio 3 : Rummikub Avanzado!

Sigamos con el Rummikub, que dá para bastantes ejercicios.

![Rummikub](imgs/rummikub02.png){width=300px height=200px}\

Una de las jugadas válidas del Rummikub es la escalera de 3 fichas del mismo color, cuyos valores sean consecutivos, por ejemplo:

* 1,2,3
* 3,4,5
* 8,9,10

Asumiendo que cómo máximo puedas jugar 3 fichas, realiza un shell script que pregunte por los valores de las 3 fichas y devuelva si es una jugada válida para ser jugada independientemente 
de la situación del tablero (es decir, que sean consecutivas).

Ejemplos:

```shell
usuario@maquina:~$./edc01-ejer03.sh 
Ficha jugada 1:
3
Ficha jugada 2:
4
Ficha jugada 3:
5
**Jugada Valida**
```

```shell
usuario@maquina:~$./edc01-ejer03.sh
Ficha jugada 1:
3
Ficha jugada 2:
5
Ficha jugada 3:
6
```

Comandos implicados:

* `read`
* `test`
* `man test`


\newpage
# Estructuras de Control II

## Conceptos: Alternativas

La estructura `if` suele ir acompañada del `else`, en la lengua humana serían el *si* y el *si no*. 

Es más fácil comprender esto con un ejemplo:

```shell
#!/bin/bash

VALOR=5

if [ $VALOR -lt 10 ]; then
	# Se cumple la condicion que se 
	# ha evaluado en el script.
	echo " $VALOR es menor que 10 "
else
	# Si no se ha cumplido la condicion
	echo " $VALOR es mayor o igual que 10"
fi

if [ -e /tmp/ ]; then
	echo " El directorio /tmp/ existe"
else
	echo " El directorio /tmp/ no existe"
fi

exit 0
```

Daos cuenta de la *identación* presente en los `ifs`.

Vamos a hacer unos ejercicios para afianzar estos conceptos.

\newpage

## EdC : Ejercicio 4 : Catan

Vamos ahora a uno de los primeros juegos de mesa modernos, y un clásico que se sigue vendiendo cada año por miles, estamos hablando del Catan. 

![Catan](imgs/catan01.png){width=35% height=35%}\

En este juego, una vez se han situado los jugadores en la malla hexagonal que es el tablero donde en las diferentes losetas aparecen unos números escritos, 
se lanzan dos dados de 6 caras, se suma el resultado de ambos dados, y aquellos hexágonos cuyos números coinciden con el resultado de la suma obtenida producen los recursos que representan:

* Ovejas
* Trigo
* Madera
* Barro
* Piedra

Los numeros se repiten en diferentes hexágonos con lo que un resultado puede dar lugar a varias "*producciones*".

Pero existe un resultado en la suma de los dados que tiene algo especial: el **7**, que hace que me muevan unos bandidos por el mapa y no se produce 
nada ese turno.

Sabiendo que el siguiente fragmento de código, simula la tirada de un dado de 6 caras:

```shell
TIRADA_DE_DADO=$((1 + RANDOM % 6))
```

Realiza un shell script que lance los dos dados, los sume y nos indique si se trata de una tirada de ladrones (el resultado de la suma ha sido un 7), o una tirada
normal de producción.

Ejemplos de ejecución:

```shell
usuario@maquina:~$./edc01-ejer04.sh
Dados : 3 + 4 = 7 
Han salido ladrones! - No se produce
```


```shell
usuario@maquina:~$./edc01-ejer04.sh
Dados : 5 + 4 = 9
Tirada de producción
```

\newpage
## Conceptos: Alternativas anidadas

Podemos ir anidando este tipo de estructuras y preguntas para obtener resultados más complejos y 
realizar operaciones en función de varias comprobaciones consecutivas:

Ejemplo:

Vamos a comprobar que nos dan un numero que sea mayor de 10, en caso de que sea mayor 10, debemos 
comprobar que es múltiplo de 5. 

Si no es mayor que 10, vamos a comprobar que sea múltiplo de 2.

```shell
#!/bin/bash
echo " Indica un numero : "
read NUMERO

if [ $NUMERO -gt 10 ]; then
	# El numero es mas grande que 10
	# luego ahora tenemos que comprobar que es 
	# multiplo de 5.
	# para ello utilizaremos la operacion Modulo (resto)
	# si el numero dividido por 5 da de resto 0.
	# significara que es multiplo de 5.
	let RESTO=$NUMERO%5
	if [ $RESTO -eq 0 ]; then
		echo " El numero es mayor que 10 y multiplo de 5 "
	else
		echo " El numero es mayor que 10, pero no es multiplo de 5"
	fi
else
	let RESTO=$NUMERO%2
	if [ $RESTO -eq 0 ]; then
		echo " El numero es menor que 10 y multiplo de 2 (par)"
	else
		echo " El numero es menor de 10, pero no es multiplo de 2 (es impar)"
	fi
fi
exit 0
```

\newpage
## EdC : Ejercicios 5 : Catan y producción

Vamos ampliar el script que habéis realizado antes, indicando para los otros resultados diferentes a la tirada de los ladrones los recursos que se producen.

![Catan](imgs/catan02.png){width=55% height=55%}\

Vamos a suponer que la malla hexagonal del tablero ha salido muy curiosa, concretamente los números de las zonas de producción son las siguientes:

* Se produce madera si el número es par.
* Se produce barro si el número es impar.
* Se produce trigo si el número es múltiplo de 3.
* Se producen ovejas si el número no es múltiplo de 3.
* Se produce piedra si el número es múltiplo de 4.

Daos cuenta de que determinados resultados producen varios recursos:

* Si sale un **4** se produce *piedra* y *madera*.
* Si sale un **12** se produce *madera*, *trigo* y *piedra*.

Por supuesto, si sale el resultado de los ladrones, no debe producirse nada, tal y como funcionaba el ejercicio anterior.

Ejemplos de ejecución:

```shell
usuario@maquina:~$./edc01-ejer05.sh
Dados : 3 + 4 = 7 
Han salido ladrones! - No se produce
```


```shell
usuario@maquina:~$./edc01-ejer05.sh
Dados : 5 + 4 = 9
Tirada de producción:
- Barro
- Trigo
```

```shell
usuario@maquina:~$./edc01-ejer05.sh
Dados : 5 + 5 = 10
Tirada de producción:
- Madera
- Ovejas
```

```shell
usuario@maquina:~$./edc01-ejer05.sh
Dados : 1 + 1 = 2
Tirada de producción:
- Madera
- Ovejas
```


\newpage
## EdC : Ejercicios 6 : Catan y expansiones

Vamos seguir ampliando el script que habéis realizado antes, añadiendo en función de unas expansiones que están disponibles, unos determinados resultados.

![Catan](imgs/catan03.png){width=55% height=55%}\

Al comenzar el script debemos preguntale al usuario si se está jugando con la expansión de las ciudades. En caso afirmativo, cada vez que el número que salga de la suma sea:

* 2,3,11 o 12.

La producción se *dobla*.

Vamos a suponer la misma malla hexagonal de antes, que expongo de nuevo:

* Se produce madera si el número es par.
* Se produce barro si el número es impar.
* Se produce trigo si el número es múltiplo de 3.
* Se producen ovejas si el número no es múltiplo de 3.
* Se produce piedra si el número es múltiplo de 4.

Daos cuenta de que determinados resultados producen varios recursos:

* Si sale un **4** se produce *piedra* y *madera*.
* Si sale un **12** se produce *madera*, *madera*, *trigo*, *trigo*, *piedra* y *piedra*.

Por supuesto, si sale el resultado de los ladrones, no debe producirse nada, tal y como funcionaba en los ejercicios anteriores.

Ejemplos de ejecución:

```shell
usuario@maquina:~$./edc01-ejer05.sh
Juegas con la expansion (Si/No)?
Si
Dados : 3 + 4 = 7 
Han salido ladrones! - No se produce
```


```shell
usuario@maquina:~$./edc01-ejer05.sh
Juegas con la expansion (Si/No)?
No
Dados : 5 + 6 = 11
Tirada de producción:
- Barro
- Ovejas
```

```shell
usuario@maquina:~$./edc01-ejer05.sh
Juegas con la expansion (Si/No)?
Si
Dados : 5 + 5 = 10
Tirada de producción:
- Madera
- Ovejas
```

```shell
usuario@maquina:~$./edc01-ejer05.sh
Juegas con la expansion (Si/No)?
Si
Dados : 1 + 1 = 2
Tirada de producción:
- Madera
- Madera
- Ovejas
- Ovejas
```

