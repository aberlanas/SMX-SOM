---
title: "[ Examen Recuperación ]1 Evaluación 22-23"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Introducción

Bienvenid@s al Examen de Recuperación *Extraordinario* de la Evaluación 1.

Tenéis 2 sesiones para la finalización del mismo. 

Son 10 preguntas. Todas puntuan 0 o 1.

**Leed todos los ejercicios antes de empezar con ninguno.**

\newpage

# Ejercicio 1

Realiza un Shell Script que pida al usuario un número y una palabra. A continuación debe mostrar la palabra tantas veces como el número
que le ha dicho el usuario.

Ejemplo:

```shell
usuario@maquina:~$./examen-rec01.sh 
Dime un numero: 
4
Dime una palabra:
tornamusa
Aqui tienes tus palabras:
tornamusa
tornamusa
tornamusa
tornamusa
```

# Ejercicio 2

Realiza un Shell Script que acepte como primer argumento un numero inicial y como segundo argumento un numero final. A continuación
el script debe mostrar los numeros que están entre los dos numeros indicados (**excluyendo ambos números indicados**).

Ejemplo:

```shell
usuario@maquina:~$./examen-rec02.sh 4 8
5
6
7
```

# Ejercicio 3

Realiza un Shell Script que pida al usuario un número. A continuación mostrará todos los números entre el 1 y ese número indicado que sean 
pares.

Ejemplo:

```shell
usuario@maquina:~$./examen-rec03.sh 7
2
4
6
```

# Ejercicio 4

Realiza un Shell Script que pida al usuario un número. A continuación mostrará todos los números entre el 1 y ese número indicado que no sean 
pares ni múltiplos de 3

Ejemplo:

```shell
usuario@maquina:~$./examen-rec04.sh 17
1
5
7
11
13
17
```

# Ejercicio 5

Realiza un Shell Script que al ejecutarse :

* Borre si existe la carpeta `/tmp/ejer5`. y todas sus subcarpetas.
* Cree las siguientes carpetas: 
	* `/tmp/ejer5/paso1`
	* `/tmp/ejer5/paso2`
	* `/tmp/ejer5/paso3`
	* `/tmp/ejer5/paso4`
	* `/tmp/ejer5/paso5`
	* `/tmp/ejer5/paso6`
* Por último muestre con un `tree` el contenido de la carpeta `/tmp/ejer5`

# Ejercicio 6

Realiza un Shell Script que pida al usuario un número. Y que vaya incrementando una variable con el numero que se le ha indicado. 
Debe seguir pidiendo números hasta que la variable que va acumulando alcance un valor superior o igual 42. Tras cada petición, debe 
indicar cuanto lleva acumulado.

Si cuando acabe el numero es exactamente 42 debe mostrar el mensaje: "Es suficiente", 
en caso contrario debe mostrar el mensaje: "La respuesta a la vida, al universo y a todo".

Ejemplo:

```shell
usuario@maquina:~$./examen-rec06.sh
Dime un numero 2
Llevas 2
Dime un numero 13
Llevas 15
Dime un numero 24
Llevas 37
Dime un numero 5
Llevas 42
Es suficiente
```

# Ejercicio 7

Realiza un Shell Script que de la salida del comando `ps -e`, muestre aquellas líneas cuyo PID sea mayor a 3000 y su CMD tenga una `f`.

# Ejercicio 8

Realiza un Shell Script que de la salida del comando `ps -e`, muestre aquellas líneas cuyo PID sea mayor a 3000, menor que 5000 y su CMD tenga una `e`.

# Ejercicio 9

Realiza un Shell Script que de la salida del comando `ps -e`, muestre aquellas líneas cuyo PID sea mayor a 3000, menor que 5000, sea par y su CMD tenga una `a`.

# Ejercicio 10

Realiza un Shell Script que de la salida del comando `ps -e`, sume el tiempo en Horas:Minutos:Segundos de todos los procesos, incrementando como toca cada uno de los
campos.

* Si se superan los 60 segundos, se añaden a minutos.
* Si se superan los 60 minutos, se añaden a horas.
