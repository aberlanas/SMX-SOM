---
title: "[ Recuperación Extraordinaria] 1 Evaluación 22-23"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Introducción

Bienvenid@s al Examen de Recuperación *Extraordinario* de la Evaluación 1.

Tenéis 2 sesiones para la finalización del mismo. 

**Leed todos los ejercicios antes de empezar con ninguno.**

\newpage

# Ejercicio 1 ( 2 puntos )

Realiza un Shell Script que acepte como primer argumento un numero inicial y como segundo argumento un numero de pasos. Para cada uno de esos pasos, usaremos el número inicial y le sumaremos el numero 2 elevado a ese paso. A continuación
el script debe mostrar los numeros aparecen en la secuencia.

Ejemplo de ejecución:

```shell
usuario@maquina:~$./examen-eva01-rec01.sh 20 3
22
24
28
```

# Ejercicio 2 ( 1 punto )

Realiza un Shell Script que al ejecutarse :

* Borre si existe la carpeta `/tmp/eva01-ejer`. y todas sus subcarpetas.
* Cree las siguientes carpetas: 
	* `/tmp/eva01-ejer/paso1`
	* `/tmp/eva01-ejer/paso2`
	* `/tmp/eva01-ejer/paso3`
* Por último muestre con un `tree` el contenido de la carpeta `/tmp/eva01-ejer`
* Para cada uno de los pasos deberá mostrar un mensaje indicando en que paso se encuentra.

# Ejercicio 3 ( 1 punto )

Realiza un Shell Script que pida al usuario un número. Y que vaya incrementando una variable con el numero que se le ha indicado. 
Debe seguir pidiendo números hasta que la variable que va acumulando alcance un valor superior o igual 42. Tras cada petición, debe 
indicar cuanto lleva acumulado.

Si cuando acabe el numero es exactamente 42 debe mostrar el mensaje: "Es suficiente", 
en caso contrario debe mostrar el mensaje: "La respuesta a la vida, al universo y a todo".

Ejemplo:

```shell
usuario@maquina:~$./examen-eva01-rec03.sh
Dime un numero 2
Llevas 2
Dime un numero 13
Llevas 15
Dime un numero 24
Llevas 39
Dime un numero 3
Llevas 42
Es suficiente
```

# Ejercicio 4 ( 2 puntos ) 

Realiza un Shell Script que de la salida del comando `ps -e`, muestre aquellas líneas cuyo PID esté entre 2000 y 4000, sea múltiplo de 3, contenga un 1 (su PID) y su CMD tenga una `e`.

# Ejercicio 5 ( 4 puntos )

Realiza un Shell Script que de la salida del comando `ps -e`, sume el tiempo en Horas:Minutos:Segundos de todos los procesos, incrementando como toca cada uno de los
campos.

* Si se superan los 60 segundos, se añaden a minutos.
* Si se superan los 60 minutos, se añaden a horas.
* Si se superan las 24 se añaden a la variable dias.
