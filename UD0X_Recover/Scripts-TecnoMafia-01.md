---
title: " Scripts de la TecnoMafia "
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Misiones de Farmeo
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Introdución

El objetivo de este documento es plantear una serie de ejercicios que recogen
los conceptos que se han ido planteando en el Módulo.

Para cada uno de los ejercicios recordad que debéis crear un pequeño fichero de *script* y 
darle permisos de ejecución.

Crear un fichero de *script* usando `nano`, `vim`, `mousepad`,...

Una vez creado, recordad darle permisos de ejecución con:
```shell
chmod +x ./RUTA_AL_FICHERO.sh
```

Se recomienda crear una carpeta para cada una de las categorías, de tal manera que nos 
sirva para más adelante como una pequeña *biblioteca* de recursos, que sin duda van a
ser utilizados.

Esto nos permitirá ser más ordenados y tener claro *donde* buscar.


# Algo de Lore

Acabamos de llegar a **Paiporta City Uno**, una ciudad en secreto gobernada por una 
élite de sujetos que han llegado al poder usando tecnologías peligrosas y prohibidas.

La red de influencias es demasiado poderosa y las fuerzas del orden no son capaces
de restablecer su poder aquí, pero el equilibrio entre bandas es muy frágil y alguien 
desde dentro podría llegar a unificar todas las bandas y transformar **Paiporta City Uno** 
en una ciudad próspera.

Con el objetivo en mente de ser l@s verdaderos *jef@s* de **Paiporta City Uno** vamos
a enfrentarnos a una serie de trabajos que nos permitirán desarrollar nuestras capacidades
y derrotar a las bandas.

Mucha suerte en la misión.

\newpage
# Script 01: Un trabajo de poca monta

Acabamos de llegar a la zona, necesitamos crearnos algo de fama para que se fijen en 
nosotros en **El Sindicato**, antes de comenzar a ser alguien en este mundo de los Sysadmins
de los bajos fondos.

Oimos que unos desalmados están pidiéndole demasiado dinero a la dueña de la Cafetería de 
*Cafés SinMiedo*, le están cobrando por un software terrible que no funciona, y le obligan
a usarlo en su TPV (*Terminal Punto de Venta*). Vamos a ayudarla creando uno que 
funcione igual y además esté hecho con **Software Libre**.

## Requisitos

El script debe mostrar un menú donde se permita : 

- Crear comanda.
- Imprimir comandas.
- Hacer caja.
- Salir.

## Crear comanda

Dentro de submenú de `Crear comanda`, debemos permitir :

- Añadir un café (1 euro).
- Añadir un *machiatto* (1 euro).
- Añadir un *capuchino* (2 euros).
- Añadir un café con *hielo fresco* (3 euros).
- Calcular el total, guardar la comanda en un fichero de regitro (`caja.txt`) y salir.
- Salir sin guardar la comanda.

Cada pedido de cada comanda debe incluir la fecha y la hora en la que se hizo en formato:

- `[YYYYMMDD-HHmm]`

**Misión oculta de la Mafia**: Si el camarero detecta que los esbirros del *Jefe Berlinas* entran al local,
puede pedir ayuda creando una comanda que incluya *exclusivamente en este orden* : 

- Café, Café, Capuchino, Café, Café.

En ese caso, se escribirá en un fichero llamado `aviso-al-jefe.txt` la fecha de hoy, incluyendo 
los segundos.

## Imprimir comandas

En este menú, solicitará al camarero una fecha en el formato `[YYYYMMDD-HHmm]` y mostrará 
todas las comandas y sus pedidos de ese dia, en caso de que no haya ninguna, que muestre
un mensaje : `"Un mal dia para la cafeteria"`.

## Hacer caja

Del dia de *hoy*, mostrar el **total** de dinero acumulado en comandas.

## Salir

Salir del programa. En caso de que se haya avisado a la protección, mostrar un mensaje de 
despedida *diferente al habitual* (a elección del/a novat@).


