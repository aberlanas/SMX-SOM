---
title: "30 Juegos de Scripts : Variables "
author: [Angel Berlanas Vicente]
date: 
subject: "Tareas"
keywords: [Recuperacion, Ejemplos]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

\maketitle
\tableofcontents

\newpage
# Introdución

El objetivo de este documento es plantear una serie de ejercicios que recogen
los conceptos que se han ido planteando en el Módulo a lo largo del primer trimestre.

Para cada uno de los ejercicios recordad que debéis crear un pequeño fichero de *script* y 
darle permisos de ejecución.

Crear un fichero de *script* usando `nano`, `vim`, `mousepad`,...

Una vez creado, recordad darle permisos de ejecución con:
```shell
chmod +x ./RUTA_AL_FICHERO.sh
```

Se recomienda crear una carpeta para cada una de las categorías, de tal manera que nos 
sirva para más adelante como una pequeña *biblioteca* de recursos, que sin duda van a
ser utilizados.

Esto nos permitirá ser más ordenados y tener claro *donde* buscar.

\newpage
# Variables 

## Conceptos: ¿Qué es una variable?

Las variables son pequeñas cajas que dentro de los scripts y los entornos de los Sistemas Operativos, nos permiten
almacenar un valor para luego ser consultado/modificado más adelante en la ejecución de un script.

Por ejemplo:
```shell
#!/bin/bash

CONTADOR=1

echo "En este momento la variable CONTADOR tiene el valor $CONTADOR"

# Esto es un comentario
# Ver la nota mas abajo

let CONTADOR=$CONTADOR+1

echo " Ahora vale uno mas de lo que valia antes :$CONTADOR"

exit 0
```

Note el/la lector/a avezado/a que cuando se **DECLARA** una variable, o se **ASIGNA** un valor
el identificador de la misma (en este caso la palabra CONTADOR) va sin el `$` de delante.

Mientras que cuando queremos acceder a su contenido, debemos indicarle al interprete de 
ejecució que debe *resolverla*, es decir *acceder a su contenido de manera dinámica* usando 
el `$`.

## Variables - Ejercicio 1

Usando el comando `read`, crea un pequeño script que pida al usuario un número y el programa
le sume 24 a ese número y lo devuelva por pantalla.

Ejemplos de uso de `read`:

```shell
#!/bin/bash

echo " Dime un numero : "
read NUMERO
echo " El numero introducido ha sido $NUMERO"

exit 0
```

Si necesitais más información : `man read`.

\newpage

## Conceptos : Sálida de órdenes en variables.

Las variables no solo pueden ser asignadas *directamente* o mediante `read`, también podemos
usar la *salida* de otros comandos y guardarla en esas variables.

Ejemplo:

```shell
#!/bin/bash

SALUDO=$(echo " Hola que tal : ")
echo " DIME TU NOMBRE :"
read NOMBRE
echo " $SALUDO $NOMBRE "
exit 0
```

Otro ejemplo (que veremos más intensamente en los bucles sería la ejecución de un comando más *complicado*,
como pudiera ser `seq`:

```shell
#!/bin/bash

LISTADENUMEROS=$(seq 1 10)

echo "$LISTADENUMEROS"

exit 0
```

## Variables - Ejercicio 2

Crea un script que cuente el número de líneas del fichero `/etc/hosts` y lo muestre en un mensaje del estilo

```shell
usuario@maquina:~$./variables-ejer02.sh 
El fichero /etc/hosts tiene 9 lineas.
```

Daros cuenta de que el script muestra **sólo** una línea de mensaje y **cómo** está estructurada. Debe devolver lo mismo.

Comandos implicados:

* `wc -l`
* `cat`
* Tuberías (`|`).

\newpage
## Conceptos : Tuberías

Las tuberías son un  mecanismo de comunicación entre procesos que se utilizan ampliamente en los sistemas operativos, utilizándose 
para que la salida de determinados comandos sean la entrada del siguiente.

Por ejemplo: 

Las ordenes:

`cat /etc/hosts | grep 127`

Al tener la tubería (`|`), lo que hacen es que la salida del comando `cat` (que por defecto es mostrar el contenido de un fichero
por la salida estándar), se la pasa al comando `grep`, que busca las líneas que contienen un determinado *patrón*.

De esta manera al ejecutarse *conjuntamente* obtenemos resultados más útiles y *afinados* que nos serán muy útiles.

## Variables - Ejercicio 3 - 

Crea un Shell Script que obtenga y guarde en *variables* la siguiente información:

* ¿Qué usuario está ejecutando el script?
* ¿Qué hora es (con la fecha)?
* ¿Cuanto tiempo lleva encendido el ordenador?

Y muestre un pequeño resumen de este estilo:

```shell
usuario@maquina:~$./variables-ejer03.sh 
Hola usuario : usuario.
Estamos a mar 20 dic 2022 19:10:39 CET
Y el ordenador lleva encendido 4:35 horas
```

Comandos implicados:

* `whoami`
* `date`
* `cut`
* Tuberías (`|`).





