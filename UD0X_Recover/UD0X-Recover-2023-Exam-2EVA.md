---
title: "[ Examen Recuperación ] 2 Evaluación 22-23"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Introducción

Bienvenid@s al Examen de Recuperación *Extraordinario* de la Evaluación 2.

Tenéis 2 sesiones para la finalización del mismo. Pero necesitaréis una máquina Xubuntu que tendréis que descargar desde el repositorio del IES:

```
http://tic.ieslasenia.org/SOM/XubuntuRecuperacion.ova
```

| Datos del usuario | Valores|
|-------------------|--------|
| User|  adminsom       |
| Password | nimda123|

Descargadla e ir importándola mientras que leéis el examen.

**Leed todos los ejercicios antes de empezar con ninguno.**

\newpage

# Ejercicio 1 ( 5 puntos )

Haz un Shell Script que para cada usuario que haya en el equipo, muestre la siguiente información:


- Si su ID es mayor que 999:

    - Login.
    - ID.
    - Ruta a la carpeta personal (si existe).
    - Ruta al fichero más nuevo modificado por ese usuario en su carpeta personal (si existe).
    - Tamaño de su carpeta personal (si existe)  en **MegaBytes**.
    - Tamaño en KB de su shell por defecto y cuál es.

- Si su ID es menor que 1000:

    - Login.
    - ID.
    - Shell por defecto.
    - Número de carpetas en el primer nivel de /var/ que pertenecen a ese usuario.

Comandos útiles:

 - `find`
 - `wc`

\newpage

# Ejercicio 2 ( 2,5 puntos )

Crea un usuario con `adduser` con los datos que aparecen a continuación y modifícalo para que su carpeta personal se encuentre en `/home/recuperacion/extra/`. Inicia sesión gráfica con él y realiza una captura de pantalla donde se muestre que se ha lanzado una terminal desde ese usuario.

| Datos Usuario | Valores |
|:-------------:|:-------:|
| login         | smxex   |
| pass          | xexms123   |
| Nombre        | Examen  |


Adjunta el fichero `/etc/passwd` de la máquina junto con la captura que has realizado.

# Ejercicio 3 ( 2,5 puntos )

Modifica el fichero de `sources.list` de la máquina virtual, quita la arquitectura `i386` y actualízala.

Instala el OpenSSH server en la máquina, configurala para que tenga una IP de la red del Aula e inicia sesión usando SSH  desde la máquina real. Realiza capturas de pantalla del proceso.

Redacta un pequeño manual en Markdown que muestre los diferentes pasos que has realizado. Adjúntalo en formato Markdwon.


