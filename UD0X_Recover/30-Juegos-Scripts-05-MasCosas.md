---
title: "30 Juegos de Scripts : 15 ejercicios con argumentos y procesos"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
keywords: [Markdown, Ficheros, Rendimiento]
subtitle: Recover
lang: "es"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

\maketitle
\tableofcontents

\newpage
# Ejercicio 15
-----------

El sistema está teniendo problemas de rendimiento. Al parecer uno de los
usuarios del sistema tiene demasiados ficheros en su carpeta personal.
Debemos crear un script que al ejecutarlo recorra las diferentes
carpetas personales de los diferentes usuarios del equipo (incluido
`root`).

Y compruebe para cada una de las diferentes carpetas si ocupa más de
**10 MB**, si es así que muestre un mensaje indicando que se ha
superado el límite de espacio establecido.

Pistas:

-   Las carpetas personales de los usuarios se encuentran en `/home`
    excepto `root` que la tiene en `/root/`.
-   Un buen comando para obtener el tamaño de un directorio es `du`.

Ejemplo de ejecución:

```shell
aberlanas@moria:~$./bucle-01.sh
* Testing [ aberlanas ] : -> Size exceeded : 192
* Testing [ smx ] -> Size correct : 9 
* Testing [ daw ] -> Size correct : 4 
* Testing [ root ] -> Size correct : 1
```

\newpage

# Ejercicio 16

Se ha detectado un incremento en el número de procesos *alarmante*.
Sabemos que los procesos se crean como directorios dentro de `/proc/`
cuyo *nombre* es un número que identifica el `PID` del proceso. (Podéis
echarle un vistazo, mediante `ls /proc/`). Hemos determinado después de
muchas pruebas que si el número de procesos excede de **200** el
sistema se encuentra en un estado crítico. Vamos a crear un bucle
infinito que compruebe cada 10 segundos si se han superado los procesos,
en caso de que se haya superado, debe mostrar un mensaje.

Pistas:

-   Contar los directorios se puede hacer entubando la salida de `ls`
    con alguna opción para que sólo muestre los directorios al comando
    `wc -l`.
-   Para esperar 10 segundos podemos ejecutar el comando `sleep 10`.

Ejemplo de ejecución:

```shell
aberlanas@moria:~$./bucle-02.sh
* Testing /proc/ : 139 processes -> OK
* Testing /proc/ : 199 processes -> OK
* Testing /proc/ : 213 processes -> DANGER
* Testing /proc/ : 139 processes -> OK
```

Para pararlo debemos tener que pulsar `CTRL+C`.

\newpage
# Ejercicio 17

Los técnicos a las órdenes del insigne **J\'aviher Dar\'Oh-Qui** que
están instalando el cableado de la empresa *Logaritmos Neperianos para
el Futuro*, necesitan un pequeño `script` que al ejecutarlo pregunta al
usuario si quiere ver los colores del cable **Cruzado** o **Directo**.

Pulsando para elegir la `d` o la `c` o salir del `script` pulsando la
`q`.

  Opción    Letra
  --------- -------
  Directo   d
  Cruzado   c
  Salir     q

A continuación, dependiendo de lo que ha seleccionado el usuario irá
mostrando cada *medio segundo* y *en orden* los *colores* de los cables
que tienen que conectar en el `RJ45`. Los colores han sido explicados
por el muy honorable Capitán **J\'aviher**, debéis buscar en sus apuntes
el conocimiento necesario para poder realizar este `script`.

Una vez mostrado la configuración de los colores, debe *volver al menú
inicial* y volver a preguntar por qué tipo de cable queremos ver.

Tras cada ejecución debe mostrar el mensaje:

`Alabado sea *J'avih-er, y sus enseñanzas prácticas, hemos consultado NUMERO_DE_CABLES`

Donde `NUMERO_DE_CABLES` debe ir incrementándose tras cada ejecución que
hagamos sin salir del `script`.

![](./imgs/daroqui.jpg)

\newpage
# Ejercicio 18

El script debe pedir primero un número, que llamaremos `BASE`, luego
otro que llamaremos `MULTIPLICANDO`, y por último un tercero que
llamaremos `REPETICIONES`.

El `script` debe devolver el resultado de multiplicar la base por el
multiplicando y debe mostrarlo por pantalla tantas veces como se ha
pedido en `REPETICIONES`.

Ejemplo de ejecución

```shell
aberlanas@moria:~$./bucle-04.sh
* Indica la base : 15
* Indica el multiplicando: 4
* Indica las repeticiones: 3
60
60
60
```

\newpage
# Ejercicio 19

La estación de tren de Requena tiene un sistema automatizado de vagones
que se llenan con la uva que se recoge en la vendimia. Vamos a realizar
un script que calcule el número de vagones necesarios para llevar la
uva. Para ello el script nos pedirá las **toneladas** de Uva que vamos a
cargar (sin decimales y redondeando hacia arriba). A continuación nos
ofrecerá a elegir 3 modelos de vagón:

-   Modelo Titan (tecla `t`).
-   Modelo Paquidermo (tecla `p`)
-   Modelo Albatros (tecla `a`).

Seleccionaremos el modelo pulsando la tecla indicada.

Si no se pulsa una tecla de las indicadas, volver a preguntar por el
modelo.

A continuación, sabiendo que cada modelo es capaz de transportar los
kilos de uva que se muestran a continuación, indicad cuantos vagones son
necesarios para transportar la Uva que se ha indicado previamente.

  Modelo de Vagón   Capacidad
  ----------------- ------------
  Titán             1200 kilos
  Paquidermo        900 kilos
  Albatros          300 kilos

![](./imgs/requena.jpg)

\newpage

# Conceptos
Como ya sabéis la orden `ps -ex`, nos muestra todos los procesos que se están ejecutando en la máquina.
Esta orden la podemos enviar a un bucle `while` de tal manera que podemos ir procesando en vez de toda la salida
a la vez, podemos ir procesando cada línea.

```shell
#!/bin/bash

ps -ex | while read linea; do
	echo " * Estamos procesando esta linea : $linea"
done


```

# Ejercicio 20

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY contenga '?' y cuyo PID no tenga un 7.

# Ejercicio 21

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuya TTY no sea '?' y cuyo PID no tenga un 1 o un 2.

# Ejercicio 22

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT acabe en 's' y cuyo PID no tenga un 3 ni un 4.

# Ejercicio 23

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT acabe en 's' y cuyo PID sea impar.

# Ejercicio 24

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT acabe en 's' y cuyo PID sea par.

# Ejercicio 25

Del listado de la salida de `ps -ex`, lista aquellos PIDs de procesos cuyo STAT no acabe en 's', cuyo PID sea impar y cuyo COMMAND tenga una ruta que contenga *al menos dos carpetas*.

# Ejercicio 26

Del listado de la salida de `ps -ex`, suma todo el tiempo que de todos los procesos y devuelvelo en HORAS:MINUTOS, en caso de superar las 24 horas que diga el número de días completos.

# Ejercicio 27

Del listado de la salida de `ps -ex`, cuenta cuantos procesos tienen la palabra 'xfce4' en su COMMAND.

# Ejercicio 28

Del listado de la salida de `ps -ex`, cuenta cuantos procesos tienen la palabra 'terminal' en su COMMAND.

# Ejercicio 29

Del listado de la salida de `ps -ex`, cuenta cuantos procesos no tienen la palabra 'xfce4' en su COMMAND.

# Ejercicio 30

Del listado de la salida de `ps -ex`, suma el tiempo en ejecución de los COMMANDS que tengan `snap` o `libexec` en su COMMAND. Devuelvelo en HORAS:MINUTOS, en caso de que superen las 24
horas, mostrar el número de días.
