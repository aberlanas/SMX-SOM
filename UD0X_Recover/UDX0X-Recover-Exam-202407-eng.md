---
title: "Evaluación Extraordinaria"
author: [Angel Berlanas Vicente]
date: 
subject: "Markdown"
subtitle: Recover
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Instruciones (En Castellano).

- Leed cada uno de los ejercicios antes de comenzar a hacerlos.
- Buena suerte.

---

\newpage

# Ejercicio 1

We have been hired to provide Microinformatics support for a Scientific Observatory on Climate Change.

We are receiving data from the different Stellar observatories in different places of Spain:

- `Azores`
- `Teide`
- `Algarve`
- `Cabo-de-Gata-Nijar`
- `Islas-Columbretes`
- `Monte-Perdido`
- `Torre-de-Hercules`
- `Cartagena`

These observatories are sending a series of very simple data and we need to store that data in different folders to
then be able to carry out checks.

The data we receive from each observatory are:

- Temperature
- Humidity
- Wind Speed.

That is, if, for example, from the **Columbretes Islands** we are told that we receive: *23 Degrees*, a Humidity of *39%* and the measurement has been taken at *12 meters* above the sea level, the script will be executed as follows:

```shell
./micro-changes-of-air-density.sh Islas-Columbretes 23 39 12
```

You will have to create in the folder `/$HOME/Observatorio/Islas-Columbretes` **three files** (if they did not exist previously) and save datetime and the three measurements in the last line of each file, each measure in their file (of course the path to the previous folders has also been verified).

**The time at which the measurement has done in the format: YYYYMMDD-HHmm.**

\newpage

Example:
```shell
user@machine:~$./micro-changes-of-air-density.sh Islas-Columbretes 23 39 12
- Folder Observatorio not exists ... Creating it
- Folder Islas-Columbretes not exists ... Creating it.
- Storing 20240617-0933 , 23 in /$HOME/Observatorio/Islas-Columbretes/Temperatura.list
- Storing 20240617-0933 , 39 in /$HOME/Observatorio/Islas-Columbretes/Humedad.list
- Storing 20240617-0933 , 12 in /$HOME/Observatorio/Islas-Columbretes/Velocidad.list
```

Assuming the next minute we will execute another measurement:

```shell
user@machine:~$./micro-changes-of-air-density.sh Islas-Columbretes 24 40 12
- Folder Observatorio exists ... nothing to do.
- Folder Islas-Columbretes exists ... nothing to do.
- Storing 20240617-0934 , 24 in /$HOME/Observatorio/Islas-Columbretes/Temperatura.list
- Storing 20240617-0934 , 40  in /$HOME/Observatorio/Islas-Columbretes/Humedad.list
- Storing 20240617-0934 , 12 in /$HOME/Observatorio/Islas-Columbretes/Velocidad.list
```

With the aim of taking measures in a "*homogeneous*" way, we have provided that when a measure is introduced, it is
show a summary of all Observatories with the following information (**per observatory**):

- How many measurements have been taken.
- What is the time of the last measurement.
- What is the maximum temperature entered (whithout the date).
- What is the lowest humidity introduced (whithout the date).
- What is the maximum wind speed (whithout the date).

> [SUBMIT] : The Shell Script named : `micro-changes-of-air-density.sh`

\newpage

## Users and Groups

They have hired a series of Collaborators in the Team of scientists who are helping us manage the different
measurements that come to us from observatories.

| User | Password | Groups |
|------|----------|-------|
| isla | todoMar | cientificos, oceanos |
| peninsula | todoTierra | cientificos, continentes|

You must create the users and groups on the system in order to make the following modifications to the previous script:

- If `--user` is indicated as the fifth argument, you must establish the **appropriate permissions** on all the files of the observatory on which it is located
 saving information (see below).
- If it is passed an Observatory that is not from the list above, it should display a message and fail.
- If `--copy` is indicated as the second argument, you must copy the entire folder `/$HOME/Observatorio` to the folder `/$HOME/Backups-Observatorio/YYYY-MM-DD_HH-mm/` and set the permissions. `rwxr-x---` to all folders and files in the *copy* (you must create the folder with date if it does not exist).

### appropriate permissions

- The files and folders must belong to the user "islands" or "peninsula", depending on the location of the observatory.
- The group will always be "scientists", except in "Teide" which will be oceans.
- The owner must always be able to read, write and execute, the group only read and others must be able to access the information but not modify it.

> [SUBMIT] : The Shell Script named : `micro-changes-of-air-density-with-users.sh`.

\newpage

# Exercise 2

Add 3 2GB virtual hard drives to the IslandOS virtual machine and create partitions on each of them.

The three disks will be partitioned as follows:


| Hard Disk | Number of Partitions | FileSystem |
|------------|-----------|---------------------|
| `/dev/sdb`   | 2         | ext4 ambas|
| `/dev/sdc`   | 1         | Fat32|
| `/dev/sdd`   | 2         | NTFS ambas |

From the `/dev/sdc` and `/dev/sdd` partitions create 3 mount points on `/mnt/` called:

- `fat`
- `ntfs_uno`
- `ntfs_dos`

For `/dev/sdb` partitions, create their mount points in `/observatory/backups` and `/observatory/measurements`

Modify the `/etc/` file that contains the mount point information and have everything mount at boot.

Create a ShellScript that gets the size of the different mount points of the 4 disks (yes, we have `/` too). You will need to check that the mount points are created, that we are running the script with privileges, and that we have the partitions correctly created and formatted.


> [SUBMIT] : The Shell Script named : `filesystem-observatorio.sh` and the file modified with the mountpoints.

