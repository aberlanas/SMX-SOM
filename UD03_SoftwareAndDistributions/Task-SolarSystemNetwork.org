#+Title: Net Tools - Solar System Network
#+Author: Angel Berlanas Vicente

#+LATEX_COMPILER: xelatex
#+EXPORT_FILE_NAME: ../PDFS/Task-SolarSystemNetwork-CLIL.pdf

#+LATEX_HEADER: \hypersetup{colorlinks=true,urlcolor=blue}

#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \fancyhead{} % clear all header fields
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead[R]{1-SMX:SOM - Exam}
#+LATEX_HEADER: \fancyhead[L]{NetTools}

#+LATEX_HEADER: \usepackage{wallpaper}
#+LATEX_HEADER: \ULCornerWallPaper{0.9}{../rsrc/logos/header_europa.png}
#+LATEX_HEADER: \CenterWallPaper{0.7}{../rsrc/logos/watermark_1.png}

#+LATEX: \newpage


* Task 00: Preparing enviroment

Create a Folder in your ~$HOME~ directory named: ~SolarWorkspace~. Inside of it,
the file downloaded from *Aules* with the Network Information about our **Solar System Network**.

Create a Shell Script inside the folder with the name: ~solarNet.sh~


* Task 01: Getting Info

From the file ~SolarSystem.net.csv~ that you can find in your Aules,
create a ShellScript that if the parameter given is ~-i~, using *relative path* from
your ~$HOME~ directory displays all the Planets from the file.

Example:

~smx@machine:~/SolarWorkspace$./solarNet.sh -i~

#+BEGIN_SRC

  * Welcome to the Solar System Network Information Appliance
  * The planets are:

  - Earth
  - Mars
  - Venus
  - Mercury
  - Saturn
  - Jupiter
  - Neptune
  - Uranus
  
#+END_SRC

\newpage
* Task 02: Getting planet info

Improve the previous script, now the script must accepts the parameter: ~-p~ followed
by a Planet Name (in lowercase or uppercase) and then the Script must display
the Network Address for this planet.

Examples:

~smx@machine:~/SolarWorkspace$./solarNet.sh -p mars~

#+BEGIN_SRC

  * Welcome to the Solar System Network Information Appliance
  * The planet Mars has the next IP : 192.168.42.2
  
#+END_SRC

~smx@machine:~/SolarWorkspace$./solarNet.sh -P uRanuS~

#+BEGIN_SRC

  * Welcome to the Solar System Network Information Appliance
  * The planet Uranus has the next IP : 192.168.42.128
  
#+END_SRC

