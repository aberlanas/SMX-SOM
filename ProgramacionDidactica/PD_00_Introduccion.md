---
title: Sistemas Operativos Monopuesto
subtitle: "Curso 2023-2024"
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
abstract: |
          Programación didáctica para el primer curso del Grado Medio de Sistemas Microinformáticos y Redes. 
          Impartida en el IES La Sénia.
          Curso 2023-2024.
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\newpage

\tableofcontents

