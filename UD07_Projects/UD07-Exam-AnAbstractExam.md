---
title: "[ Unit 07 ] : Un Examen Abstracto"
author: [Angel Berlanas Vicente]
subject: "Abstract"
keywords: [Markdown, Permissions, Files]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Un Examen Abstracto

Leed todos los ejercicios antes de comenzar, todos puntuan lo mismo. Elena, que no tiene AOF, cuenta con las salidas volcadas ya a ficheros.

Cread un esqueleto básico del script en el ejercicio 1 e id añadiéndole comprobaciones (tal y como habéis hecho estos últimos días. Todos los ficheros
temporales, carpetas y tal que se creen en anteriores ejecuciones del script han de ser borradas para que no vaya generando basura informática.

## Ejercicio 1

Cread un script llamado : `aeroabstract.sh` que como primer parámetro acepte la ruta la Base de Datos (en caso de Elena, ruta a la carpeta que contiene
sus ficheros de trabajo).

## Ejercicio 2

El script ha de aceptar como segundo parámetro : `--ejer2`, y obtener el listado de los Aeropuertos de España y para cada uno de los resultados, crear si no existe una carpeta en `/tmp/ejer2-HHMM/`,
cuyo nombre sea el del Aeropuerto.

Nota : HHMM -> Es HorasMinuto : 0834 del tiempo de ejecucion.

## Ejercicio 3

El script ha de aceptar como segundo parámetro : `--ejer3`, si se le ha indicado ese argumento, entonces deberá obtener el listado de los 5 aviones que pueden cargar más en su bodega. Ese listado ha
de procesarse a continuación y devolver la carga en *NewtonsPorKiloPondio* que es una medida que están estándarizando que consiste en dividir el Número de Kilos de carga, por el número de carácteres del nombre del avion.

## Ejercicio 4

El script ha de aceptar como segundo parámetro : `--ejer4`, en ese caso del listado de las 3 Compañías con más aviones obten aquellas cuyo número de aviones sea múltiplo de 3.

## Ejercicio 5

El script ha de aceptar como segundo parámetro : `--ejer5` y un tercer parámetro : `LETRA` , en ese caso del listado de convenios ordenados alfabéticamente, imprime sólo aquellos que contengan la letra indicada
como argumento.
