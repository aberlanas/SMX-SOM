---
title: "[ Unit 07 ] : Binding Concepts for the Win(dows)"
author: [Angel Berlanas Vicente]
subject: "Abstract"
keywords: [Markdown, Permissions, Files]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Windows Tiny 11 Installation

Using the ISO at the repository (tic.ieslasenia.org) of Windows Tiny 11, create a MV in VirtualBox
with the next features:

- 20 GB of Disk.
- User : barbanegra
- Pass : barbanegra
- CPUS : 2 Cores.
- RAM : 2048

# PowerShell

Run PowerShell as an Administrator (Right-Click over the icon) and execute:

```PowerShell
Set-ExecutionPolicy unrestricted
```

In order to allow the PowerShell Script executions.


# DuckDuckgo and Scite

Install the DuckDuckGo Web Browser and the Scite Text Editor. Add the two apps to the Panel.

# PowerShell Scripting

## whereismyhome.ps1

Create a PowerShell Script (.ps1) that displays the contents of the Current User Personal Folder.

## ohmydesktop.ps1

Create a PowerShell Script that displays the content of the Current User Desktop Folder

## populatemydesktop.ps1

Create a PowerShell Script that create the next structure on your Desktop Folder:

```
populous\
        Roma\
            RomaPopulation.txt
        Cartago\
            CartagoPopulation.txt
        Hispalis\
            QuienSeFueAHispalis.txt
        Saguntum\
            Castillo.txt
        Lutecia\
            LosFamososCurasanes.txt
````

