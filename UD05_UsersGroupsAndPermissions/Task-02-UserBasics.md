---
title: "Users and Groups Basic Info - 02" 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---
# Task 01

Common user commands, fill the table with information about the commands required to resolve these situations

| Situation | Command |
|-----------|---------|
| login as user | `login` |
| change password | |
| become a user | |
| become a user only for a command execution | |
| go to their homeDirectory | |
| get the groups from a user | |
| get the uid from a user | |
| get the users currently logged into the system| |
| get the groups that are available in the system | |

> [TIP] : This is a Tool Task

# Task 02

Code a Shell Script that accepts only one parameter, if the  paramater is a valid user of the system (system or user), 
display all of the groups that this user is *not belonging to*.

> [TIP] : This is a Tool Task
