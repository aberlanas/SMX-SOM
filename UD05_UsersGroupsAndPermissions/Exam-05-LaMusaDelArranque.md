---
title: "La Musa del Arranque"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01: tarea01-wallpaper-contest

Sabiendo que `dpkg -l` lista todos los paquetes instalados en el sistema y que `dpkg -L NOMBREPAQUETE` lista los ficheros pertencientes a un paquete.

En la Máquina Virtual con **noble numbat** actualizada, crea un shell script que como único argumento acepte un número entero entre el 1000 y el 1500, este será el del usuario al que deberemos copiarle todos los wallpapers (*.png) del paquete que contiene los Wallpapers del concurso a su **Carpeta Personal**. Tendréis que averiguar qué páquete es el del concurso.

Indicaciones:

- Es una buena idea que creeis algun usuario de prueba adicional: "talia", "terpsicore", "euterpe","urania", ... (las musas que luego usaréis).
- Si el usuario no tiene el $HOME creado, que muestre un mensaje de error.

> [SUBMIT] : El script :  `tarea01-wallpaper-contest.sh`

\newpage

# Task 02: tarea02-mayonesa-contest.sh

Crea un Shell script que si no se le pasa ningún argumento, pregunte por qué marca de Mayonesa es la mejor, le anote un punto y finalice.

- En caso de que se le indique el parámetro "`--reset`" debe  establecer todos los votos a 0 de todas las mayonesas.
- En caso de que se le indique el parámetro "`--votes`" debe mostrar la puntuación actual del concurso. 

Lista de Mayonesas del Concurso:

| Mayonesas | 
|-----------|
| Musa      |
| Heinz     |
| Hellmanns |
| Ybarra    |

> [SUBMIT] : El script :  `tarea02-mayonesa-contest.sh`

\newpage

# Task 03: tarea03-arrancando.sh

Haz un shell script que tenga el siguiente comportamiento dependiendo del parámetro:

- `-selfie-dd` : Haga una copia del MBR del primer disco duro (512 Bytes) en el fichero `/root/mbr-selfie-YYYYMMDD.mbr`.
- `-init-desc` : Muestre una pequeña descripción de los pasos de arranque de un SO LinuX.
- `--help`:  Muestre este menú de ayuda (las opciones).

Para otro parámetro que muestre un mensaje de error y salga.

> [SUBMIT] : El script :  `tarea03-arrancando.sh`






