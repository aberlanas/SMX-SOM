#!/bin/bash

# Me pasan 3 argumentos?
if [ $# -ne 3 ]; then
    echo " * Numero incorrecto de argumentos "
    echo "$(basename $0) FILE1 OP FILE2"
    exit 1
fi

# Vale, aqui ya tenemos esto resuelto
# $1 y $3 son ficheros?

if [ ! -f $1 -o ! -f $3 ]; then
    echo " $1 o $3 no son ficheros"
    exit 1
fi

# Vamos ahora con $2
if [ $2 = "and" ]; then
    # Vale, procesemos ahora el fichero buscando usuarios
    cat $1 | while read line; do

	# Me quedo con el login
	aux_login=$(echo $line| cut -d ":" -f1)

	# Me quedo con el id (por si acaso)
	aux_id=$(echo $line| cut -d ":" -f3)

	if [ $aux_id -gt 999 -a $aux_id -lt 65000 ]; then
	    
	    # Vale, ahora buscamos este usuario en el OTRO fichero
	    estaEnElOtroFichero=$(cat $3|grep "^$aux_login:")

	    # Si devuelve algo (lo que sea), lo procesamos
	    if [ -n "$estaEnElOtroFichero" ]; then

		# Obtenemos su id en el otro fichero
		aux_id_otro=$(echo $estaEnElOtroFichero| cut -d ":" -f3)

		# Los imprimimos ^_^
		echo "$aux_login:$aux_id:$aux_id_otro"
	    fi

	    
	fi

    done
    
fi

exit 0
