#!/bin/bash
# Lo primero sera procesar el fichero de usuarios 
# linea a linea:
NUMUSUARIOS=0
cat /etc/passwd | while read linea; do 
    # Ahora en la variable $linea tenemos una unica linea en cada 
    # iteracion del bucle.
    rc=0
    echo $linea| cut -d ":" -f 6 | grep -q "^/home" || rc=1
    if [ $rc -eq 0 ]; then
        let NUMUSUARIOS=$NUMUSUARIOS+1
        AUXUSUARIO=$(echo $linea| cut -d ":" -f 1)
        echo " El usuario $AUXUSUARIO tiene su \$HOME en /home/, llevamos $NUMUSUARIOS"
        echo $NUMUSUARIOS > /tmp/totalUsuarios
    fi
done

NUMUSUARIOS=$(cat /tmp/totalUsuarios)
echo " En total hemos encontrado $NUMUSUARIOS con su \$HOME donde se ha buscado"
exit 0
