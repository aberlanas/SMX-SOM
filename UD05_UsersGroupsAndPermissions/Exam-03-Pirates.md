---
title: "I Want to be a Pirate! " 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01

Using the VirtualMachine or your installed machine (in the case that you have laptops and root privileges), 
create the next four users:

| User | Password | Group |
|------|----------|-------|
| guybrush | IWant2BeA31415rate| pirates|
| lechuck  | DisneyMeCopioATope | pirates |
| nelson   | leFaltaUnaPata | navy |
| dutchman | nobodyExpects | navy |
 
Here has you some ideas for the folder structure that you will require during the exam.

![The Black Pearl](imgs/monkey-structure-01.png)\\

\newpage

![The Flying Dutchman](imgs/monkey-structure-02.png)\\


> [TIP] : This is a Tool Task

\newpage
# Task 02

Let's consider a situation where within the :

* `/monkeyisland/port/TheBlackPearl` 
* `/monkeyisland/port/TheFlyingDutchman` 

folders, there are files containing *sensitive information* that should only be accessible 
by specific users or groups. Here's the scenario:

**Situation**:

Within the 

* `/monkeyisland/port/TheBlackPearl` 
* `/monkeyisland/port/TheFlyingDutchman`

there are files named treasure_map.txt and captain_log.txt, respectively. 

These files contain **highly confidential information**. The requirement is to set permissions in such a way that only the `lechuck` user can read and modify `treasure_map.txt` and the other pirates only can read the instructions, while only members of the group `navy` can read `captain_log.txt`, and only `nelson` can modify it.

![Guybrush at Sea](imgs/guybrush-at-sea.png)\\

\newpage

Code a Shell script that:

* Test if the path exists for the two files, if not exists...error message.
* Create the file `treasure_map.txt` in the correct place.
* Set the permissions and ownerships. 
* Create the file `captain_log.txt` in the correct place.
* Set the permissions and ownerships.

> [SUBMIT] : The Script.

\newpage
# Task 03

Using a valid user for each file (from previous task), put the next content in the files:

```shell
===== CONTENTS FOR THE BLACK PEARL ====

This is a pirate Ship

```

```shell
===== CONTENTS FOR THE FLYING DUTCHMAN ====

This is a Navy Ship

```

![Everyone at the docks](imgs/monkey-muelle.jpg)\\


\newpage
Create a Shell Script that using `stat` and `cut`, and so on, accepts only 2 arguments, the first one 
must be a shipname (*TheBlackPearl, or theFlyingDutchman)*. If the folder not exists in the **valid path**, displays an error message.

The second argument must be one valid pirate or soldier (from the previous users).

The script must display if the user has read permissions over that folder and, if it 
is possible for the `treasure_map.txt` or `captain_log.txt` file (if the user can access it).

Every test must be checked in the script.

## Bonus 

Create a Shell script that using a `for` sequence of users, try to access inside the two ships and read the files.
If the user has no permissions, display an error message, in other case, display the content of the file.


> [SUBMIT] : The Shell(s) Script(s) created and a Screenshot of the execution of this Script in the Installed OS


