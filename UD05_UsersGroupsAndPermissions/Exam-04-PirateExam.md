---
title: "Exam Permissions + CSV"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 00 : Melee Island People.
Contrary to popular belief, Monkey Island pirates don't usually allow outsiders to enter their domains. To avoid problems, brawls and fights in general, it has been decided that UNIX groups will be used for the organization of the different parts of the Island. As well as the assignment of pirates to one or more gangs (never more than 3), except for three famous pirates: 

- Guybrush 
- Swordmaster 
- Lookoutmaster

Table of the Pirates of the Monkey Island

| User | Password | Group |
|------|----------|-------|
| guybrush | IWant2BeA31415rate| pirates, rookies, candidates|
| lechuck  | DisneyMeCopioATope | pirates, ghosts |
| swordmaster | LosInsultosSonLaClave | pirates, candidates, ghosts, lookout|
| lookoutmaster | LookoutDot | pirates, lookout|
| bill| Cacerola | pirates, circus|
| alfredo| Cazo | pirates,circus |
| troll | ThouShallNotPass | pirates, candidates |

> [TIP] : This is a Tool Task, you need to create and assign the pirates **only to these listed** groups.

\newpage
# Task 00 : Creating the Frontiers (continuation)

![Melee Island Map](imgs/melee-island.png)\

Using a Shell Script, create in the folder `/TheMonkeyIsland/` the folders of the locations that appear in white color in the previous map.
In case of whitespaces, reemplace them with "-" and the **'s** from **SwordMaster's** could be safely ignored.

In this script, all the folders must belongs to `root:pirates` and the permisssions must be set to :

- Owner can do everything.
- Group can enter all the folders.
- Others can do nothing.

> [SUBMIT] : The Shell Script named : `MonkeyIsland-Step00-Exam.sh`

\newpage
# Task 01 : A little more concretion

Code a shell Script that first of all, test all the Paths from the previous script, and restore the permissions to the 
originals.

Then, asign the permissions to the users and groups in order to allow the next situations:

* **Only** Alfredo and Bill can do everything in the circus, and others can enter the circus (except `root`).
* The Lookoutmaster can read, write and enter at the Lookout Point. The other pirates has no permissions, but the other people can enter it.
* The Troll is the Master of the Bridge, he rules at this point, and no one other permission is allowed to nobody.
* The Sword Master has permissions to read,write and execution in the Emporium, the House and Her House.
* Guybrush is the only one to modify and enter the Beach.
* Lechuck is the only one to have full access the Island, but the rookies can enter and read the folder. 

![Melee Island Map](imgs/monkey-lookout.png)\
For each permission set, the Script must display a message with the permissions of the Path in Octal mode.

> [SUBMIT] : The Shell Script named : `MonkeyIsland-Step01-Exam`.sh and a Screenshot of the execution of this Script in the Installed OS

\newpage
# Task 02 : A list of Pirates

Create a shell script that using `/etc/password` and processing only the users that his/her `id` is greater than 999, replenish a CSV named: `thePirateBayList.csv`with the next fields:

| Column 1| Column 2| Column 3| Column 4 |Column 5| 
|---------+---------+---------+----------+--------|
| Number  | User ID | login   |groups    | $HOME  |

Example (*Assuming the same order of user creation as in the example*):

```shell
1,1000,guybrush,pirates rookies candidates,/home/guybrush
2,1001,lechuck,pirates ghosts,/home/lechuck

...

```

Optional : The first line can be the name of the column:

```shell
number,userID,login,groups,homeFolder
```

> [SUBMIT] : The Shell Script named : `MonkeyIsland-Step02-Exam`.sh and a Screenshot of the execution of this Script in the Installed OS





