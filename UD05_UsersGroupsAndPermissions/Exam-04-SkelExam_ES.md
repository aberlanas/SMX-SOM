---
title: "Exam Skel + Permissions"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 00 : Verduras de base

Vamos a crear y a practicar con una serie de usuarios que son representados por *verduras*. Vamos a susponer que disponemos de 2 usuarios ya creados 
que se llaman :

- calabaza
- pepino

Y contamos con una carpeta en `/etc/skel/` que se llama `verduras/` y que contiene un fichero oculto que se llama `.simimiente` con el siguiente contenido:

```shell
# 
# NOS 
# PLANTAMOS

```

\newpage
# Task 01 : Ordena y crea

Redacta en un `.md` o en `.odt` los pasos ordenados para que esta situación se resuelva de la mejor manera posible. Adjunta capturas de cada una 
de las ejecuciones de cada comando.

* Creamos un grupo *verduras*.
* Creamos una carpeta que deberá copiarse en cada usuario cuando se cree, el nombre es: `huerto/`.
* Añadimos los siguientes usuarios:
  * `patata`
  * `pimiento`
  * `rabano`
* Ponemos todos los usuarios (tanto las verduras de base, como los que acabamos de añadir) al grupo *verduras*.
* `rabano`y `patata`tambien se tienen que añadir a un grupo llamado *excavadores*.

> [SUBMIT] : El fichero con todo el proceso :  `verduras_ordenadas.pdf`

\newpage
# Task 02 : Permisos


Establece los permisos en los huertos de los excavadores de tal manera que las verduras puedan llegar a mostrar el contenido
del fichero `.simiente`, pero no escribirlo. Adjunta captura ejecución de comandos de tal manera que se vea que se ha conseguido
lo deseado (cambiando de usuarios y tal).

Establece los permisos en los huertos de las verduras que no pertenecen a excavadores de tal manera que otros puedan leer, pero los de
excavadores no puedan ni entrar a ver los ficheros que hay.   Adjunta captura ejecución de comandos de tal manera que se vea que se ha conseguido
lo deseado (cambiando de usuarios y tal).

> [SUBMIT] : El fichero con todo el proceso :  `verduras_permisos.pdf`

\newpage
# Task 03 : Un poquito de scripting

Instalad *zsh*.

Usando el fichero `/etc/password`, haced un script que compruebe si se le ha pasado 1 argumento, si no que falle. 

* Si el argumento es `-e` debe listar aquellos usuarios que pertenezcan al grupo *excavadores*, así como comprobar
  si en su carpeta personal existe la carpeta `$HOME/huerto/`. 
* Si el argumento es `-v` debe listar aquellos usuarios que pertenezcan al grupo *verduras*, así como comprobar
  si en su carpeta personal existe el fichero `$HOME/verduras/.simiente`. 
* Si el argumento es `-s` debe listar ordenadas por tamaño de menor a mayor las rutas reales a los siguientes intérpretes de órdenes:
  * sh
  * bash
  * zsh
 
  Indicando su tamaño en bytes.

> [SUBMIT] : The Shell Script named : `script-pepino.sh` and a Screenshot of the execution of this Script in the Installed OS





