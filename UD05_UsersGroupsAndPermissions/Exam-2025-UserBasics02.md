---
title: "Users and Groups Basic Info - Part 2" 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---
# Task 01

Create a Shell Script that accepts only 3 parameters. 

The second parameter must be : **and** or **not** (see below).
The parameters 1 and 3 must be valid paths to files.

You can use the files provided in Aules in order to test the script.

## And

If the second argument is **and** the script must display each *non-system* user that is in *both* files, and their ids:

Example:

```shell
smx@pc:~$./part2.sh ./passwd.from.server and ./passwd.from.client
tic:1000:1000
aberlanas:1002:1003
redqueen:1005:1010
```

## Not

If the second argument is **not** the script must display each *non-system* user that is present **only** in the first file given.

```shell
smx@pc:~$./part2.sh ./passwd.from.server not ./passwd.from.client
alice:1042
bob:1024
```



