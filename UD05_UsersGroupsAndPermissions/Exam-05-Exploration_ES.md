---
title: "Explorando + Permisos"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01: tarea01-pegajoso.sh

Crea un shell script que acepte un único argumento, este será comprobado si se trata de una 
ruta. En caso de que no sea una ruta a un directorio, que falle y muestre un mensaje.

Si es un directorio y existe, que muestre si es relativa o absoluta la ruta y si se trata 
de un directorio con el **sticky-bit** aplicado.

> [SUBMIT] : El script :  `tarea01-pegajoso.sh`

# Task 02: tarea02-privilegios.sh

Crea un shell script que acepte un único argumento, este debera ser comprobado que sea una ruta a un fichero ejecutable, 
en caso contrario : que falle y muestre un mensaje.

Si es un fichero, existe, que muestre si es relativa o absoluta la ruta y si se trata 
de un comando con el **setuid-bit** aplicado.

> [SUBMIT] : El script :  `tarea02-privilegios.sh`

# Task 03: tarea03-encontrando-permisos.sh

Crea un shell script que acepte 2 argumentos, el primero de ellos será un número entre el 000 y el 777.
El segundo una ruta **Relativa** a un fichero o directorio que se encuentre en la carpeta personal
del usuario que está ejecutando el script (variables y tal).

En caso de que el fichero indicado exista, el script debe comprobar si el número que se le indica en 
el primer argumento es una representación (en octal) de más o menos permisos respecto a los que tiene
el fichero en ese momento y mostrar las posibles diferencias.

> [SUBMIT] : El script :  `tarea03-encontrando-permisos.sh`






