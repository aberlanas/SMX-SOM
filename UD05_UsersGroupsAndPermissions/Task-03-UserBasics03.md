---
title: "Users and Groups Basic Info - 03" 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01

Set up your VirtualMachine in NAT connection mode. Using netplan: configure the adapter to use dhcp.

Install the *plucky-desktop* .iso file with the next user as the admin:

| User     | Password |
|----------|----------|
| somadmin | nimdamos |

Now, reboot and enter into the installed system.

Create the next users, with exactly the given passwords:


| Users      | Password   |
|------------|------------|
| charmander | rednamrahc |
| squirtle   | eltrisuqs  |
| metapod    | dopatem    |
| magikarp   | prakigam   |
| horsea     | aesroh     |
| lapras     | sarpal     |
| vulpix     | xipluv     |
| arcanine   | eninacra   |
| kakuna     | anukak     |
| paras      | sarap      |

Add the users to their correct groups:

* bug
* fire
* water

> [TIP] : This is a Tool Task

