---
title: "Users and Groups Basic Exam - 02" 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---


# Task 01

Using the VirtualMachine or your installed machine (in the case that you have laptops and root privileges), 
create the two users from the previous tasks and another few more:

| User | Password |
|------|----------|
| guybrush | IWant2BeA31415rate|
| lechuck  | DisneyMeCopioATope |
| monkey   | MiraDetrasDeTi |
| swordmaster | PeleasComoUnGranjero |

> [TIP] : This is a Tool Task

# Task 02

Using the Calc file provided and the next command line : 

```shell
libreoffice --headless --convert-to csv PATH_TO_ODS --outdir PATH_TO_CUSTOM_TMP
```

![Guybrush at Sea](imgs/guybrush-at-sea.png)\\

\newpage

Code a Shell script that:

* Accepts only 1 or 2 parameters.
* The first one must be a Valid PATH to an ods *file*.
* The second one must be `-l` or `-g`.
* Convert the ODS file into csv and process it line by line.
* For each user line from this csv:
  * Test if the user exists in the system (`/etc/password`).
  * If the user exists: 
    - Test if the homeDirectory is correct and exists at filesystem as a folder.
    - Test if the shellBinaryPath is correct and exists at filesystem and is executable.
  * If the user not exists:
    * Displays a Custom Error Message : `[[ ERROR ]] : User not found `.
* If the second argument is `-l` additionally to the previous operations:
  * For each user from the ODS that exists in the system, display the password from the 
    `/etc/shadow` file.
* If the second argument is `-g` additionally to the previous operations:
  Display the groups from the system that this user could be added (is not belonging to right now).

> [SUBMIT] : The Script.
