---
title: "Permissions"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01

Create a table in Markdown **without internet** that contains the translation of the next words:

You can use an expression, and everyone must be treated in a IT Environment.

| English | Spanish | English | Spanish |
|---------|---------|---------|---------|
|owner    |         | file      ||
|group    |         | device    ||
|others   |         | size      | |
|read     |         | inode     ||
|write    |         | links     ||
|execution|         |acces time| |
|octal    |         |modify     ||
|size     |         |birth      ||
|mode     |         |I/O        ||

> [TIP] : This is a Tool Task

# Task 02

Set at your `sources.list` file the next repositorioes (only), in your **noble desktop** (VM):

```shell
deb http://ubuntu.cica.es/ubuntu noble main restricted universe multiverse 
deb http://ubuntu.cica.es/ubuntu noble-updates main restricted universe multiverse 
deb http://ubuntu.cica.es/ubuntu noble-security main restricted universe multiverse 
deb http://ubuntu.cica.es/ubuntu noble-backports main restricted universe multiverse
```

After this, execute: 

```shell
sudo apt update; sudo apt full-upgrade --yes
```

Prepare a Markdown Document with the full process, and answer the next cuestion:

What is the function of the semicolon (`;`) in the previous command execution?.

> [SUBMIT] : The PDF generated with pandoc.

# Task 03

Create a Shell Script that using `stat` and `chmod`, accepts only 2 arguments, the first one 
must be a `relative path` to a regular file. If the file not exists, displays an error message.

The second argument must be one letter (`-r`, `-w` or `-x`).

The script must display if the user, the group and others has this permission over that file.

Examples of execution

```shell
guybrush@monkey:~$./SOM/Script-Perms.sh ../lechuck/.bashrc -r
Owner : lechuck - Can Read
Group : lechuck - Can Read
Others: -       - Permission Denied
```

> [SUBMIT] : The Shell Script created and a Screenshot of the execution of this Script in the Installed OS

