---
title: "Users and Groups Basic Info - Part 1" 
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---
# Task 01

Create a Shell Script that accepts only 0 or 1 parameter. If a parameter is given, test if it is a path to file. If no arguments given, use `/etc/passwd` file from the system.

Create a Shell Script that generates two `.csv` files: 

- `system-users-YYYYMMDD-HHmm.csv` (`date` is our friend).
- `non-system-users-YYYYMMDD-HHmm.csv`.

For the *system-users*, the script must extract the next information (in this order):

- ID
- Login
- If the user has GECOS: extract the firstname, if not set the value to: *Lagarto*.
- If the user has `/usr/sbin/nologin` as default shell put: *Non-Login-Allow*, in other case: the shell for the user.
- If the user has the `$HOME` inside `/var` put: *Variable*, in other case: the `$HOME` of the User

For the *non-system-users* the info is the same, but adding between login and firstname all the groups that the user belongs to.


