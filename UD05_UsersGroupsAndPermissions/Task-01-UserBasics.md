---
title: "Users and Groups Basic Info"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---

# Task 01

Fill in the table with the commands that allow us to obtain the following *attributes* or *information* from the user (or logged user).

| Attributes       | Command    |
|------------------|:----------:|
| login            |            |
| first name       |            |
| last name        |            |
| home directory   |            |
| user id          |            |
| shell            |            |
| user's groups    |            |


> [TIP] : This is a Tool Task


# Task 02

Install a VirtualMachine with the next OS : *Fedora 41* ( you can found it in the [tic.ieslasenia.org](http://tic.ieslasenia.org)), but the 
the Official Webpage is (if you are working at Home ^_^ ):

 - [Fedora 41](https://fedoraproject.org/es/workstation/download)

And create a document with :

 - The Virtual Hardware Specification for the machine.
 - The Network configuration.
 - The Installation user created (with his/her password).
 - The hostname of the machine: `monkey`

> [TIP] : This is a Tool Task

\newpage

# Task 03

Using the VirtualMachine created at the previous task, add two new users to the system:

| User | Password |
|------|----------|
| guybrush | IWant2BeA31415rate|
| lechuck  | DisneyMeCopioATope |

> [TIP] : This is a Tool Task

# Task 04

Create a shell script that using the commands from the Task 01, displays the information
about these two users: *guybrush* and *lechuck*. In the same order that in the Task 01.

> [TIP] : This is a Tool Task
