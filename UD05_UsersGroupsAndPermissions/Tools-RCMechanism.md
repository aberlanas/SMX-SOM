---
title: "Herramientas - El Mecanismo $rc"
author: [Angel Berlanas Vicente]
date: 
subject: "Permissions"
keywords: [users, groups]
subtitle: Unit 05
lang: "en"
page-background: "../rsrc/backgrounds/background-2025.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-2025.pdf"
---


\maketitle
\tableofcontents

\newpage
# Introduccion

Como **Sysadmins o DevOps**, nos encontraremos muchas veces que desearemos que determinadas órdenes se ejecuten
o determinados bloques de código sean tenidos en cuenta en función de una serie de comprobaciones que muchas
veces no son fáciles de expresar en un contexto de *if-elses*, sobretodo en los lenguajes de script (*los mejores* ^_^).

En este pequeño documento se explica y se comenta la utilidad de desarrollar algún tipo de estructura de control 
que nos permita realizar estas comprobaciones de una manera *estandarizada*.

# Exit status

¿Qué es el *exit status*?, cuando a nivel de programación de sistemas queremos saber si la última operación 
realizada ha tenido *éxito* (es decir, ha ido bien), más allá de consultar con  nuestros ojos humanos la línea de
órdenes, podemos consultar una variable que va cambiando de valor tras cada línea de ejecución del script.

Esta variable contiene el valor **entero** del *estado de salida* (**exit status**) de la línea anteriormente
ejecutada. La variable es : `$?`

Por ejemplo, el siguiente script:

```bash
#!/bin/bash

echo " Esto no fallara " 
echo $?
echo " Lo siguiente si" 
mkdir /tmp/estofallara/es/una/ruta/que/no/existe/ 2>/dev/null
echo $?
```

devuelve la siguiente salida:

```bash
 Esto no fallara 
0
 Lo siguiente si
1
```

> Nota: Dejo a las curiosas y curiosos lectores que busquen : ¿Qué ha pasado con el mensaje de error del `mkdir`?

\newpage

Este mecanismo ( el del `exit status`) es el que nos permite usar "combinaciones lógicas" en los Scripts, vamos con 
una pequeña explicación:

Si queremos mostrar un mensaje cuando algo ha tenido éxito, podemos usar el doble *ampersand* (**AND lógico**):

```bash
mkdir /tmp/Workspace/ && echo " Directorio creado "
```

Esta línea creará la carpeta y como ha podido hacerla (su `exit status`  ha sido `0`), nos mostrará el mensaje que hemos 
indicado con la orden `echo`.

Si queremos que algo se haga cuando **no haya ido bien**, tocará usar la doble barra vertical (**OR lógico**):


```bash
test -e /tmp/ElDirQueNoExiste || echo " [ Error ] : NO Existe "
```

Ahora lo que ocurrirá es que sólo ejecutará el `echo` (segunda parte de la **OR lógica**), si la primera 
parte va mal, es decir, que no la evaluará ni será tenida en cuenta.

¿Todo esto ha quedado claro? Os animo a ejecutar estas pruebas, deduciendo cómo funciona e interiorizando
estos conceptos.

Propongo un pequeño ejercicio:

Elabora un única línea que compruebe que un fichero **No existe o que lo borre**, la ruta puede ser por ejemplo: 

* `/tmp/noExiste.txt`

\newpage

# Exit Status + if 

Pues vamos a juntar ahora esto en la  **más famosa de las estructuras de control conocidas por el ser humano : el *IF***.

Si nosotros establecemos una variable antes de ejecutar una orden que puede fallar *y controlamos el error* con una **OR lógica**, podremos comprobar más adelante si la orden ha funcionado.

¿Un ejemplo? Vamos a ello:

Vamos a suponer que queremos crear una carpeta en el caso de que no hayamos podido crear un determinado fichero:

```bash
#!/bin/bash
RUTAFICHERO="/tmp/carpeta/ficherin.txt"
RUTACARPETA="/tmp/carpeta"

rc=0
touch $RUTAFICHERO || rc=1

if [ $rc -eq 1 ]; then 
    # Si estamos aqui es que no ha podido ejecutar con exito el 
    # touch, cono lo que ha establecido el valor de rc a 1.
    # Si hubiera ido bien lo que habria pasado es que rc vale 0
    
    # Creemos la carpeta pues...
    mkdir $RUTACARPETA

fi

# Ahora seguimos con el script plenty of Amazing and Awesome STuFF
# ...
exit 0 # <- Exit status!!!
```

\newpage
# Buscando patrones

Espero que todo vaya quedando claro. 

¿Y si lo que queremos es buscar que una linea de un fichero contiene 
o *no contiene* determinado patrón para tomar una decisión?. Pues este 
mecanismo también nos sirve (*una verdadera **navaja suiza***), de la 
programación de sistemas.[^1]

[^1]: Esto es mucho más importante de lo que creéis.

Supongamos que queremos hacer un pequeño script que nos diga cuales y cuantos de los
usuarios del sistema tienen una carpeta personal en la carpeta `/home/`.

Vamos a ello:

```bash
#!/bin/bash
# Lo primero sera procesar el fichero de usuarios 
# linea a linea:
NUMUSUARIOS=0
cat /etc/passwd | while read linea; do 
    # Ahora en la variable $linea tenemos una unica linea en cada 
    # iteracion del bucle.
    rc=0
    echo $linea| cut -d ":" -f 6 | grep -q "^/home" || rc=1
    if [ $rc -eq 0 ]; then
        let NUMUSUARIOS=$NUMUSUARIOS+1
        AUXUSUARIO=$(echo $linea| cut -d ":" -f 1)
        echo " El usuario $AUXUSUARIO tiene su \$HOME en /home/, llevamos $NUMUSUARIOS"
        echo $NUMUSUARIOS > /tmp/totalUsuarios
    fi
done

NUMUSUARIOS=$(cat /tmp/totalUsuarios)
echo " En total hemos encontrado $NUMUSUARIOS con su \$HOME donde se ha buscado"
exit 0
```

Os dejo ahora que leáis el código y lo comprendáis (que no es fácil), pero no tengo 
dudas de que seréis capaces de trabajarlo e interiorizarlo si le dedicáis algo de  tiempo[^2].


[^2]: Leer código, y comprenderlo debe ser uno de vuestros objetivos como *sysadmins*.

\newpage
# Ejercicio propuesto 

Dejo planteado el siguiente ejercicio:

Crea un *Shell Script* que de todos los usuarios del sistema, busque aquellos que pertenecen 
a **más de un grupo** y nos muestre:

* Su `login`.
* Su `uid` .
* Su carpeta personal.
* Muestre un mensaje acerca de la existencia o no de su carpeta personal en el sistema de ficheros.



