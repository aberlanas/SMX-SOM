---
title: "[ Unit 05 ] : Distribuciones - Tareas"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01 (0 puntos)

De la lista de [DistroWatch](https://distrowatch.com/?language=ES), elegiremos una de las distribuciones de la lista de los últimos 6 meses (la asignación se hará en clase).

* *Tarea Sin Entrega*.

# Tarea 02 (0 puntos)

Se descargará una ISO de instalación de la distribución elegida.

* *Tarea Sin Entrega*.

# Tarea 03 (0 puntos)

Instalad la distribución elegida y cread un documento de **Markdown** donde se muestren los datos de instalación y configuración que vayamos realizando. 

# Tarea 04 (5 puntos)

Averiguad que gestor de paquetes utiliza vuestra distribución y cread una tabla en el documento de **Markdown** donde se especifiquen los comandos necesarios para realizar las siguientes acciones:

| Acción | Comando |
|:------:|:-------:|
| Ver el contenido de los orígenes del Software |  - |
| Actualizar el catálogo desde los repositorios |  - |
| Instalar un paquete desde los repositorios    |  - |
| Actualizar todos los paquetes desde los repositorios | - |
| Desinstalar un paquete                        |  - |
| Listar los ficheros pertencientes a un paquete | - |

# Tarea 05 (2 puntos)

¿Qué es un paquete Software si estamos hablando de distribuciones GNU/LinuX?, redacta una respuesta **TU** y adjuntala al documento.

* *Tarea Con Entrega: El documento en Markdown/PDF*.

# Tarea 06 (3 puntos)

En Debian y derivadas podemos contar con `dpkg --compare-versions` una herramienta la mar de útil para saber que versión es mayor cuando el nombre de la versión se complica.

Realiza un shell script que pregunte por dos versiones y diga cual es la mayor (o más reciente).

## Enlaces de interés

* [DistroWatch](https://distrowatch.com/?language=ES)
* [Wikipedia - Distribuciones](https://es.wikipedia.org/wiki/Anexo:Distribuciones_Linux)

## Zona de exploración

Instalar el SSH en la distribución elegida y configuralo para que sólo acepte al usuario `smx` desde tu de la máquina real.
	

