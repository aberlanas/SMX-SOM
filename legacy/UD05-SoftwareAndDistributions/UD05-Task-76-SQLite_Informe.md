---
title: "[ Unit 05 ] : SOM meets AOF: SQLite Informe"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01 

Instala **pandoc, sqlite3 y un editor** y todo lo necesario en una máquina virtual con Xubuntu (o en tu equipo si usas portatil con Ubuntu) para realizar 
las tareas que se describen a continuación.

## Contenido del Manual

Se pide la creación del *manual* acerca de cómo se deberían resolver las consultas **9 y 12** del Examen de Diego de SQLite. 

El manual debe realizarse usando **Markdown** y debe contener todo el código necesario para explicar cómo funciona la base de datos, las tablas implicadas, imágenes explicativas, **resultado de las consiultas**,... 

Ese Manual en Markdown ha de ser compilado en PDF usando la plantilla de eisvogel para un aspecto más profesional.

Describid en el propio Manual de Markdown cuales han sido los pasos  y programas necesarios para poder conseguirlo.

## Enlaces de Interés

* [Pandoc](https://pandoc.org/)
* [Eisvogel](https://github.com/enhuiz/eisvogel)

