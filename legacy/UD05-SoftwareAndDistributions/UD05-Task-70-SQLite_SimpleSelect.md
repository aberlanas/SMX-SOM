---
title: "[ Unit 05 ] : SOM meets AOF: SQLite I"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01 (10 puntos)

Sabiendo que el comando sqlite3 acepta argumentos para gestionar la base de datos y realizar diferentes consultas. Realizad un shell script que se llame:

* `som-sqlite-example.sh`

Que realice las siguientes operaciones:

- Compruebe que la ruta a la base de datos que se le indica como primer argumento existe y es un fichero de SQLite válido. Usad `file` y `test` para realizar 
las operaciones.
- Compruebe que las tablas :
  - Genero
  - Pelicula
  - Pelicula_Genero
  
  existen dentro de la base de datos. Para ello podéis usar sqlite3 desde la línea de comandos y usar la tecnología del `grep -q` junto con el `rc=0`.
  
- Y por último ejecute una consulta que muestre todas las películas del Género *acción* y todas las del Género *comedia* (ajustad las mayúsculas y acentos
  para que cuadre con vuestra BD.


Ejemplo de cómo tiene que ser su salida al ejecutarlo:

```shell
smx@maquina:~/Escritorio/bd-utiis/$./som-sqlite-example.sh ../cineserie.db 
 * Welcome to SOM+AOF
 * El fichero cineserie.db existe y es una SQLite DB válida.
 * La tabla Genero existe.
 * La tabla Pelicula existe.
 * La tabla Pelicula_Genero existe.
 * La lista de peliculas cuyo genero es comedia o accion es:
Sharknado
Sharknado
Sharknado 2
Sharknado 2
Sharknado 3
Sharknado 3
Sharknado 4
Sharknado 4
Sharknado 5
Sharknado 5
The Last Sharknado
The Last Sharknado
Apocalipsis Voodoo
Apocalipsis Voodoo
Me compré una moto vampiro
Terrorificamente Muertos
El ejercito de las tinieblas
Iron Sky
Iron Sky
Mad Heidi
Mad Heidi
Estan Vivos!
Fantasmas de Marte
KungFu contra los 7 Vampiros de Oro

```
