---
title: "[ Unit 05 ] : Practica Unificada: Parte 1 y 2"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
mainfont: "../rsrc/sorts-mill-goudy/OFLGoudyStM.otf"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents


# Introducción


Se recomienda leer todas las tareas antes de comenzar a trabajar en ninguna de ellas. 

El uso de la IA para la resolución de Scripts está prohibida. Si se detecta su uso, se dará por fallada toda la práctica y se suspenderá la materia.

Duración : 2 sesiones.

\newpage
# Tarea 01 (3 puntos)

Realiza un Script que usando el fichero de usuarios del equipo habitual en los Sistemas GNU/LinuX, liste todos aquellos usuarios cuyo `uid` sea mayor que 1000 y además sea par. 

En el listado deberá aparecer tanto el usuario como el id del mismo.

Una vez listados, debe mostrar para cada uno de ellos si su carpeta personal se encuentra bajo la ruta `/home/` y en caso afirmativo, contar el número de carpetas que hay en ella
en el primer nivel y mostrarlas en una única línea.

Debe comprobar que el usuario que lo ejecuta tiene privilegios de `root`.

La salida del script ha de ser lo más parecida a la que se muestra a continuación.

Ejemplo de ejecución:

```
smx@maquina~:$./Practica/Tarea01.sh
-- Usuarios
profe (1002)
smx (1004)
asir (1006)
-- Carpetas Personales
 * Usuario : profe
 * /home/profe/ se encuentra bajo /home/
 * Tiene 6 carpetas en su primer nivel
 Escritorio Documentos Plantillas Musica Videos Public
 * Usuario : smx
 * /home/smx/ se encuentra bajo /home/
 * Tiene 3 carpetas en su primer nivel
 Escritorio Documentos Practica
 * Usuario : asir
 * /otros/asir/ no se encuentra bajo /home/
```
 
## Entrega

* El Script realizado.

\newpage
# Tarea 02 (3 puntos)

Se instalará la ISO de Xubuntu que se pondrá en el servidor del Aula (atención a la pizarra).

| Usuario de la instalación |  Password de la instalación |
|---------------------------|-----------------------------|
| somusu		    | sompass                     |

* Se configurará la máquina con Red en **Adaptador Puente** y se establecerán los repositorios a los mismos que se vieron en la práctica del SSH (*capturas*).
* Se actualizará el sistema via línea de comandos (*capturas*).
* Se instalará el SSH en la máquina virtual y se iniciará sesión desde la máquina real a la virtual usando el usuario de la instalación (*capturas*).
* Se añadirá un usuario en la máquina virtual recién instalada con `adduser` y se añadirá al grupo `adm`. Los datos del usuario van a continuación (*capturas*).

| Usuario del SSH        |  Password del usuario para el SSH |
|---------------------------|-----------------------------|
| somssh		    | somssh                      |

##  Bonus de puntuación

Busca en el fichero de configuración del **Servicio SSH** la directiva que restringe los usuarios que pueden iniciar sesión mediante `ssh` en el equipo, y configura 
el servicio para que sólo el usuario `somssh` sea capaz de entrar.

Añade capturas del archivo modificado, y del usuario `somusu` intentando entrar sin éxito y del usuario `somssh` entrando.

## Entrega

* Documento de la instalación y configuración de todo el proceso con las capturas. En PDF.


