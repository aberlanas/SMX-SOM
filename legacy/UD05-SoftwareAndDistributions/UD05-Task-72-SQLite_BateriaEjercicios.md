---
title: "[ Unit 05 ] : SOM meets AOF: SQLite III"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

# Tarea 01

Sabiendo que el comando sqlite3 acepta argumentos para gestionar la base de datos y realizar diferentes consultas. Realizad un shell script que se llame:

* `som-sqlite-test-pelicula_01.sh BBDD.db GENERO`

Que realice las siguientes operaciones:

- Compruebe que la ruta a la base de datos que se le indica como primer argumento existe y es un fichero de SQLite válido. Usad `file` y `test` para realizar 
las operaciones.
- Compruebe que las tablas :
  - Genero
  - Pelicula
  - Pelicula_Genero
  
  existen dentro de la base de datos. Para ello podéis usar sqlite3 desde la línea de comandos y usar la tecnología del `grep -q` junto con el `rc=0`.
- Debe comprobar que el Género indicado como argumento está asociado al menos a 2 películas.

# Tarea 02

Sabiendo que el comando sqlite3 acepta argumentos para gestionar la base de datos y realizar diferentes consultas. Realizad un shell script que se llame:

* `som-sqlite-test-pelicula_02.sh BBDD.db GENERO PELICULA`

Que realice las siguientes operaciones:

- Compruebe que la ruta a la base de datos que se le indica como primer argumento existe y es un fichero de SQLite válido. Usad `file` y `test` para realizar 
las operaciones.
- Compruebe que las tablas :
  - Genero
  - Pelicula
  - Pelicula_Genero
  
  existen dentro de la base de datos. Para ello podéis usar sqlite3 desde la línea de comandos y usar la tecnología del `grep -q` junto con el `rc=0`.
- Debe comprobar que el Género indicado como argumento no está asociado a esa película. 

\newpage
# Tarea 03

Sabiendo que el comando sqlite3 acepta argumentos para gestionar la base de datos y realizar diferentes consultas. Realizad un shell script que se llame:

* `som-sqlite-test-pelicula_02.sh BBDD.db GENERO PELICULA`

Que realice las siguientes operaciones:

- Compruebe que la ruta a la base de datos que se le indica como primer argumento existe y es un fichero de SQLite válido. Usad `file` y `test` para realizar 
las operaciones.
- Compruebe que las tablas :
  - Genero
  - Pelicula
  - Pelicula_Genero
  - RELLENAR AQUI
  
  existen dentro de la base de datos. Para ello podéis usar sqlite3 desde la línea de comandos y usar la tecnología del `grep -q` junto con el `rc=0`.
- Debe comprobar que el Género indicado como argumento está asociado a esa película y que además esa Película tiene al menos 3 actores.

 
---

