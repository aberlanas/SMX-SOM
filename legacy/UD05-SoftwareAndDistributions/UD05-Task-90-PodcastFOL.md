---
title: "[ Unit 05 ] : SOM meets FOL: Podcasting"
author: [Angel Berlanas Vicente, Eva Puigcerver]
subject: "Chat GPT y otras cosas"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Podcasting/Videopodcasting

Vamos a realizar una práctica conjunta con FOL relacionada con la creación de un Podcast o videopodcast en que se comenten 
novedades y cómo funciona el ChatGPT.

Al introducir la materia de SOM, se debe incluir en el trabajo los siguientes elementos:

- Guión técnico.
- Música de fondo.
- Efectos de sonido para bajar el volumen de la música cuando hablen los entrevistadores/alumnado.
- Carátula del podcast.
- Capítulos del podcast.
- Software Utilizado.
- Capturas de pantalla de todo el proceso de creación y edición.
- Memoria del podcast en formato *markdown* + pandoc + eisvogel.
- Mp3/Mp4 resultante. 


## Enlaces de Interés

* [Pandoc](https://pandoc.org/)
* [Eisvogel](https://github.com/enhuiz/eisvogel)
* [Audacity](https://www.audacityteam.org/download/)
* [9 Decibelios](https://www.ivoox.com/podcast-9-decibelios_sq_f1154015_1.html)
