---
title: "[ Unit 05 ] : Practica Unificada : Parte 3"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
mainfont: "../rsrc/sorts-mill-goudy/OFLGoudyStM.otf"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents


# Introducción


Se recomienda leer todas las tareas antes de comenzar a trabajar en ninguna de ellas. 

El uso de la IA para la resolución de Scripts está prohibida. Si se detecta su uso, se dará por fallada toda la práctica y se suspenderá la materia.



\newpage
# Tarea 3: An Infamous Task (3 puntos) (50 minutos)

Durante el siglo XIX, los británicos extendieron sus redes mercantiles desde la Joya del Imperio Británico (La India), hacia el Este y Sur de China, ya que el parlamento 
en un edicto realizado en 1833 canceló el tratado mercantil con la *Compañia de las Indias Orientales*.

![An Infamous Traffic](imgs/anInfamousTraffic.png)\

En esta expansión mercantil hacia China, uno de los productos que más se transportaban hacia Inglaterra fué el *Opio*, por el que se pagaban auténticas fortunas y que se cultivaba
en el interior de China. Sin embargo, los buques mercantes provenientes de Albión no podían llegar hasta el interior de China, así que se establecieron una serie de **Rutas Comerciales** 
que permitieron el transporte del *Opio* desde el interior hacia los puertos.

Se necesita gestionar las diferentes rutas y los diferentes elementos lógisticos necesarios para que el Opio pueda llegar desde el interior hasta los barcos.

Cada una de las rutas se va a gestionar en un fichero. Cada uno de ellos será del estilo: `nombreDeLaRuta.route`.

Diseña un *Script* que permita añadir a cada una las rutas los diferentes elementos que intervienen en ellas (**escribiendo al final del fichero el elemento logístico**). 

Cuando una ruta contenga todos los elementos logísticos quedará *completada* y deberá indicarse en la propia ruta que se ha completado y mostrar un mensaje indicándolo. El script le quitará los permisos de escritura al fichero correspondiente de la ruta, impidiendo así que se escriba en el fichero.

\newpage

|Tabla de posibles **elementos logísticos**|
|------------------------------------------|
|mercancia |
|caravana  |
|proteccion|
|puerto    |
|flota     |


\newpage
A continuación se detalla el comportamiento del script.

* Debe comprobar que se le pasan **dos argumentos**, el primero el tipo de *elemento logístico* que podrá ser uno de los que aparecen 
  en la tabla anterior. El segundo argumento debe ser el nombre de la ruta donde se  introducirá ese elemento logístico (sin el `.route`).
  
  Ejemplo:
  ```shell
  smx@maquina:~$./anInfamousScript.sh mercancia hunan
  ```

* En caso de que no sea ninguna de los elementos logísticos anteriormente descritos, debe mostrar un mensaje y no añadirlo al fichero de la ruta.
* En caso de que la ruta no exista (porque el fichero no exista), debe indicarlo y no añadirlo a ningún sitio.
* En caso de que no se tengan permisos de escritura sobre la ruta, debe mostrarnos el mensaje: "RUTA COMPLETADA".
* En caso de que ya esté ese elemento en la ruta, debe mostrar un aviso y no añadirlo.
* Cuando al añadir *satisfactoriamente* a una ruta el último **elemento logístico** (uno de cada, no importa el orden) esta se *complete*, es 
  decir: contenga todos los elementos logísticos necesarios, deberá mostrar un mensaje de "COMPLETADA" y quitarle los permisos
  de escritura al fichero de la ruta, para prevenir futuros cambios.
 

\newpage
Se muestra a continuación un esqueleto de script:

```shell
#!/bin/bash
# An Infamous Script 
# Licensed under GPL v3.0 or higher.

# Primero creamos las rutas
mkdir -p /tmp/infamous-trades/

# Estas operaciones son inocuas en caso 
# de que los ficheros ya existan
touch /tmp/infamous-trades/guangdong.route
touch /tmp/infamous-trades/guangxi.route
touch /tmp/infamous-trades/hunan.route
touch /tmp/infamous-trades/fujian.route
touch /tmp/infamous-trades/jiangsu.route

# Comprobaciones 
# RELLENAR AQUI

# Anyadiendo (si hace falta)
# RELLENAR AQUI

# Comprobando (si se ha completado)
# RELLENAR AQUI

exit 0
```


## Entrega

* El Script realizado.


