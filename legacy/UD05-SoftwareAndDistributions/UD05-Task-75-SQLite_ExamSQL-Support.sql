--- Ejercicio 1 

--- Listar todas las peliculas de un genero
--- Adecuar el select a lo que necesiteis
SELECT * FROM pelicula
INNER JOIN pelicula_genero ON pelicula_genero.id_pelicula = pelicula.id_pelicula 
INNER JOIN genero ON pelicula_genero.id_genero = genero.id_genero
WHERE genero.nombre = "Western"


--- Muestra el titulo de una pelicula
SELECT pelicula.titulo 
FROM pelicula
WHERE pelicula.id_pelicula = 18

--- Muestra la sinopsis de una pelicula
SELECT pelicula.sinopsis 
FROM pelicula
WHERE pelicula.id_pelicula = 18



--- Ejercicio 2
--- Lista todos los directores de la base de datos
--- Adecuar el select a lo que necesiteis
SELECT * 
FROM persona
INNER JOIN pelicula_director ON persona.id_persona = pelicula_director.id_persona


--- Lista todas las peliculas de un id de director dado
SELECT pelicula.titulo
FROM pelicula 
INNER JOIN pelicula_director ON pelicula_director.id_pelicula = pelicula.id_pelicula
WHERE pelicula_director.id_persona = 13
