---
title: "[ Unit 05 ] : Practica Unificada con Restricciones"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
mainfont: "../rsrc/sorts-mill-goudy/OFLGoudyStM.otf"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---

\maketitle
\tableofcontents

\newpage
# Introducción

Se recomienda leer todas las tareas antes de comenzar a trabajar en ninguna de ellas. 

El uso de la IA para la resolución de Scripts está prohibida. Si se detecta su uso, se dará por fallada toda la práctica y se suspenderá la materia.

Todas las tareas son susceptibles de ser probadas y comprobar si el comportamiento es el adecuado, así que si no hace lo que se pide no se corregirá ninguna de las
partes de las diferentes tareas.

Para la ejecución de todas las tareas, debéis contar con la BBDD que se suministra en el examen : `cineSerieBRedux.db`

----

![Plan Nueve](imgs/plan9cartel.jpg)\

\newpage
# Tarea 1: Sinopsis de Peliculas en Generos (5 puntos)

Realizad un Shell script cuyo comportamiento sea el siguiente:

- Comprobar que se le pasan dos argumentos, en caso contrario que muestre un mensaje y acabe la ejecución.
- Comprobar que el primero de ellos es una ruta hasta el fichero `cineSerieBRedux.db` (puede ser tanto relativa como absoluta).
- Comprobar que el fichero indicado como base de datos existe y es una base de datos SQLite válida.
- Si se le pasa como segundo argumento `clean`, debe borrar (si existe) la carpeta : `/tmp/ddbb/` y todo su contenido, en caso de que no exista la carpeta, que muestre un mensaje.
- Si el segundo argumento *no es* `clean` ha de buscar en la base de datos si existe alguna película con ese género.
- Si existe alguna película, entonces:
	- Crear dentro de la ruta `/tmp/ddbb/` un fichero **.txt** por cada una de las películas con ese género.
	- En cada uno de ellos establecer el contenido a la Sinopsis de la respectiva película.
	
**Notas**

1. Si la Pélicula tiene espacios en el nombre, sustituirlos por guiones bajos "_".

	- "Sharknado 2" $\rightarrow$ "Sharknado_2"
	- "Fantasmas de Marte" $\rightarrow$ "Fantasmas_de_Marte"
	
2. Si se ejecuta dos veces seguidas el script sin pasarle el parámetro `clean` (es decir: *sin limpiar*), deberá indicar que sería una buena idea ejecutarlo 
   e informar al usuario de que el directorio no está limpio y que habrá más películas de las que debería (pero no fallar).
3. Cualquier comprobación no especificada pero necesaria debe implementarse.

\newpage

## Ejemplos de ejecución


```shell
usuario@maquina:~/ExamenSQL$./ejercicio1.sh clean
 * No se  han indicado dos argumentos.
 
 * USAGE: ejercicio1.sh [clean|GENERO] RUTA_A_LA_BBDD
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio1.sh ../cineSerieBRedux.db clean
 * El directorio /tmp/ddbb no existe, no se hace nada.
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio1.sh ../cineSerieBRedux.db clean
 * El directorio /tmp/ddbb existe, se borra su contenido.
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio1.sh ../cineSerieBRedux.db clean
 * El directorio /tmp/ddbb no existe, no se ha ce nada.
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio1.sh ../cineSerieBRedux.db "Western"
 * El fichero ../cineSerieBRedux.db existe y es una SQLite valida.
 * Creando directorio /tmp/ddbb/Western
 * Creando fichero /tmp/ddbb/Western/Fantasmas_de_Marte.txt
 * Estableciendo sinopsis en fichero
```

----


![Manos](imgs/Manosposter.jpg)\

\newpage
# Tarea 2: Directores, enlaces y Películas (5 puntos)

Realizad un Shell script cuyo comportamiento sea el siguiente:

- Comprobar que se le indica como *único argumento* la ruta hasta el fichero `cineSerieBRedux.db` (puede ser tanto relativa como absoluta), **o el parámetro** `clean`.
- Si el parámetro es `clean` deberá borrar los directorios que sean creados en ejecuciones anteriores de este mismo script (ver rutas más adelante).
- Comprobar que el fichero indicado como base de datos existe y es una base de datos SQLite válida.
- Cree en la ruta `$HOME/directores/` una carpeta por cada uno de los diferentes directores de la Base de Datos, usando su nombre como carpeta.
- Cree en la ruta `/tmp/ddbb-enlaces/` una carpeta por cada una de las películas de la base de datos, usando su título como nombre de carpeta.
- Dentro de cada director, cread enlaces a las diferentes películas que ha dirigido.

**Notas**

1. Si el director o la pélicula tienen espacios en el nombre, sustituidlos por guiones bajos "_".

	- "John Carpenter" $\rightarrow$ "John_Carpenter"
	- "Fantasmas de Marte" $\rightarrow$ "Fantasmas_de_Marte"
	
2. Si se ejecuta dos veces seguidas el script sin pasarle el parámetro `clean` (es decir: *sin limpiar*), deberá indicar que sería una buena idea ejecutarlo 
   e informar al usuario de que el directorio no está limpio y salir con un mensaje de aviso.
3. Cualquier comprobación no especificada pero necesaria debe implementarse.

\newpage

## Ejemplos de ejecución

```shell
usuario@maquina:~/ExamenSQL$./ejercicio2.sh
 * No se ha indicado el argumento.
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio2.sh clean
 * Cleaning :
   - /home/usuario/directores/ -> borrado
   - /tmp/ddbb-enlaces/ -> borrado
```

```shell
usuario@maquina:~/ExamenSQL$./ejercicio2.sh ../cineSerieBRedux.db
 * Sanity checks:
 - Creando /home/usuario/directores/
 - Creando /tmp/ddbb-enlaces/
 *  Listando directores
 - Creando /home/usuario/directores/John_Carpenter
 - Creando /tmp/ddbb-enlaces/Fantasmas_de_Marte enlace a /home/usuario/directores/John_Carpenter
....
```

Podéis comprobar si el script se ha ejecutado bien haciendo un tree de la carpeta `directores`.

![Resultado de ejecutar tree sobre la carpeta con sólo John Carpenter](imgs/ejemplo-enlaces.png)\

En el ejemplo se ha supuesto que sólo hay una película y un director en la BBDD, vosotros tendréis muchos más. 


