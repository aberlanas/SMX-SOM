---
title: "[ Unit 05 ] : Distribuciones - Windows 11"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01 (0 puntos)

De la URL que el profesor os ponga en el proyector, descargar la ISO de WIndows 11 e instalarla.

Configuración de la Maquina:

- 50 GB de disco.
- Red NAT.
- Nombre: Win11SOM

* *Tarea Sin Entrega*

# Tarea 02 (0 puntos)

Instalad la ISO elegida y cread un documento de **Markdown** donde se muestren los datos de instalación y configuración que vayamos realizando. A lo 
largo de toda la práctica irán apareciendo tareas y acciones que sería conveniente que guardárais dentro del **Markdown**. 

# Tarea 03 (10 puntos)

Vamos a Investigar varios puntos dentro de este Sistema Operativo (si se puede denominar así). 

Descargad el instalador del LibreOffice (de la página **oficial**).

Utilizando la línea de comandos, situaros en la carpeta donde se encuentra el instalador que acabáis de descargar.

Daros permisos para ejecutar Scripts utilizando el comando `Set-ExecutionPolicy unrestricted`.

Averiguad que argumentos se le pueden pasar al instalador del LibreOffice. Imaginad que vuestro jefe os ha pedido las siguientes cosas:

- ¿Se puede instalar para todos los usuarios del Equipo?
- ¿Se puede realizar la instalación sin que se muestre ninguna ventana?
- ¿Se puede guardar todo el proceso de instalación en un fichero?
- ¿Serías capaz de revisarlo y comprender qué es lo que está ocurriendo en el proceso?

Para todos estos puntos, apuntad un pequeño resumen de acciones e ideas en el documento de **Markdown**.



