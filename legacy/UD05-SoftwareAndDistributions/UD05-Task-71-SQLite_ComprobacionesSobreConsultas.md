---
title: "[ Unit 05 ] : SOM meets AOF: SQLite II"
author: [Angel Berlanas Vicente]
subject: "Distribuciones"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01 (10 puntos)

Sabiendo que el comando sqlite3 acepta argumentos para gestionar la base de datos y realizar diferentes consultas. Realizad un shell script que se llame:

* `som-sqlite-test-pelicula.sh BBDD.db NOMPELICULA [GENERO]`

Que realice las siguientes operaciones:

- Compruebe que la ruta a la base de datos que se le indica como primer argumento existe y es un fichero de SQLite válido. Usad `file` y `test` para realizar 
las operaciones.
- Compruebe que las tablas :
  - Genero
  - Pelicula
  - Pelicula_Genero
  
  existen dentro de la base de datos. Para ello podéis usar sqlite3 desde la línea de comandos y usar la tecnología del `grep -q` junto con el `rc=0`.
- Debe comprobar que la película indicada como argumento aparece en la tabla *Pelicula*.
- Si se le pasa un genero, debe comprobar que la película que se le indica existe y tiene ese género.
 
---

\newpage
Ejemplos de cómo tiene que ser su salida al ejecutarlo:

```shell
smx@maquina:~/Escritorio/bd-utiis/$./som-sqlite-test-pelicula.sh ../cineserie.db Sharknado 
 * Welcome to SOM+AOF
 * El fichero cineserie.db existe y es una SQLite DB válida.
 * La tabla Genero existe.
 * La tabla Pelicula existe.
 * La tabla Pelicula_Genero existe.
 * La Pelicula Sharknado esta en la tabla
```

```shell
smx@maquina:~/Escritorio/bd-utiis/$./som-sqlite-test-pelicula.sh ../cineserie.db Sharknado Drama
 * Welcome to SOM+AOF
 * El fichero cineserie.db existe y es una SQLite DB válida.
 * La tabla Genero existe.
 * La tabla Pelicula existe.
 * La tabla Pelicula_Genero existe.
 * La Pelicula Sharknado esta en la tabla pero no tiene ese Genero
```

```shell
smx@maquina:~/Escritorio/bd-utiis/$./som-sqlite-test-pelicula.sh ../cineserie.db Sharknado Comedia
 * Welcome to SOM+AOF
 * El fichero cineserie.db existe y es una SQLite DB válida.
 * La tabla Genero existe.
 * La tabla Pelicula existe.
 * La tabla Pelicula_Genero existe.
 * La Pelicula Sharknado esta en la tabla y tiene ese Genero (Comedia).
```

**ATENCIÓN**

El script debe ser insensible a mayúsculas y minúsculas y además debe obtener exactamente la salida que se muestra con ese formato.

