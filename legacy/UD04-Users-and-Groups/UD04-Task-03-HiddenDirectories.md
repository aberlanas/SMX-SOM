---
title: "[ Unit 04 ] : Users and Groups : Carpetas Personales y Tipografías"
author: [Angel Berlanas Vicente]
subject: "Markdown"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01

En la carpeta personal de los usuarios hay una carpeta que contiene todas las tipografias instaladas
que están disponibles **sólo** para ese usuario.

Entra en **dafont.com** y descarga el tipo de letra: "Serif Of Nottingham". **NO LA INSTALES, SÓLO DESCARGALA**.

Crea un Shell Script que acepte como argumento la ruta a un fichero *.ttf* (TrueTypeFont), y haga las siguientes operaciones:

- Compruebe si existe la carpeta personal de fuentes, si no existe, que la cree.
- Compruebe que se le ha pasado un argumento, si se le han pasado menos o más de 1, que muestre un mensaje de error y acabe la ejecución.
- Si el argumento que se le ha pasado es la ruta a un fichero *.ttf*, que copie el fichero a la carpeta de fuentes del usuario que ejecuta 
  el script.
  
## Tarea 01: Zona de Exploración

- Además de comprobar que el argumento es un fichero *.ttf*, comprobar que realmente es un fichero de tipo TTF utilizando el comando `file`.
- Renombrar el fichero en la copia a un nombre que **no contenga espacios**:

	**"Fuente chulisima.ttf"** $\rightarrow$ **Fuente_chulisima.ttf**.
	

