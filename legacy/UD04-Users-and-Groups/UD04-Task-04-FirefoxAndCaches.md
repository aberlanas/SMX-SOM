---
title: "[ Unit 04 ] : Users and Groups : Caches y Navegadores"
author: [Angel Berlanas Vicente]
subject: "Markdown"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01

En la carpeta personal de los usuarios (`$HOME`), los diferentes navegadores crean carpetas para almacenar configuraciones, caches, perfiles, etc.
que son utilizados para aportar comodidad a los usuarios que los utilizan.

Vamos a trabajar un poco con estos ficheros y con algunas de las configuraciones que podrían ser establecidas.

Crea un `ShellScript` que cuando se ejecute acepte un único parámetro (en caso de pasarle más o menos, debe fallar y mostrar un mensaje de error).

Ese parámetro puede ser una de las dos siguientes opciones:

- `--firefox`
- `--chromelikes`

A continuación el Script mostrará la *ruta completa* hasta la carpeta del perfil del *usuario actual* del navegador seleccionado (si existe), así como 
el **tamaño en Megas** que ocupa.

A continuación le preguntará al usuario si desea borrarla, mediante una pregunta de (`y/n`). En caso de que el usuario diga que si (`y`), debe borrar
el directorio de ese navegador indicando al finalizar cuanto tiempo le ha costado realizar esa operación de borrado.

Si el navegador se encuentra abierto, debe mostrar un mensaje al usuario y preguntarle si desea cerrarlo, si el usuario indica que sí, debe cerrar el proceso
desde el script, en caso contrario que muestre un pequeño mensaje y finalice sin errores.

## Toolbox

| Comando    | Función |
|------------|---------|
| `readlink` | Mostrar rutas y resolver enlaces |
| `du`       | Mostrar tamaño de ficheros y carpetas|
| `read`     | Preguntar al usuario por un valor, o texto|
| `time`     | Toma el tiempo que le cuesta a un(os) comando(s) ejecutarse  |
| `kill`     | Manda una señal de cierre a un proceso|
  
## Zona de Exploración

- Si establezco una página de inicio en un navegador ¿Dónde se guarda esa configuración?
- Haz un `ShellScript` que si se le indica una URL como argumento, la establezca como página de inicio en el navegador seleccionado (como en el script anterior). 

	

