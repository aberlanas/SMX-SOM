---
title: "[ Unit 04 ] : Users and Groups : SSH Basico"
author: [Angel Berlanas Vicente]
subject: "Markdown"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01

En la carpeta personal de los usuarios ($HOME) se almacenan ficheros que utilizan los programas cuando determinados usuarios (típicamente los que lo acaban de lanzar) realizan operaciones con ellos.

Vamos a trabajar un poco con `SSH`, que es una herramienta la mar de útil.

Esta tarea la vamos a realizar en la máquina virtual, que deberá tener instalada una Xubuntu 22.04. 

* Actualizad los repositorios de Software de vuestra máquina virtual. ¿Cómo? Cambiando el fichero de `/etc/apt/sources.list` al siguente contenido:

```shell
# LA SENIA
deb http://tic.ieslasenia.org/ubuntu jammy main restricted universe multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-updates main restricted universe multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-security main restricted universe multiverse
deb http://tic.ieslasenia.org/ubuntu jammy-backports main restricted universe multiverse
```

* Una vez actualizados los repositorios, vamos a actualizar la lista de paquetes.

```shell
sudo apt update
```

* Una vez actualizados los repositorios, dará unos pequeños errores, debido a que nos falta una arquitectura en la réplica, que quitaremos de nuestro sistema:

```shell
sudo dpkg --remove-architecture i386
```

* Actualizaremos todos los paquetes del sistema.

```shell
sudo apt update; sudo apt full-upgrade --yes
```

* Reiniciamos

```shell
sudo reboot
```

* Y por último instalamos el servidor ssh

```shell
sudo apt install openssh-server
```

Ahora ya podemos iniciar sesión desde nuestra máquina **REAL** utilizando `ssh`.

```shell

ssh usuario@IP_DE_LA_MAQUINA_VIRTUAL

```

Introduciremos la contraseña y ya estaremos dentro del equipo.

Realizad una captura de pantalla de vuestra terminal local con la sesión iniciada en vuestra máquina virtual. Entregadla en Aules.

![Terminal](imgs/steampunk-terminal.jpeg) \

\newpage

# Tarea 02

En el fichero `.ssh/known_hosts` se almacenan las diferentes claves de fingerprint (RSA) de las máquinas a las que nos vamos conectando.

¿Con qué objetivo? Redactalo con tus propias palabras (SABRÉ CUANDO OS COPIAIS).

# Tarea 03

Crea un shell script que al ejecutarlo en la máquina REAL:

- Compruebe que se le pasan 2 parámetros: USUARIO y MAQUINA. En caso de que sean más parámetros o menos, que muestre un error.
- Mueva el fichero `.ssh/known_hosts` al una copia de seguridad de la forma: `.ssh/known_hosts_202302200910` (YYYYMMDDHHmm).
- Inicie sesión mediante ssh en la máquina indicada con el usuario indicado.
- Al salir muestre un mensaje  indicando cuantos segundos ha durado la conexión.

![Pixel Art](imgs/pixelart-terminal.jpeg)\

## Zona de exploración

Genera un par de claves RSA de tal manera que el SSH no te pida contraseña cuando intentes iniciar sesión en el equipo remoto (MV).
	

