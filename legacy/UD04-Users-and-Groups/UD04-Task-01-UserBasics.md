---
title: "[ Unit 04 ] : Basic Permissions in GNU/LinuX"
author: [Angel Berlanas Vicente]
subject: "Markdown"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Introducción

Muchas de las soluciones de los scripts que van a aparecer a continuación están en la página de manual del comando `test`. 
Otras serán la combinación de `ls`, `cut` y algo de astucia por parte de l@s futur@s administrador@s de Sistemas.

Para la comprobación del correcto funcionamiento de los diferentes scripts, deberéis ir creando la infraestructura necesaria 
en el entorno de ejecución.

# Tarea 01

Realiza un Shell script que compruebe si el fichero que se le pasa como primer argumento:

- Existe.
- El usuario propietario es el mismo que el que está ejecutando el script.
- El usuario propietario tiene permisos de lectura y escritura sobre el mismo.

Para cada uno de esos test el script debe indicar tanto si ha tenido éxito como si no.


# Tarea 02

Realiza un Shell script que compruebe si el fichero que se le pasa como primer argumento:

- Existe y es un directorio
- El usuario propietario es el mismo que el que está ejecutando el script.
- El usuario propietario tiene permisos de lectura y ejecución sobre el mismo.

Para cada uno de esos test el script debe indicar tanto si ha tenido éxito como si no.


# Tarea 03

Realiza un Shell script que compruebe si el fichero que se le pasa como primer argumento:

- Existe y es un directorio que se encuentra en el mismo directorio que el propio script.
- El grupo propietario es el mismo que el que está ejecutando el script.
- El grupo propietario tiene permisos de lectura y ejecución sobre el mismo.

Para cada uno de esos test el script debe indicar tanto si ha tenido éxito como si no.

# Tarea 04

Realiza un Shell script que compruebe si el fichero que se le pasa como primer argumento:

- Existe y es un directorio que se encuentra en el mismo directorio que el propio script.
- El grupo propietario es el mismo que el que está ejecutando el script.
- El usuario y grupo propietario tienens permisos de lectura y ejecución sobre el mismo.

Para cada uno de esos test el script debe indicar tanto si ha tenido éxito como si no.


# Tarea 05

Realiza un Shell script que compruebe si el fichero que se le pasa como primer argumento:

- Existe y es un directorio que se encuentra en el mismo directorio que el propio script.
- El grupo propietario es el mismo que el que está ejecutando el script.
- El grupo propietario tiene permisos de lectura y ejecución sobre el mismo.

Para cada uno de esos test el script debe indicar tanto si ha tenido éxito como si no.
