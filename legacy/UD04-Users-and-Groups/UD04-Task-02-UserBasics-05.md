---
title: "[ Unit 04 ] : Users and Groups : Usuarios e Información asociada"
author: [Angel Berlanas Vicente]
date: "2023-02-02"
subject: "Markdown"
keywords: [Markdown, Users, Groups]
lang: "es"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
...

# Tarea 01

Haz un Shell Script que para cada usuario que haya en el equipo, muestre la siguiente información:


- Si su ID es mayor que 999:

	- Login.
	- ID.
	- GECOS.
	- Ruta a la carpeta personal.
	- Ruta al fichero más nuevo modificado por ese usuario en su carpeta personal.

- Si su ID es menor que 1000:

	- Login.
	- ID.
	- Shell por defecto.
	- Número de carpetas en /var/ que pertenecen a ese usuario.

Comandos útiles:

 - `find`
 - `wc`
