---
title: "Exam 01: Buscando ficheros perdidos"
author: [Angel Berlanas Vicente]
date: "2024-04-17"
subject: "Markdown"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Examen 01 : Buscando ficheros perdidos.

Estamos en la empresa "MTG Urza", acabamos de recuperar una serie de ficheros que forman parte de un plan de
recuperación que se hizo hace unos días en la **Sede Central de Serra**.

Sin embargo durante el transporte ha habido un problema y los ficheros de imagen han cambiado de nombre, haciendo complicado
averiguar cual es el fichero que contiene el sistema de ficheros, así como la clave de acceso al **Ordenador central**.

En el último correo antes de que la Sede fuera atacada por las fuerzas de Phyrexia, el Administrador de Sistemas, dijo que la
clave sería el contenido del fichero `urza-power-plant.key` y el tipo de sistema de ficheros en el que está la partición que lo contiene.

Haz un Shell Script que acepte como argumento la ruta a la carpeta en la que están las diferentes imágenes de disco. Que compruebe los
permisos de administrador de la ejecución del propio script y que monte cada una de ellas en `/mnt/searching-for-serra` (comprobaciones
habituales aqui). Para cada imagen ha de montarla, buscar el fichero en su interior y si está presente mostrar por pantalla el tipo de sistema
de ficheros de la misma y en la misma línea el contenido del fichero.

Ejemplo de ejecución:

```bash
usuario@serra:~/$sudo ./searching-serra-pass.sh /home/usuario/Descargas/serra-files/
 * Working with: disco1.img
 * Mounting at /mnt/searching-for-serra/
 * Found it! : /mnt/searching-for-serra/level1/pass/urza-power-plant.key
 * PASS: ext4estoessolounejemplo
```

