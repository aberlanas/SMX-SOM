---
title: "FileSystems 04: No Mercy"
author: [Angel Berlanas Vicente]
date: "2024-04-05"
subject: "Markdown"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Task No Mercy

Estamos en la empresa "MTG Urza", nos acaban de llegar una serie de equipos que tienen instaladas un montón de
máquinas Virtuales.

En algunas de estas máquinas virtuales, existen ciertos ficheros de interés. Pero resulta que no sabemos en cuales
han sido configurados esos ficheros, ya que es posible que el nombre de la máquina virtual no coincida con el/los
sistemas operativos instalados en la misma.

Nos han pedido que realicemos un Script que busque esos ficheros de interés y nos muestre información acerca de los mismos.

¿Cómo debe funcionar?

- Debe comprobar que lo estamos ejecutando con privilegios, o que muestre un mensaje y salga.
- Con el parámetro "`-h`" muestre la ayida.
- Con el parámetro "`-l`" liste todos los ficheros de discos **vdi** que estén "attached" a alguna máquina virtual del usuario.
- Con el parámetro "`-c`" y luego una ruta a un fichero vdi, entonces:
  - Compruebe que el fichero tiene un tamaño superior a 2G, en caso contrario que muestre un mensaje.
  - Compruebe que contiene un disco que contiene una partición que está formateada en `ext4`.
  - Si los dos pasos anteriores son ciertos, que compruebe que el fichero `/etc/apt/sources.list` existe, si no
    que muestre un mensaje indicando que no es una Derivada de Debian.
  - Si existe el fichero, que muestre la versión de "ubuntu" o "debian" que está configurada en el mismo. Si existe más
    de una distribución configurada, que muestre todas (pero sólo una vez cada 1).

![No Mercy](imgs/nomercy2.png)\


