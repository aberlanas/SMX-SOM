#!/bin/bash

# Función para generar contenido aleatorio de un tamaño específico en bytes
generate_random_content() {
    tr -dc '[:alnum:]' < /dev/urandom | head -c $1
}

# Función para generar un archivo de tamaño aleatorio con contenido aleatorio
generate_random_file() {
	local size=$(($((RANDOM % 10241)) + 10240))  # Tamaño entre 10KB y 20KB
    local content=$(generate_random_content $size)
    echo "$content" > "$1"
}

# Función para crear una estructura de carpetas aleatoria
create_random_folders() {
	local depth=$(($((RANDOM % 5)) + 1))  # Profundidad máxima de 5 niveles
    local num_files=100

    for ((i=1; i<=$num_files; i++)); do
        local file_path="$1/file_$i.txt"
        generate_random_file "$file_path"
    done

    if [ $depth -gt 0 ]; then
        for ((i=1; i<=$num_files; i++)); do
            local folder_path="$1/folder_$i"
            mkdir "$folder_path"
            create_random_folders "$folder_path"
        done
    fi
}

# Verificación de la ruta proporcionada
if [ $# -ne 1 ]; then
    echo "Uso: $0 <ruta>"
    exit 1
fi

# Comprobar si la ruta existe
if [ ! -d "$1" ]; then
    echo "La ruta especificada no es válida."
    exit 1
fi

# Crear la estructura de carpetas y archivos aleatorios
create_random_folders "$1"
echo "Se han generado 100 archivos aleatorios en carpetas aleatorias dentro de la ruta proporcionada."

