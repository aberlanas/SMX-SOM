---
title: "FileSystems 02: Warming Up"
author: [Angel Berlanas Vicente]
date: "2024-04-05"
subject: "Markdown"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Task 01

Using `dd` create a file filled with *zeroes, with the next command:

```bash
sudo dd if=/dev/zero of=/newFakeDevice.img bs=1M count=1024
```

What is the final size of the file?

> [TIP] : This is a Tool Task

# Task 02

Using `fdisk` and `mkfs.ext4` commands, create a partition with the *ext4* FileSystem inside the file.

> [TIP] : This is a Tool Task

# Task 03

**I+D+i**, searching on the Internet, try to mount in the "Real Machine" a Virtual Hard Disk from the VirtualBox environment. It is possible?

> [TIP] :  This is a Tool Task

# Task 04

Create a markdown guide to format and create a partition(s) or devices in Disks or Mountpoints. The commands explained must be:

- `fdisk`
- `mkfs.*` (choose two)
- `mount`
- `sync`

> SUBMIT : The Markdown Guide

