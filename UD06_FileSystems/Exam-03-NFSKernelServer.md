---
title: "Exam 03: NFS - Need For Speed"
author: [Angel Berlanas Vicente]
date: "2024-05-02"
subject: "NFS Kernel Server"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Exam 03 : NFS 

Configure a folder in `/etc/exports` -> `/srv/nfs-server/` and put the PDF of the exam (*this file*), and **at the end of the exam** the **ShellScript** 
with the name : more-or-less-NUMBER.sh where NUMBER is your number assigned in the blackboard.

If everything fails, upload the script to Aules.

The Script must perform several checks and operations:

- Check if the user that is running it has root permissions (if not, exit with a message).
- Check if the folder `/client/first-step` exists and create it if not.
- Check if the folder `/client/second-step` exists and create it if not.
- Check if the folder `/client/third-step` exists and create it if not.
- If any file is inside these directories (from previous script executions), remove all.
- Mount at `/client/first-step/` the `/srv/nfs-server/` previously configured (with the IP from loopback).
- Copy the `/etc/exports` file to `/client/second-step/`.
- Redirect the output of the `mount` command to a file called : `mount-status.txt` at `/client/second-step/`
- Redirect the output of the `fdisk -l` command to a file called : `fdisk-status.txt` at `/client/third-step/`
- Count how many devices of type `disk` are installed on the machine (using the previous generated file). 

