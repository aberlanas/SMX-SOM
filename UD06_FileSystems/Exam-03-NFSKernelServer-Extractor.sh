#!/bin/bash
#
#


echo " * Welcome to the Script extractor "

mkdir -p /nfs-exam-extractor/
sudo umount /nfs-exam-extractor/*

for ipa in 192.168.5.116:Joel 192.168.5.126:Jesus 192.168.5.184:Pau 192.168.5.173:Jorge 192.168.5.182:Ivan 192.168.5.121:Alex 192.168.5.189:HST 192.168.5.183:Alvaro 192.168.5.150:Vileneuve 192.168.5.144:Elena ; do 

	echo " Accediendo : $ipa"
	ALUMNO="$(echo $ipa | cut -d ":" -f2)"
	IP="$(echo $ipa | cut -d ":" -f1)"
	echo " Alumno -> $ALUMNO : IP -> $IP"

	mkdir -p /nfs-exam-extractor/$ALUMNO

	mount $IP:/srv/nfs-server/ /nfs-exam-extractor/$ALUMNO/

done


tree -f /nfs-exam-extractor/

DATE=$(date +%Y%m%d_%H%M)
cp -r /nfs-exam-extractor/ /exam-extractor-$DATE/

exit 0
