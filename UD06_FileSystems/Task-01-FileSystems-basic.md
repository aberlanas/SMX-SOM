---
title: "FileSystems: The Beginning"
author: [Angel Berlanas Vicente]
date: "2024-04-05"
subject: "Markdown"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Task 01

Download a GNU/LinuX distribution from these:

- Archlinux.
- Manjaro.
- Debian.
- CentOS.

You must download the *iso files* from their projects home page.

> [TIP] : This is a Tool Task

# Task 02

Install the Distribtion in a VM with the requeriments for your chosen distro. Please remove all other distributions used in *SOM*,

> [TIP] : This is a Tool Task

# Task 03

Get the FileSystem type from your **/** directory. From where is mounted?.

> SUBMIT: A simple Shell Script that obtains the info which is required.

# Task 04

Code a Shell Script that displays all *filesystems* mounted from `/dev/` and ask the user for one of them, then, display the next info about the mounted fs:

- Mountpoint.
- Is a block device? (yes/no).
- FileSystem Format (ex4,vfat,...)
- Disk Usage.

> SUBMIT : The Shell Script created and a Screenshot running it in the Installed OS

