---
title: "FileSystems-OST: mount"
author: [Angel Berlanas Vicente]
date: "2024-04-05"
subject: "Markdown"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Task 01

Listen the Song in Aules about `mount` command, and make a simple guide for yourself.

> [TIP] : This is a Tool Task

