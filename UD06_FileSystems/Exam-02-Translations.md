---
title: "Exam 02: Lost in Translation"
author: [Angel Berlanas Vicente]
date: "2024-05-02"
subject: "Filesystems, devices and Apertium"
keywords: [Markdown, FileSystems]
lang: "en"
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage: true,
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-06-task.pdf"
---

# Exam 02: Lost in Translation

We are working at International Company (British Council), and for the next exam (today) the teacher
needs a special file that was provided by an editorial *long time ago* (several years, maybe).

This file was stored in a CD-ROM, an *ancient* device that was able to store a lot of information. But
unfortunately, the Original CD-ROM was lost, but the previous *sysadmin* made a raw copy of this device
in a `.iso` file: `lostInTranslation.iso`.

In this image, you can find a *csv* file that in three columns present the three impersonal forms
of several verbs in *spanish*.

## Installation

```bash
sudo apt install apertium-lex-tools cg3 apertium apertium-spa-cat
```

## Coding

Using the package: `apertium`, and installing the data for translation *Spanish-Catalan* pair:

 * Create a Shell script which accepts as the first argument, the path to the ISO file.
 * checking if it exists.
 * And mount the **iso** at `/cdrom/`.
 * Access to the file that is inside.
 * And reading this file line by line, create another file in `/tmp/` that contains the original columns,
   and next to each one the translation of each word in it.

See the example:

```bash
usuario@lostInSom:~/$sudo ./lost-in-translation /home/usuario/Descargas/lostInTranslation.iso
 * Working with: /cdrom/csv_verb.csv
 * Creating the file: /tmp/found-in-translation.csv
 * Line By Line Processing
 Infinitivo, Infinitiu,Participio,Participi,Gerundio,Gerundio
 hablar,parlar,hablado,parlat,hablando,parlant
 ....
```

