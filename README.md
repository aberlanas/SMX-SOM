# SMX-SOM

Recursos para el Módulo de Sistsemas Monopuesto del Ciclo de Grado Medio de Sistemas Microinformáticos y Redes 
---

## Entorno y Desarrollo

![Vim](https://img.shields.io/badge/VIM-%2311AB00.svg?style=for-the-badge&logo=vim&logoColor=white)
![Ubuntu](https://img.shields.io/badge/Ubuntu-E95420?style=for-the-badge&logo=ubuntu&logoColor=white)

## Herramientas

[![made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-1f425f.svg)](http://commonmark.org)
[![made-with-bash](https://img.shields.io/badge/Made%20with-Bash-1f425f.svg)](https://www.gnu.org/software/bash/)

## Filosofía

[![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](https://github.com/Naereen/badges/)

---

## Unidades

| UD   | Nombre |  
|------|--------|
| UD01 | Conceptos Básicos y Virtualización |
| UD02 | Shell |
| UD03 | Software y Actualizaciones |
| UD04 | Arranque, procesos y red |
| UD05 | Usuarios, grupos y permisos |
| UD06 | Sistemas de Ficheros |
| UD07 | Proyecto Integrador |

## Comandos 

```shell
# Ejecutar teniendo en cuentas las rutas relativas

pandoc fichero.md -o fichero.pdf --from markdown --template ../rsrc/templates/eisvogel.tex --listings

```
## Makefile

Debido a la gran cantidad de unidades y utilidades que se presentan en este repositorio, se ha creado un pequeño Makefile para la ayuda a la generación de los diferentes recursos en PDF, HTML, etc. desde Markdown.

## Construyendo con Pandoc

Debido a la gran cantidad opciones y parámetros que se van utilizando en los diferentes ficheros de Markdown. Es necesario instalar en el equipo donde se vaya a generar la documentación en PDF los siguientes paquetes:

* pandoc
* texlive-extra-utils
* texlive-lang-spanish 
* texlive-latex-extra
* texlive-fonts-extra

### Actualizado a Oracular : 24.10

```shell
sudo apt install pandoc \
	     texlive-extra-utils \
		 texlive-lang-spanish \
		 texlive-latex-extra \
		 texlive-fonts-extra

```

## Algunas Utilidades

* vim
* nvim
