---
title: Practica 04 - Shub-Niggurath y sus vástagos
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Tarea : Pistas hacia Arkham

Vamos a seguir investigando el caso de Charles Ward Dexter (posible homenaje a Charles Dexter Ward, pero es un misterio). Las últimas semanas estuvo muy pendiente
de los sistemas de la empresa que vigilaban las instalaciones mentales de Arkham. Se ha contactado con los médicos de la institución pero no parecen muy comunicativos.

Tal vez la única esperanza de averiguar qué es lo que ocurre es que encontréis las pistas necesarias en los `Scripts` que ha dejado en los
sistemas de vigilancia.

Recordad que la técnica que podéis (debéis) utilizar es, crear el script:

1. script-Niggurath-01.sh
4. ...

Y copiais el contenido del script al script, le daís permisos de ejecución y lo probáis para ver **qué** es lo que hace.

Algunos comandos útiles:

- `nano FICHERO.sh` 	
- `chmod +x FICHERO.sh`
- `./FICHERO.sh`

Donde `FICHERO.sh` debe ser sustituido en cada caso por : script-arkham-01.sh, script-arkham-02.sh, etc...

¡Sed ordenad@s!

\newpage

A medida que nos internamos en Arkham vamos conociendo a los habitantes de esta oscura ciudad. Una de las pistas que estábamos siguiendo nos lleva hasta
Thelonius Wolf, un criador de perros de la raza *husky*, que roporciona perros guardianes a muchas de las casa de Arkham para protegerlas durante las noches.

Este criador está *especialmente pendiente* del número de cachorros en las camadas, ya que cuando el número de cachorros que nacen de una perra es múltiplo 
de 3 debe ofrecer un pequeño ritual a lo que él llama : **La Negra Cabra de los Bosques con sus Diez Mil Vástagos**.

![Shub Niggurath](imgs/shubniggurat.jpg)\

Debemos crear un Script que genere un número aleatorio de cachorros (ver fragmento del *SCRIPT* más adelante para obtener pistas), por cada uno de los dias del año (no importa si es bisiesto).

Además debemos contar cuantas veces se ha cumplido la maldición de *Shub-Niggurath* a lo largo del año, según lo descrito anteriormente, 
mostrando al final del Script cuantas veces se ha ido realizando. 

El Ritual de Expiación consiste en *mostrar por pantalla* cuantos Cachorros han nacido este año en **TOTAL**, hasta ese momento.

Además es importante tener en cuenta que si el día en el que nacen los cachorros es *luna llena*, no se debe realizar el ritual, sino que debe mostrar el mensaje: 

*SALVADOS POR LA CABRA, SALVADOS POR SHUB-NIGGURATH*

Podéis tener en cuenta que el día 1 de Enero siempre es luna llena.

## Script Niggurath

```shell
#!/bin/bash

NUMERO_CACHORROS=$(($RANDOM%9+1))
echo $NUMERO_CACHORROS

exit 0 
```



