---
title: Task For of Fors
subtitle: Unit 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Task 01

Create a Shell Script that ask the user about a number, and display the Multiplication table of this Number until the 11.

# Task 02

Create a Shell Script that ask the user about a number and display all the odd numbers between 1 and this number.

Useful commands:

* `seq`
* `let resto=dividendo%divisor`

# Task 03

Create a Shell Script that list all the folders on `/`, ask the user about one of them, and then if the folder exists list this folder with the next checks about each file of folder inside of it:

- If the file that is being checked is a file, then displays the message:
  `This file : PATH_TO_FILE is a file`
- If the file that is being checked is a folder, then calculate the disk usage 
  of this folder.
  
Useful commands:

* `du`
* `ls -1`

# Task 04 

Create a Shell Script that list all the numbers between 1 and 100 that are even and multiple of 3.

# Task 05

Create a Shell Script that list all the numbers between 2 and 500 that are odd and multiple of 5.


