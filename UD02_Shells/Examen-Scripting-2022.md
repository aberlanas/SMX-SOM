---
title: Examen - ZombiExam
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

\maketitle
\tableofcontents


# Inicio

Algunas recomendaciones:

- Leer *todo* el examen antes de comenzar.
- Si os atascáis en algún ejercicio, seguid con el siguiente.
- Contáis con todo el material de Internet y vuestras tareas y apuntes, pero no podéis copiaros.
- Todos los ejercicios valen lo mismo.
- Siempre que se necesite un fichero de texto para hacer búsquedas en este examen se hace uso de :
   `ZombieIpsum.txt`.

¡Mucha suerte a tod@s!

\begingroup
\centering
![Zombie Ipsum](imgs/zombieIpsum.png)\
\endgroup


\newpage
# Ejercicio 1: Creando directorios con Cerebro.

Realiza un Shell Script que cree en la carpeta personal del usuario (`$HOME`), la siguiente estructura 
de directorios:

\begingroup
\centering

![ZombiExam](imgs/exam2022-tarea01.png)\
\endgroup


Antes de crear **cada uno** de los directorios, el script debe comprobar que existe y en ese caso: mostrar 
un mensaje indicando que no es necesaria su creación.**CADA UNO**.

\newpage
# Ejercicio 2: Corrigiendo con cabeza.

El siguiente fragmento de código tiene un error, corríjelo:

```Shell
#!/bin/bash

# Este script debe mostrar todos los ficheros que esten en 
# /usr/bin/ que sean ejecutables.

for f in $(ls /usr/bin/); do

	if [ -x $f ]; then
			echo $f
	fi
	
done

```

\newpage
# Ejercicio 3: Operaciones Zombies!

Realiza un Shell Script que acepte 3 parámetros:

1. OPERACION A REALIZAR
2. OPERANDO_1
3. OPERANDO_2

Las operaciones pueden ser: 

- *SumaNormal* -> Entonces debe sumar ambos Operandos y devolver el resultado.
- *SumaZombie* -> Debe sumar ambos operandos y si el resultado es mayor que 5 devolver un 1 y mostrar un mensaje: "CEREBRO COMIDO".

## Ejemplos

```shell
smx@maquina:~$ ./ejercicio3.sh sumaNormal 2 6
8

smx@maquina:~$ ./ejercicio3.sh sumaZombie 2 1
3

smx@maquina:~$ ./ejercicio3.sh sumaZombie 2 6
1
CEREBRO COMIDO

```

\newpage
# Ejercicio 4: Buscando Cerebros

Realiza un Shell Script que compruebe que se le están pasando sólo 2 argumentos como máximo, en caso de que se le indiquen menos de 1 o más de 2, que muestre el mensaje:

```Shell
DEMASIADOS ARGUMENTOS
```

Y ahora, que realice las siguientes operaciones en función de los argumentos:

- Si el primer argumento es `CEREBROS`, debe mostrar las líneas del fichero `ZombieIpsum.txt` que contienen la palabra *brain* en cualquier combinación de mayúsculas y minúsculas.
- Si el primer argumento es `ZETAS`, debe mostrar las líneas del fichero `ZombieIpsum.txt` que contienen la palabra *zombie* en cualquier combinación de mayúsculas y minúsculas.
- Si el primer argumento es `ZETAS` y el segundo es `COMIDA` debe mostrar las líneas del fichero que contienen la palabra *hypothalamus* sin la palabra *zombie*.

## TERRITORIO DESCONOCIDO

Para aquell@s valientes que se atrevan con más, que cuando se pregunte por `CEREBROS` también aparezcan las líneas con palabras como: *brainz,braaiiiinnns,breins,....* usando *EXPRESIONES REGULARES*.

\begingroup
\centering
![Zombie Ipsum](imgs/zombieIpsum.png)\
\endgroup
