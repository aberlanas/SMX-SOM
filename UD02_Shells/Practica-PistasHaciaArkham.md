---
title: Practica 03 - Pistas hacia Arkham
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

\maketitle
\tableofcontents

# Tarea : Pistas hacia Arkham

Vamos a seguir investigando el caso de Charles Ward Dexter (posible homenaje a Charles Dexter Ward, pero es un misterio). Las últimas semanas estuvo muy pendiente
de los sistemas de la empresa que vigilaban las instalaciones mentales de Arkham. Se ha contactado con los médicos de la institución pero no parecen muy comunicativos.

Tal vez la única esperanza de averiguar qué es lo que ocurre es que encontréis las pistas necesarias en los `Scripts` que ha dejado en los
sistemas de vigilancia.

Recordad que la técnica que podéis (debéis) utilizar es, crear los diferentes scripts:

1. script-arkham-01.sh
2. script-arkham-02.sh
3. script-arkham-03.sh
4. ...

Y copiáis el contenido del script al script, le dais permisos de ejecución y lo probáis para ver **qué** es lo que hace.

Algunos comandos útiles:

- `nano FICHERO.sh` 	
- `chmod +x FICHERO.sh`
- `./FICHERO.sh`

Donde `FICHERO.sh` debe ser sustituido en cada caso por : script-arkham-01.sh, script-arkham-02.sh, etc...

¡Sed ordenad@s!

## Código Secreto

En **Aules** el anterior Administrador de Sistemas nos ha dejado un archivo comprimido que contiene información acerca de todo lo que descubrió en Arkham... No quiso que cayera en malas 
manos así que dejó pistas en los siguientes Scripts para que pudierais ayudarle (o vengarle)...

Arreglad los Scripts, encontrad el código y estaréis un paso más cerca de averiguar **qué está pasando en Arkham**.

\newpage

## Script Arkham 01

![Mapa De Arkham](./imgs/arkham_00.jpg)\

Al cruzar el puente sobre el rio Miskatonic, al Noreste de la Ciudad, dejando atrás Innsmouth Str, aparece una amplia avenida, que lleva por nombre *River Street*. 
En ella, existen una serie de semáforos muy antiguos que regulan el paso de los tres puentes. 

Estos semáforos están controlados desde el Manicomio de Arkham, ya que cuando se trasladan determinados pacientes, se regula el tráfico para evitar posibles retrasos y
evitar posibles encuentros *desafortunados*.

\newpage
Corrige el programa que pregunta al usuario **qué** semáforo debemos poner en **verde** y establezca los otros dos en **rojo**. Por defecto, los tres semáforos están en **ámbar**.

```shell
#!/bin/bash

SEMAFORO_2="Ambar"
SEMAFORO_6="Ambar"
SEMAFORO_8="Ambar"

echo " * Bienvenido al sistema de Trafico de Arkham "
echo " --> Accediendo a RiverStreet"
sleep 0.5
echo " --> Indique que semaforo desea establecer a Verde"
echo " 2)"
echo " 6)" 
echo " 8)"
echo -n " Numero de Semaforo ? : "
read SEMAFORO_ACTUAL

#####
# FRAGMENTOS PERDIDOS
#####

echo " Semaforo Calle 2 : $SEMAFORO_2"
echo " Semaforo Calle 6 : $SEMAFORO_6"
echo " Semaforo Calle 8 : $SEMAFORO_8"

exit 0
```

\newpage

## Script Arkham 02

![Mapa De Arkham](./imgs/arkham_00.jpg)\

Seguimos investigando en el barrio de los muelles, el Rio Miskatonic cuenta con un denso tráfico marítimo hacia Boston, de donde provienen la mayoría de los buques mercantes
que atracan en Arkham.

En el complejo de Muelles también podemos encontrar infraestructuras relacionadas de una u otra forma con el Manicomio. Los edificios marcados en el mapa con dos 11, contienen una serie
de hangares especiales que apenas se utilizan de día, pero que son frecuentados por el personal de Arkham a lo largo de las noches.

Estos hangares tienen un control horario muy estricto, permitiendo tan solo el acceso a uno de los hangares cada 5 minutos. ¿Cómo se resuelve esto? Mediante el uso de Enlaces Simbólicos...

Contamos con un Script que comprueba cada vez que se ejecuta qué hora es y crea el acceso directo correspondiente al hangar habilitado. **NO DEBEMOS PERMITIR QUE LOS OTROS HANGARES ESTÉN ACCESIBLES** 
(borrando el enlace anterior será suficiente).

\newpage

Adjunto tabla horaria:

| Minutos | Hangar Disponible |
|---------|-------------------|
| 0-4     | Terrar|
| 5-9     | Profur|
| 10-14   | Medea | 
| 15-19   | Terrar|
| 20-24   | Profur|
| 25-29   | Medea |
| 30-34   | Terrar|
| 35-39   | Profur|
| 40-44   | Medea |
| 45-49   | Terrar|
| 50-54   | Profur|
| 55-59   | Medea |

-----

```shell
#!/bin/bash


mkdir -p $HOME/Arkham/Hangares/Terrar
mkdir -p $HOME/Arkham/Hangares/Profur
mkdir -p $HOME/Arkham/Hangares/Medea

#### 
## FRAGMENTO PERDIDO
###


ls -l $HOME/Arkham/Hangares/Habilitado

exit 0
```

\newpage

![Mapa De Arkham](./imgs/arkham_01.jpg)\

Cerca de la **Colina del Ahorcado** existe una pequeña iglesia donde el Sacerdote que acude al Manicomio de Arkham realiza confesiones y meditaciones. Este Sacerdote, llamado Edward Hopkins
ha sido entrevistado por nuestro Administrador de Sistemas Desaparecido en varias ocasiones.

Suele asistir al Manicomio a petición de las familias de los internos, así como de los doctores que residen allí. 

Últimamente ha ido en repetidas ocasiones a las horas del alba, parece ser que su presencia en el Manicomio es especialmente necesaria cuando sale el Sol. Muchos de los internos están expirando durante 
las noches sin que nadie se explique **qué** es lo que ocurre. Sin embargo, Edward tiene órdenes del Médico Jefe de que no le vean llegar al Manicomio, lo que implica estar pendiente de los
horarios de la *guardia nocturna* que vigila las oscuras y húmedas calles de Arkham cada hora.

Para no tener que saberse el horario de la patrulla, solicitó a nuestro anterior Administrador de Sistemas 
un Script que dependiendo de la hora en la que estaba, le indicaba qué calles debía elegir para llegar, así cómo el número de pasos que le costaría llegar
desde **La Iglesia del Oeste** al Manicomio.

\newpage

Hemos perdido gran parte del script, pero sabemos que: 

- La entrada del Manicomio está al final de *College St*.
- Debe evitar encontrarse con la patrulla, pero debe hacer el recorrido más corto posible.
- A la patrulla le cuesta *1 hora* realizar el recorrido, con lo que sólo debemos tener en cuenta los minutos.
- Cada calle es revisada en 15 minutos y el recorrido es el siguiente:
	1. Boundary Street.
	2. Crane Street.
	3. West Street.
	4. Paralela a Crane Street (numero 19).

A medida que tengáis esta tarea, hacérselo saber a vuestro profesor, os indicará la clave que os falta para abrir el `FicheroCompridoSecreto`.

## Script Arkham 03

```shell
#!/bin/bash

WEST_STREET_NUMPASOS="100"
BOUNDARY_STREET_NUMPASOS="200"
CRANE_STREET_NUMPASOS="50"
PARALELA_CRANE_STREET_NUMPASOS="50"
COLLEGE_STREET_NUMPASOS="300"

TOTAL_NUMPASOS=0


#####
# FRAGMENTO PERDIDO
#####


echo " * Son las $(date) y debes coger la Ruta: $RUTA"
echo " * Te costara : $TOTAL_NUMPASOS"

exit 0
```


