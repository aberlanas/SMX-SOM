---
title: Exam - Loops and Shell
subtitle: Unit 02
author:  SOM - Angel Berlanas Vicente
header-includes: |
lang: en-UK
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Exam Shell - 01

Read carefully all the exam before start.

## Task 01 : 3pts

Code a Shell Script that asks the user for a number, and create at his/her `$HOME` folder so many subfolders
in the `$HOME/exam-task01/` as the number given, with the name: `sub-folder-X`, where X must be the differents 
values between 1 and the number given.

If the folder `$HOME/exam-task01/` exists (because a previous execution of the script for example), the script
must ask the user if he/she wants to delete its contents before create the new folders.

## Task 02 : 3 pts 

In the famous city of Samarkand there was a jeweler who was dedicated to sculpting precious words in rubies. This jeweler was required by the great kings of antiquity, and its fame was due in part by the wonderful inscriptions in *lapislazuli* that could be read perfectly in the jewels he engraved. His work was not easy, but he had a very refined and exquisite technique, since he only recorded phrases whose number of vowels were ordered, that is, there were more "a"s than "e"s and more "e"s than "i"s (or at least the same), and so on with the rest of the vowels. 

Also, its important that the number of vowels must be greather than the number of consonants in the sentence.

```shell
 * Give me a sentence -> Al amanecer oi un alma.
a = 5
e = 2
i = 1
o = 1
u = 1
* Sentence is ok
```


```shell
 * Give me a sentence -> El almendro en flor
a = 1
e = 3
i = 0
o = 2
u = 0
* Sentence is NOT OK
```

\newpage
## Task 03 (Option A): 4 pts

Ask the user for an hour:minute (using two variables), and display for the next times that the minutes will be the same in the day (before 23:59) how many seconds 
has passed from the **8:00** in each *hour-step* (if the hour is earlier than *8:00*, displays a message and do nothing), including itself.

See the examples below:

```shell
 * Give me an hour -> 20
 * Give me a minute-> 10
 20:10 -> 43800
 21:10 -> 47400
 22:10 -> 51000
 23:10 -> 54600
```


```shell
 * Give me an hour -> 7
 * Give me a minute-> 45
 Too Early
```

\newpage
## Task 03 (Option B): 4 pts
Help the user to know what day of week will be one day of a month.

First ask the user about what month is : /1-12/.
Secondly ask what is the first day of week of this month.

You can use the next table as a reference:

| Day of Week | Number |
|-------------+--------|
| Sunday      |      0 |
| Monday      |      1 |
| Thursday    |      2 |
| Wednesday   |      3 |
| Tuesday     |      4 |
| Friday      |      5 |
| Saturday    |      6 |

Then, display in a list all the days of the month with its day of the week.

Example:

```shell
 * Give me a month: 10
 * Give me a Day: 2
 
 S  M  T  W  X  F  S 
       1  2  3  4  5
 6  7  8  9  10 11 12
 13 14 15 16 17 18 19
 20 21 22 23 24 25 26
 27 28 29 30 31
 
 ```
  
 
 


