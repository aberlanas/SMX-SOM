---
title: SHELL - Avanzando en la investigación
subtitle: Unit 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Tarea : Avanzando en la Investigación

Vamos a seguir investigando el caso de Charles Ward Dexter (posible homenaje a Charles Dexter Ward, pero es un misterio. Los siguientes pasos dentro de su trabajo ya se ponen más *peliagudos*, un cierto aire sombrío aparece en los comentarios que podemos
encontrar en los diferentes Scripts...¿podremos avanzar en su locura y descubrir qué le ha pasado?.

Recordad que la técnica que podéis (debéis) utilizar es, crear los diferentes scripts:

1. script01.sh
2. script02.sh
3. script03.sh
4. ...

Y copiais el contenido del script al script, le daís permisos de ejecución y lo probáis para ver **qué** es lo que hace.

Algunos comandos útiles:

- `nano FICHERO.sh` 	
- `chmod +x FICHERO.sh`
- `./FICHERO.sh`

Donde `FICHERO.sh` debe ser sustituido en cada caso por : script01.sh, script02.sh, etc...

¡Sed ordenad@s!

\newpage

## Script 10

```shell
#!/bin/bash

# Repetir, repetir, repetir....

# el mensaje ha de aparecer 3 veces 

for f in $(seq 1 4); do
	echo " * No estoy loco "
done

exit 0
```

\newpage

## Script 11

```shell
#!/bin/bash

# Repetir, para cada carpeta y fichero de la carpeta personal....
# repetir, repetir, repetir....
# estamos condenados....
# repetir, repetir, repetir....

for f in $(ls -1 $HOME); do
	echo " * Esto es $f"
done

exit 0
```

\newpage

## Script 12

```shell
#!/bin/bash

# Repetir, sin fin, sin fin....

while /bin/true; do

	echo " Sin Fin "
	sleep 0,1

done

exit 0
```


\newpage

## Script 13

```shell
#!/bin/bash

# No es sencillo, pero se puede comprender
# Cada uno de los directorios de Raiz
# se ha de mostrar
# aquellos que no lo son
# no se veran

for f in $(ls -1 /); do
	if [ -e $f ] ; then
		echo " * Muestrate! $f"
	fi
done

exit 0
```


\newpage

## Script 14

``` shell
#!/bin/bash

# Otro giro inesperado
# en la lista de directorios
# no se esperaba el lector
# encontrar tal emboltorio
# alrededor de un error.

# Por si acaso no queda claro
# debe mostrar solamente
# aquellos directorios 
# que esten en el directorio 
# raiz y nada mas

for f in $(ls -1 /); do
	if [ ! -d $f ] ; then
		echo " * Muestrate! $f"
	else
		echo " * Oculto queda $f " 
	fi
done



exit 0
```


\newpage

## Script 15

``` shell
#!/bin/bash

# Se muestran las carpetas del usuario
# que comienzan por las letra D

for f in $(ls -1 $HOME/d*); do
	if [ -d $f ] ; then
		echo " * Muestrate! $f"
	else
		echo " * Oculto queda $f " 
	fi
done



exit 0
```


\newpage

## Script 16

``` shell
#!/bin/bash

# Se muestran las carpetas del directorio raiz
# que comienzan por las letra m

for f in $(ls -1 $HOME/m*); do
	if [ -d $f ] ; then
		echo " * Muestrate! $f"
	else
		echo " * Oculto queda $f " 
	fi
done



exit 0
```

\newpage

## Script 17

``` shell
#!/bin/bash

# Se muestran las carpetas del directorio /var/log/

for f in $(ls -1 /var/log/*); do
	if [ ! -d $f ] ; then
		echo " * Muestrate! $f"
	else
		echo " * Oculto queda $f " 
	fi
done



exit 0
```


\newpage

## Script 18

``` shell
#!/bin/bash

# Contando ejecutables
# Vamos a contar cuantos ficheros 
# ejecutables hay en el directorio 
# /usr/bin

TOTAL=0

for f in $(ls -1 /usr/bin/*); do
	if [ ! -x $f ] ; then
		let TOTAL=$TOTAL+1	
	else
		echo " * Este no lo cuento $f " 
	fi
done


echo " En TOTAL hay $TOTAL "

exit 0
```



