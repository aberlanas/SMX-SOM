---
title: SHELL - Trabajando de Detectives
subtitle: Unit 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Tarea : Trabajando de Detectives

Acaban de contratarnos en una empresa de administrador@s de sistemas, ya que el antiguo administrador ha desaparecido en **extrañas circunstancias**. Al parecer durante los últimos meses se fue comportando de manera extraña, nerviosa, cometiendo fallos en tareas cotidianas y poniendo en peligro la estabilidad de los sistemas.

Te han contratado para que examines los últimos Scripts que realizó, y encuentres los fallos que hay en ellos y los arregles. Ten en cuenta que en cada Script hay **al menos** un fallo. Toma nota de las herramientas y técnicas del viejo administrador, te pueden resultar útiles más adelante.

**NOTA PARA L@S INVESTIGADOR@S**: Al comienzo de cada Script hay un pequeño texto que describe el **comportamiento** adecuado para dicho Script, es decir: *qué debería hacer*, encontrad los fallos y arregladlos.

Una técnica que podéis (debéis) utilizar es, crear los diferentes scripts:

1. script01.sh
2. script02.sh
3. script03.sh
4. ...

Y copiais el contenido del script al script, le daís permisos de ejecución y lo probáis para ver **qué** es lo que hace.

Algunos comandos útiles:

- `nano FICHERO.sh` 	
- `chmod +x FICHERO.sh`
- `./FICHERO.sh`

Donde `FICHERO.sh` debe ser sustituido en cada caso por : script01.sh, script02.sh, etc...

¡Sed ordenad@s!

\newpage

## Script 01 

```shell
#!/bin/bash

# Este Script debe mostrar por pantalla un mensaje 
# de bienvenida indicando la hora del sistema con 
# la fecha.

eco " Hola, Bienvenid@s"
date 

exit 0
```

\newpage

## Script 02

```shell
#!/bin/bash

# Este Script debe preguntar por nuestro nombre de usuario 
# y crear una carpeta en /tmp/
# que tenga ese nombre.

echo -n " Indique su nombre de usuario : "
read NOMBRE

echo " El Nombre introducido es $N0MBRE "
mkdir /tmp/$N0MBRE

echo " El directorio se ha creado satisfactoriamente"

exit 0
```

\newpage

## Script 03

```shell
#!/bin/bash

# Este Script debe preguntar por un nombre de carpeta 
# y crear una carpeta en /tmp/nivel1
# que tenga ese nombre, en caso de que la carpeta 
# nivel1 no exista, debe crearla primero.

echo -n " Indique el Nombre de la Carpeta: "
read CARPENOMBRE

echo " El Nombre introducido es $CARPENOMBRE"

if [ test -L /tmp/nivel1 ]; then
	mkdir /tmp/nivel1
fi

mkdir /tmp/nivel1/$CARPENOMBRE


exit 0
```


\newpage

## Script 04

```shell
#!/bin/bash

# Este Script debe listar el contenido de la carpeta personal 
# del usuario, una vez listado
# preguntar por una carpeta que esté en la carpeta personal 
# y crear dentro de esa carpeta
# un fichero vacio que se llame : EstuvoAqui.txt

ls $HOME

echo -n " Indique el Nombre de la Carpeta: "
read CARPENOMBRE

echo " El Nombre introducido es $CARPENOMBRE"
touch $HOME/$CARPENOMBRE/EstuvoAqi.txt

echo " El fichero se ha creado"


exit 0
```


\newpage

## Script 05

``` shell
#!/bin/bash

# Este Script debe crear en la carpeta personal del usuario 
# una estructura de directorios
# tal y como aparece en la captura.

mkdir $HOME/nivel5
mkdir $HOME/nivel5/charles
mkdir $HOME/nivel5/charles/administrador
mkdir $HOME/nivel5/charles/dexter
mkdir $HOME/nivel5/charles/dexter/ward
mkdir $HOME/nivel5/charles/dexter/horario
mkdir $HOME/nivel5/charles/dexter/lunes
mkdir $HOME/nivel5/charles/dexter/martes
mkdir $HOME/nivel5/charles/dexter/miercoles
mkdir $HOME/nivel5/charles/dexter/jueves
mkdir $HOME/nivel5/charles/dexter/viernes
mkdir $HOME/nivel5/charles/dexter/sesiones


exit 0
```

![Nivel 5](imgs/nivel5.png){width=250}\

\newpage

## Script 06

```shell
#!/bin/bash

# Este Script debe crear en la carpeta personal del usuario una estructura de directorios
# tal y como aparece en la captura:

mkdir $HOME/nivel6
mkdir $HOME/nivel6/director
mkdir $HOME/nivel6/enfermeria/
mkdir $HOME/nivel6/enfermos/
mkdir $HOME/nivel6/enfermos/paulGraham
mkdir $HOME/nivel6/enfermos/sofieCeres
mkdir $HOME/nivel6/enfermos/augustusMoon
mkdir $HOME/nivel6/enfermos/teloniusAres
mkdir $HOME/nivel6/enfermos/nervThyonius
cp $HOME/nivel6/enfermeria $HOME/nivel6/pruebas


exit 0

```

![Nivel 6](imgs/nivel6.png){width=250}\


\newpage

## Script 07

```shell
#!/bin/bash

# Este Script debe crear en la carpeta personal del usuario 
# una estructura de directorios
# tal y como aparece en la captura:

mkdir $HOME/nivel7
mkdir $HOME/nivel7/boston
mkdir $HOME/nivel7/arkham
mkdir $HOME/nivel7/casos
mkdir $HOME/nivel7/sospechosos
mkdir $HOME/nivel7/sospechosos/paulGraham
mkdir $HOME/nivel7/sospechosos/sofieCeres
mkdir $HOME/nivel7/sospechosos/augustusMoon
mkdir $HOME/nivel7/sospechosos/teloniusAres
mkdir $HOME/nivel7/sospechosos/nervThyonius

## Una vez creada debe pedir el nombre de uno de los sospechosos
## y comprobar que ese nombre esta en la carpeta de sospechosos
## si esta, debe copiarlo a la carpeta casos.

echo -n " Dime el nombre del sospechoso : "
read SOSPECHOSO

if [ -d $SOSPECHOSO ]; then

	cp -r $HOME/nivel7/sospechosos/$SOSPECHOSO $HOME/nivel7/casos/

fi


exit 0

```

![Nivel 7](imgs/nivel7.png){width=250}\

\newpage

## Script 08

Para este nivel, es recomendable echarle un ojo al manual del comando `ln`. Y hacer algunas pruebas.

```shell
#!/bin/bash

# Este Script debe crear en la carpeta personal del usuario 
# una estructura de directorios
# tal y como aparece en la captura:

mkdir $HOME/nivel8
mkdir $HOME/nivel8/boston
mkdir $HOME/nivel8/arkham
mkdir $HOME/nivel8/casos
mkdir $HOME/nivel8/sospechosos
mkdir $HOME/nivel8/sospechosos/paulGraham
mkdir $HOME/nivel8/sospechosos/sofieCeres
mkdir $HOME/nivel8/sospechosos/augustusMoon
mkdir $HOME/nivel8/sospechosos/teloniusAres
mkdir $HOME/nivel8/sospechosos/nervThyonius

## Una vez creada debe pedir el nombre de uno de los sospechosos
## y comprobar que ese nombre esta en la carpeta de sospechosos
## si esta, debe crear un ENLACE SIMBOLICO a la carpeta caso 
## con el nombre del sospechoso

echo -n " Dime el nombre del sospechoso : "
read SOSPECHOSO

if [ -d $SOSPECHOSO ]; then

	ln -s $HOME/nivel8/casos/ $HOME/nivel8/sospechosos/$SOSPECHOSO 

fi


exit 0

```

![Nivel 8](imgs/nivel8.png){width=200}\



