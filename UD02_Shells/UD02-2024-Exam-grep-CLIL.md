---
title: Exam - Login and grep
subtitle: Unit 02
author:  SOM - Angel Berlanas Vicente
header-includes: |
lang: en-UK
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# GREP Exercises - Log Analysis

## Log Simulation (`logins-exam.log`):

## Exercises

1. **Find failed login attempts specifics IP**
   - Objective: Find all lines where a failed login attempt occurred from the IP `203.0.113.5` or `203.0.113.6`.

2. **Count successful login attempts**
   - Objective: Count how many failed logins occurred in the system.

3. **Find successful login attempts by the root or admin user**
   - Objective: Find all login successful attempts by the `root` or `admin` user.

4. **Find successful login attempts by the root user**
   - Objective: Find all login successful attempts by the `root` user.

5. **Find logins using privatekey**
   - Objective: Show all access attempts (successful or failed) that use privatekey.

6. **Count failed logins with unknown users**
   - Objective: Count all failed access attempts with unknown users.

7. **Search logins within a specific time period**
   - Objective: Find access attempts that occurred at 23:15-23:22 (between that minutes).

8. **Show the IP of successful login attempts**
   - Objective: Extract only the IP addresses of successful login attempts.
   
9. **Show the IP of successful login attempts in October**
   - Objective: Extract only the IP addresses of successful login attempts in October
   
10. **Show the IP and port of successful login attempts in October**
   - Objective: Extract only the IP addresses and the ports of failed login attempts in October from 
