#+Title: Loops Exam - I (Recover)
#+Author: Angel Berlanas Vicente

#+LATEX_COMPILER: xelatex
#+EXPORT_FILE_NAME: ../PDFS/Exam-Loops-Recover-CLIL.pdf

#+LATEX_HEADER: \hypersetup{colorlinks=true,urlcolor=blue}

#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \fancyhead{} % clear all header fields
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \fancyhead[R]{1-SMX:SOM - Exam}
#+LATEX_HEADER: \fancyhead[L]{Loops}

#+LATEX_HEADER: \usepackage{wallpaper}
#+LATEX_HEADER: \ULCornerWallPaper{0.9}{../rsrc/logos/header_europa.png}
#+LATEX_HEADER: \CenterWallPaper{0.7}{../rsrc/logos/watermark_1.png}

#+LATEX: \newpage

* Scripting - Loops

First of all: Read all the exam.

You can use all the tasks previously done and all kind of resources and documents
/already downloaded/ in your machines.

Do your best!

You must present the Script with the next name:

- loop-exam-recover-01.sh

#+LATEX: \newpage
** Calendar Reloaded

Help the user to know what day of week will be one day of a month.

First ask the user about what month is : /1-12/.
Secondly ask what is the first day of week of this month.

You can use the next table as a reference:

| Day of Week | Number |
|-------------+--------|
| Sunday      |      0 |
| Monday      |      1 |
| Thursday    |      2 |
| Wednesday   |      3 |
| Tuesday     |      4 |
| Friday      |      5 |
| Saturday    |      6 |

Then, display in a list all the days of the month with its day of the week.

But, now you must display the Calendar in a "calendar view", for example:

#+CAPTION: Calendar Reloaded in Action
#+NAME:   Calendar
[[./imgs/calendar_reloaded.png]]
