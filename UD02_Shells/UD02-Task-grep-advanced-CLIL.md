---
title: Task - Login and grep
subtitle: Unit 02
author:  SOM - Angel Berlanas Vicente
header-includes: |
lang: en-UK
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# GREP Exercises - Log Analysis

## Log Simulation (`logins.log`):

## Additional Exercises

1. **Find all log entries from a specific IP address**
   - Objective: Retrieve all login attempts (successful or failed) from the IP `8.8.8.8`.

2. **Find all login attempts by the user "admin"**
   - Objective: Search for all log entries related to the user `admin`.

3. **Display logs from a specific time range**
   - Objective: Display all log entries that occurred between 08:12 and 08:15.

4. **Show all logs that contain "ssh2" in the protocol field**
   - Objective: Search for log entries that use the `ssh2` protocol.

5. **Find lines where the port number starts with '5'**
   - Objective: Show all login attempts where the port number starts with the digit `5`.

6. **Count how many different users have logged in successfully**
   - Objective: Count how many unique users have successfully logged into the system.

7. **Find all failed login attempts from outside the local network**
   - Objective: Display failed login attempts from IP addresses not within the `192.168.x.x` network.

8. **Find log entries where the username is "unknown"**
   - Objective: Show all log entries related to an unknown user, whether the attempt was successful or failed.

9. **Find failed login attempts with non-standard ports**
   - Objective: Search for failed login attempts that occurred on ports higher than `50000`.

10. **Extract the list of unique IP addresses**
    - Objective: Display a list of all unique IP addresses from which login attempts (successful or failed) originated.

## Advanced Exercises

1. **Show the time and IP address of all failed login attempts**
   - Objective: Extract the timestamp and IP address for each failed login attempt.

2. **Display the count of failed login attempts per IP address**
   - Objective: Group and count how many failed login attempts originated from each IP address.

3. **Find log entries that contain both "Failed" and "root"**
   - Objective: Find all failed login attempts specifically for the `root` user.

4. **Find logs with login attempts using high ports**
   - Objective: Search for login attempts on ports higher than `60000`.

5. **Display the usernames of all failed logins**
   - Objective: Extract and list all the usernames associated with failed login attempts.

6. **Find login attempts from public IP addresses**
   - Objective: Display logs of login attempts from IP addresses that are not part of the private network ranges.



