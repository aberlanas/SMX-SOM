---
title: Colección de Problemas
subtitle: 16 Problemas
author: Patxi Sanchis y Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Coleccion de Problemas

## Introducción

Esta colección de problemas plantea diferentes situaciones sin entrar a detallar qué lenguaje de programación
se debe usar para resolverlas.

Las matemáticas son una fuente inagotable de inspiración para este tipo de ejercios, aunque otras áreas no se
quedan atrás.

Esperamos que os sirvan.

## Problema 01

Sabiendo que en el ajedrez se colocan tantos peones como columnas hay en cada uno de los bandos, calcula el número *total* de peones
que deben introducirse en el inicio de una partida en un tablero *personalizado* cuyo número total columnas es preguntado al usuario.

## Problema 02

Realiza un programa que pregunte por tres numeros y devuelva el más alto.

## Problema 03

Asumiendo que *PI* sea igual a *3,141592*. Realiza un programa que a partir de un radio *r* dado: 

* Calcule la longitud de la Circunferencia.
* Calcule el área del círculo.
* Calcule el volumen de la esfera.

## Problema 04

Dado un año válido del segundo milénio, indica si es bisiesto o no.

## Problema 05

Dada una hora en formato 00-23, muestra un "*De Madrugada*" entre las 23 y las 05, un "*Buenos dias*" entre las 06 y las 11, 
un "*Buenas Tardes*" entre las 12 y las 19 y un "*Buenas Noches*" entre las 20 y las 23.

## Problema 06

Dado un numero entre 1 y 100, calcula el número de la decena más próxima a él (asumiendo que el 5 está más cerca de la siguiente decena).

Ejemplo:

```shell
Dame un numero: 16
Resultado : 20
```

```shell
Dame un numero: 32
Resultado : 30
```

```shell
Dame un numero: 35
Resultado : 40
```

## Problema 07

Realiza un programa que a partir de una palabra dada cuente el número de vocales que contiene la palabra.

\newpage
## Problema 08

En la famosa ciudad de Samarkanda existía un joyero que se dedicaba a esculpir preciosas palabras en rubíes. Este joyero era requerido 
por los grandes reyes de la Antigüedad, y su fama se debía en parte a las maravillosas inscripciones en lapislázuli que se podían leer 
perfectamente en las joyas que grababa.

No era fácil su labor, pero tenía una técnica muy depurada y exquisita, ya que sólo grababa frases cuya cantidad de vocales estuvieran ordenada, es decir, hubiera más "a"s que "e"s y más "e"s que "i"s (o al menos las mismas), y así con el resto de vocales. 

```shell
 * Dime la frase para grabar -> Al amanecer oi un alma.
a = 5
e = 2
i = 1
o = 1
u = 1
* Frase Adecuada para ser grabada
```


```shell
 * Dime la frase para grabar -> El almendro en flor
a = 1
e = 3
i = 0
o = 2
u = 0
* Frase no valida para ser grabada
```

Haz un programa que ayude al joyero a decidir si la frase es digna de ser grabada en los rubíes.

\newpage
## Problema 09

Los trenes son algo estupendo (¿os había contado que me encantan?), son máquinas increíbles que nos ayudan a transportar mercancias, a viajar, ...

Sabiendo que en una estación existen 2 tipos de locomotora : **Pacific-231** y **Garrat-462** que alcanzan diferentes velocidades promedio:

| Locomotora  | Velocidad|
|-------------|----------|
| Pacific-231 | 90 Km/h  |
| Garrat-462  | 54 Km/h  |

Haz un programa que pregunte por el tipo de locomotora y la cantidad de Kilómetros del viaje, indicando cuantas horas le costará a dicha 
locomotora realizar el trayecto.

## Problema 10

En el famoso expreso Ohio-Arkham, existe una parada que sólo está permitida bajo ciertas extrañas circunstancias que tienen que ver con la 
hora del dia en la que nos encontramos, más concretamente con los *minutos* que llevamos de día. El número de minutos minutos totales que han transcurrido del dia ha de ser
múltiplo de 3 y de 7 de manera simultánea. 

Por ejemplo si el tren pasa por la ubicación de la parada a las 00:21 y se notifica al revisor, este puede parar el tren y se puede bajar. Si por el contrario, intentaramos
detenernos en la parada y la hora del trayecto es las 02:21 no se permite, ya que : *2x60=120+21=221* que no es múltiplo de 3 (ni de 7).

Haz un programa que ayude al revisor a partir de una hora y un minuto dados  a averiguar si la parada está permitida.

\newpage
## Problema 11

No es sencillo llevar la cuenta de los pasajeros que suben y bajan de los trenes en la maravillosa línea férrea que va desde Chicago hasta Baltimore. Sin embargo, 
en Cleveland y Philadelphia los revisores se toman su tiempo y cuentan a todos los pasajeros presentes en el tren antes de continuar con su viaje. Esto se realiza en cada 
viaje, pero la **B&O** no sólo está interesada en saber cuantos pasajeros tienen al final, sino cuantas veces ha "disminuido" el número de pasajeros y cuantas "aumentado".

Sabiendo que las paradas se relizan en el siguiente orden: **Baltimore** $\rightarrow$ **Philadelphia** $\rightarrow$ **Cleveland** $\rightarrow$ **Chicago**, haz un programa que pregunte por el número de viajeros en cada una de ellas y cuando acabe el trayecto diga si ha sido un día *pesimista* (han bajado más *veces* del tren pasajeros que subido), *optimista* (han subido más veces que bajado), o *neutro* que han bajado y subido personas el mismo número de veces.

>NOTA: Es importante que no deben tener en cuenta el número de pasajeros, sino si la gente "sube" o "baja" del tren.

Ejemplos:

```shell
Pasajeros en Baltimore: 33
Pasajeros en Philadelphia: 45
Pasajeros en Cleveland: 5
Pasajeros en Chicago: 10

* Se ha incrementado 2 veces el numero de pasajeros.
* Dia Optimista
```

```shell
Pasajeros en Baltimore: 33
Pasajeros en Philadelphia: 45
Pasajeros en Cleveland: 10
Pasajeros en Chicago: 5

* Ha disminuido 2 veces el numero de pasajeros.
* Dia Pesimista
```

```shell
Pasajeros en Baltimore: 33
Pasajeros en Philadelphia: 45
Pasajeros en Cleveland: 33
Pasajeros en Chicago: 33

* Se ha incrementado y decrementado el mismo numero de veces los pasajeros
* Dia Neutro
```

\newpage
# Problema 12 

Durante los trayectos de los trenes del siglo XIX, estos debían recargar agua para las inmensas locomotoras que funcionaban gracias 
al vapor de agua.

Estos depósitos de agua que se encontraban en las estaciones no estaban en todas, sino que se le indicaban al/la maquinista en el inicio
del viaje, concretamente se le indicaba *cada cuantas estaciones* se encontraría un depósito (para que él/ella lo tuviera en cuenta).

Utilizando estructuras de repetición (`for`), crea un programa que pregunte al usuario cada cuantas estaciones se encontrará el tren 
los depósitos y *cuantas estaciones* tiene el recorrido. Y a continuación muestre un listado de las estaciones indicando en cada una 
de ellas si tendŕa depósito o no.

Ejemplo:

```shell
Dime cada cuantas estaciones hay deposito: 3
Dime cuantas estaciones tiene el recorrido: 10

Estacion 1 : No
Estacion 2 : No
Estacion 3 : Si
Estacion 4 : No
Estacion 5 : No
Estacion 6 : Si
Estacion 7 : No
Estacion 8 : No
Estacion 9 : Si
Estacion 10: No

Termino
```

\newpage
# Problema 13

**El Gato, los botones y los números**

Hace mucho tiempo una famosa Sastre, llamada *Enesen* trabajaba en una colección de *chalecos* que debían usarse para un desfile. El desfile 
era algo muy importante en la ciudad y *Enesen* se esforzó mucho para que los botones de los chalecos quedaran *perfectos*. Sin embargo, 
el gato de *Enesen* (que era bastante juguetón y le gustaban los números), solo le permitia coserlos a los chalecos si el *número total* 
de agujeros de los botones era *múltiplo del número de botones* que se iban a coser al chaleco.

Haz un programa que pregunte por el número de chalecos que *Enesen* tiene que coser, y para cada uno de esos chalecos que simule el 
comportamiento del gato, es decir: que pregunte por el número de botones que va a tener el chaleco y luego para cada uno de ellos que pregunte 
cuantos agujeros tiene. **Si se cumple la condición**, el chaleco estará *perfecto*, en caso contrarió el gato lo descartará.

Al final, el programa debe indicarnos cuantos chalecos han quedado *perfectos*.

Ejemplo:
```
Dime cuantos chalecos vamos a coser: 2
Chaleco 1
 - Dime cuantos botones tiene: 2
 - Agujeros boton 1: 1
 - Agujeros boton 2: 3
** Tiene 4 agujeros y 2 botones.
** Es un chaleco perfecto.
Chaleco 2
 - Dime cuantos botones tiene: 3
 - Agujeros boton 1: 2
 - Agujeros boton 2: 3
 - Agujeros boton 3: 3
** Tiene 8 agujeros y 3 botones.
* Descartado
--> Hay 1 chaleco(s) perfecto(s)
```

![Chalecos](imgs/gato-chaleco-magico.png)\
\newpage

# Problema 14
Uno de los asuntos que más problemas dan a l@s informátic@s son aquellos 
relacionados con las fechas y las horas. A lo largo de vuestra vida como
profesionales os encontraréis con un montón de situaciones en las que tendréis
que lidiar con días de la semana, horas, minutos y segundos.

Vamos a hacer un pequeño programa que pregunte por la hora que es al usuario 
y que a continuación muestre las horas en punto que faltan hasta el día de mañana (las 00:00). Se ve muy bien con el ejemplo:

```shell
 - Indica que hora es: 17
18:00
19:00
20:00
21:00
22:00
23:00
```

# Problema 15

Amplia el problema anterior mostrando los *cuartos*. 

```shell 
- Indica que hora es: 20
20:15, 20:30, 20:45
21:00, 21:15, 21:30, 21:45
22:00, 22:15, 22:30, 22:45
23:00, 23:15, 23:30, 23:45
```

# Problema 16

Amplia el problema anterior añadiendo la pregunta de los minutos y calculando 
a partir del siguiente *cuarto*.

```shell
- Indica que hora es: 21
- Indica que minuto es: 18
21:30, 21:45.
22:00, 22:15, 22:30, 22:45
23:00, 23:15, 23:30, 23:45
```


