---
title: Task - Login and grep
subtitle: Unit 02
author:  SOM - Angel Berlanas Vicente
header-includes: |
lang: en-UK
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "222222"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# GREP Exercises - Log Analysis

## Log Simulation (`logins.log`):

## Exercises

1. **Find failed login attempts**
   - Objective: Find all lines where a failed login attempt occurred.

2. **Find failed logins from a specific IP**
   - Objective: Search for failed login attempts from the IP `203.0.113.5`.

3. **Count successful login attempts**
   - Objective: Count how many successful logins occurred in the system.

4. **Find login attempts by the root user**
   - Objective: Find all login attempts (successful or failed) by the `root` user.

5. **Find logins from a specific network**
   - Objective: Show all access attempts (successful or failed) from the `192.168.1.0/24` network.

6. **Find failed logins with unknown users**
   - Objective: Find all failed access attempts with unknown users.

7. **Search logins within a specific time period**
   - Objective: Find access attempts that occurred at 08:19 (within that exact minute).

## Advanced Exercise

- **Show the IP of failed login attempts**
  - Objective: Extract only the IP addresses of failed login attempts.
