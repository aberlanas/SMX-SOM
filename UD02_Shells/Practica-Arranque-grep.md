---
title: Practica 05 - Grep Arranque
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

\maketitle

# Tarea : Grep y Arranque

Os propongo en esta tarea la realización de un Script que al ejecutarse
obtenga la información del núcleo del Sistema (**aka** *kernel*) y busque
en esa información algunos valores que pueden ser de nuestro interés.

## Pistas de Scripts

Algunos comandos útiles:

- `nano FICHERO.sh` 	
- `chmod +x FICHERO.sh`
- `./FICHERO.sh`
- `grep`
- `dmesg`

Donde `FICHERO.sh` debe ser sustituido en cada caso por lo que corresponda.

¡Sed ordenad@s!

\newpage
## Skeleton (Plantilla).

```shell
#!/bin/bash

# Obtenemos la informacion del arranque
echo " * Obtenemos la informacion del arranque"
sudo dmesg > /tmp/infoarranque.txt


# Ahora cada uno de los pasos

# Rellenar aqui


exit 0

```

## Datos que se buscan

Adjunto tabla con lo que estamos buscando, así como las palabras *clave* que nos pueden 
servir para encontrarlos.

| Información | Palabras clave |
|-------------|----------------|
| Memoria RAM Detectada | `Memory:`|
| Modelo CPU | smpboot + CPU |
| Hora y Fecha del arranque | RTC + time + date| 
| Nombre del equipo | Hostname |

Recordad que cuando se expresan las cosas con un **+** quiere decir *qué estén en la misma línea*, no que estén juntas en la misma línea.

## Datos opcionales avanzados

También estaría bien (para el/la que haya completado esto con éxito la primera parte), que se mostrara:

| Información | Palabras clave |
|-------------|----------------|
| Informacion sobre discos duros | Salida del comando `fdisk -l /dev/sda` |
| Velocidad de todas las tarjetas de red detectadas | NO HAY PISTAS|
| Tamaño de la RAM en Megabytes | TOCA HACER CONVERSIÓN SOBRE LO OBTENIDO ANTERIORMENTE|


