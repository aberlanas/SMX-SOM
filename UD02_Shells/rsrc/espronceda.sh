#!/bin/bash

#Si el fichero ./laCancionDelPirata.txt no está en el mismo directorio que el Script "espronceda.sh" debe salir y mostrar un mensaje.

if [ ! -e ./laCancionDelPirata.txt ]; then
	echo " * ERROR : Fichero no encontrado "
	exit 1
fi

#Si no se le indica ningún argumento o no es uno de los siguientes, que muestre los diferentes argumentos permitidos.

#Si el 1er argumento es "bucanero" -> Cuenta el número de veces que aparece la palabra cañón, de manera case insensitive.

if [ $1 = "bucanero" ]; then
	cat ./laCancionDelPirata.txt | grep -i "cañon"| wc -l 
fi

#Si el 1er argumento es "estribillo" ->Cuenta el numero de veces que se dice : "Que es mi barco mi ..."

if [ $1 = "estribillo" ]; then
	cat ./laCancionDelPirata.txt | grep -i "Que es mi barco"| wc -l 
fi


#Si el 1er argumento es "Capitales" cuenta el número de palabras que comienzan por mayúscula.

if [ $1 = "Capitales" ]; then
	cat ./laCancionDelPirata.txt | grep "[A-Z]" | wc -l 
fi

#Si el 1er argumento es "unaSola" cuenta el número de versos que están formados por una sola palabra.
if [ $1 = "unaSola" ]; then
	cat ./laCancionDelPirata.txt | grep -v " " | grep -v "^$" | wc -l 
fi

#Si el 1er argumento es "vocal" cuenta cuantos versos acaban en vocal, o vocal y signo de puntuación.

if [ $1 = "vocal" ]; then
	cat ./laCancionDelPirata.txt | grep "[aeiou][,.;]$"
fi

#Si el 1er argumento es "estrofas" cuenta el número de estrofas del poema.

if [ $1 = "estrofas" ]; then
	cat ./laCancionDelPirata.txt | grep "^$" | wc -l
fi

#Si el 1er argumento es "metrica" muestra el verso más largo (en letras).

MASGRANDE=0
VERSO=""
cat ./laCancionDelPirata.txt | while read linea; do

	AUX=$(echo $linea | wc -c | cut -d " " -f1)
	echo $AUX
	if [ $AUX -gt $MASGRANDE ]; then
		MASGRANDE=$AUX
		VERSO=$linea
	fi

done

echo " El Verso mas largo tiene $MASGRANDE letras "
echo "$VERSO"

exit 0