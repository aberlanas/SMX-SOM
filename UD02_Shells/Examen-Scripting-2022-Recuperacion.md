---
title: Examen - ZombiExam-II El Retorno
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

\maketitle
\tableofcontents


# Inicio

Algunas recomendaciones:

- Leer *todo* el examen antes de comenzar.
- Si os atascáis en algún ejercicio, seguid con el siguiente.
- Contáis con todo el material de Internet y vuestras tareas y apuntes, pero no podéis copiaros.
- Todos los ejercicios valen lo mismo.
- Siempre que se necesite un fichero de texto para hacer búsquedas en este examen se hace uso de :
   `ZombieIpsum.txt`.

¡Mucha suerte a tod@s!

\begingroup
\centering
![Zombie Ipsum](imgs/zombieIpsum.png)\
\endgroup


\newpage
# Ejercicio 1: Creando directorios después del Virus.

Realiza un Shell Script llamado : `setupz.sh` que cree en la carpeta personal del usuario (`$HOME`), la siguiente estructura 
de directorios:

\begingroup
\centering

![ZombiExam](imgs/exam2022-tarea02.png)\
\endgroup

Antes de crear **cada uno** de los directorios (incluido `AfterTheVirus` , el script debe comprobar que existe y en ese caso no debe crearlo, sino mostrar 
un mensaje indicando que no es necesaria su creación.

Realiza otro Shell Script llamado `uninstallz.sh` que cuando se ejecute, si el directorio `AfterTheVirus` está presente en el `$HOME` del usuario, debe 
borrar el directorio `AfterTheVirus`. 

\newpage
# Ejercicio 2: Corrigiendo con cabezas de Zombies.

El siguiente fragmento de código tiene un error, corríjelo:

```Shell
#!/bin/bash

# Este script debe mostrar el numero de zombies restantes en las calles
# despues de que la patrulla vaya eliminando los zombies.

# Al comienzo existen : 100 zombies
ZOMBIES=100

# Y la patrulla recorre las calles 10 veces, eliminando cada vez un 
# zombie mas que la vez anterior, empezando por 1.

# NUMERO DE VECES 
NUMVECES=10

ZOMBIESQUEMATALAPATRULLA=1

for vez in $(seq 1 $NUMVECES); do
	let ZOMBIES=$ZOMBIES-$ZOMBIESQUEMATALAPATRULLA
	
	echo " Esta es la vez numero $vez "
	echo " Quedan $ZOMBIES "

done

exit 0

```

\newpage
# Ejercicio 3: Operaciones acerca de Zombies!

Realiza un Shell Script que acepte sólo 3 parámetros (en caso contrario mostrar un mensaje de error):

1. OPERACION A REALIZAR
2. NIVEL_ACTUAL
3. NUEVO_NIVEL

Las operaciones pueden ser: 

- *IncrementaNivel* -> Entonces debe indicar cuantos niveles de **diferencia** hay entre NIVEL_ACTUAL y NUEVO_NIVEL. Si el resultado es negativo, mostrar un 0.
- *IncrementaZombies* -> El programa debe calcular cuantos zombies hay entre los dos niveles junto a la palabra `Zombies`, que es el resultado de multiplicar la diferencia de nivel por 77. 
En caso de que hayan más de 777 zombies, debe mostrar un mensaje:

```shell
DEMASIADOS ZOMBIES...HUYE!
```

## Ejemplos

```shell
smx@maquina:~$ ./ejercicio3.sh IncrementaNivel 2 6
4

smx@maquina:~$ ./ejercicio3.sh IncrementaZombies 2 12
770 Zombies

smx@maquina:~$ ./ejercicio3.sh IncrementaZombies 1 13
924 Zombies
DEMASIADOS ZOMBIES...HUYE!

```

\newpage
# Ejercicio 4: Buscando Cerebros y el final

Realiza un Shell Script que compruebe que se le están pasando sólo 2 argumentos como máximo, en caso de que se le indiquen menos o más de 2, que muestre el mensaje:

```Shell
DEMASIADOS ARGUMENTOS
```

Y ahora, que realice las siguientes operaciones en función de los argumentos:

- Si el primer argumento es `CEREBROOOS`, debe mostrar las líneas del fichero `ZombieIpsum.txt` que contienen la palabra *cerebro* en cualquier combinación de mayúsculas y minúsculas.
- Si el primer argumento es `SOMOSLEYENDA`, debe mostrar las líneas del fichero `ZombieIpsum.txt` que contienen la palabra *apocalypsi* en cualquier combinación de mayúsculas y minúsculas.

\begingroup
\centering
![Zombie Ipsum](imgs/zombieIpsum.png)\
\endgroup
