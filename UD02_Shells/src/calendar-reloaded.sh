#!/bin/bash

echo -n " Month : "
read MONTH

echo -n " Day of the Week (Su/Mo/Tu/We/Th/Fr/Sa) : "
read DOW

echo " "
echo " "
# Days of this month?
DOM=0

# Day of the Week number?
DOWNUM=0

if [ $MONTH -eq 2 ]; then
    DOM=28
elif [ $MONTH -eq 4 -o $MONTH -eq 6 -o $MONTH -eq 9 -o $MONTH -eq 11 ]; then
    DOM=30
else
    DOM=31
fi

# Day of the week number
if [ $DOW = "Mo" ]; then
    DOWNUM=1
elif [ $DOW = "Tu" ]; then
    DOWNUM=2
elif [ $DOW = "We" ]; then
    DOWNUM=3
elif [ $DOW = "Th" ]; then
    DOWNUM=4
elif [ $DOW = "Fr" ]; then
    DOWNUM=5
elif [ $DOW = "Sa" ]; then
    DOWNUM=6
fi


# Display the days

echo -e "\tSu\tMo\tTu\tWe\tTh\tFr\tSa"
echo -en "\t"

if [ $DOWNUM -gt 0 ]; then

    for blanks in $(seq 1 $DOWNUM); do
	echo -ne "\t"
    done
    
fi

for day in $(seq 1 $DOM); do

    echo -ne "$day\t"

    let FINDESEMANA=($day-1+$DOWNUM)%7
    if [ $FINDESEMANA -eq 6 ]; then
	echo -ne "\n\t"
    fi

done

echo " "
echo " "

exit 0
