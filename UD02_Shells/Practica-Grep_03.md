---
title: Practica 06 - Grep y Cthulhu
subtitle: Unidad 02
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---
\maketitle
\tableofcontents

# Grep y Cthulhu

Con lo explicado en clase y estos pequeños apuntes que adjuntamos,
entregar un script con los comandos necesarios para resolver
cada uno de los puntos que se plantean sobre `TheCallOfCthulhu.txt`.

Ese fichero (`TheCallOfCthulhu.txt`) podéis descargarlo de Aules.

Entregar los comandos que resuelven los siguientes puntos de la Llamada
de Cthulhu.

## Pistas

```shell
$ cat TheCallOfCthulhu.txt | grep 
```


## Script

Debéis hacer un script que muestre un mensaje de cada una de las siguientes búsquedas y a continuación muestre 
las lineas qué hacen \"matching con las siguientes peticiones.

-   Comienzan por \"T\" y acaban en \"n\".
-   Contienen a \"Jhon\"
-   Contienen a \"Cthulhu\" pero no empiezan por él.
-   Contienen a \"would\" y acaba en consolante.
-   Comienza por vocal y acaba en vocal.
-   Comienza por A o por T.
-   Comienza por N o M y acaba en Vocal.
-   Contienen la palabra \"Cult\", en minúscula o mayúscula.
-   Contienen una palabra entre comillas.
-   Contienen una palabra con admiración, pero no acaba en ella.
-   Contienen 2 palabras con \"-\".
-   Contiene la palabra R'lyeh.
-   Contiene la palabra Cthulhu **y** R'lyeh.
-   Contiene la palabra Cthulhu **o** R'lyeh.
