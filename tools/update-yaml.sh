#!/bin/bash
#

UNIT=$1


for f in $(find $UNIT -name "*.md"); do 

	echo " * WiP : $f"
	echo " - Date"
	sed -i "s/^date:.*/date: /" $f
	sed -i "s/^titlepage-background:.*/titlepage-background: \"..\/rsrc\/backgrounds\/background-title-2025.pdf\"/" $f
	sed -i "s/^page-background:.*/page-background: \"..\/rsrc\/backgrounds\/background-2025.pdf\"/" $f
	sed -i "s/^titlepage-text-color:.*/titlepage-text-color: \"000000\"/" $f
	sed -i "s/^subtitle:.*/subtitle: Recover/" $f

	

done


exit 0
