#!/home/aberlanas/GitLab/yaml-env/bin/python

import sys
import os
import frontmatter

print (" * Welcome to the poor parser ")

if len(sys.argv) != 2 :
    print(" USAGE ")
    sys.exit(1)


filetoparse = sys.argv[1]

if not os.path.exists(filetoparse):
    print(" * Working with "+str(filetoparse))
    print(" AND IT IS NOT A FILE")
    sys.exit(1)


ejermd = frontmatter.load(filetoparse)

print(" TITULO -> "+ejermd['title'])

sys.exit(0)
