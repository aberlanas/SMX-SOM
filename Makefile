#!/usr/bin/make -f

#TEMPLATE_TEX_PD="rsrc/templates/pd-nologo-tpl.latex"
# Colors
BLUE= \e[1;34m
LIGHTBLUE= \e[94m
LIGHTGREEN= \e[92m
LIGHTYELLOW= \e[93m

RESET= \e[0m

# Templates 
TEMPLATE_TEX_PD="../rsrc/templates/eisvogel.latex"
PANDOC_OPTIONS="-V fontsize=12pt -V mainfont="../rsrc/sorts-mill-goudy/OFLGoudyStM.otf" --pdf-engine=xelatex "
TEMPLATE_TEX_TASK="../rsrc/templates/eisvogel.latex"

# PDFS
PDF_PATH:=$(shell readlink -f PDFS)

ACEPTA_EL_SCRIPT="AceptaElScript"
UNIT01_NAME_PATH="UD01_IntroduccionSistemasOperativos"
UNIT02_NAME_PATH="UD02_PowerShell-BASH"
UNIT03_NAME_PATH="UD03_Procesos"
UNIT04_NAME_PATH="UD04_ProcessAndNetwork"
UNIT05_NAME_PATH="UD05_UsersGroupsAndPermissions"
UNIT06_NAME_PATH="UD06_FileSystems"
UNIT07_NAME_PATH="UD07_Projects"
UNIT0X_NAME_PATH="UD0X_Recover"

# Units 
UNIT01_DIR:=$(shell readlink -f $(UNIT01_NAME_PATH))
UD01_FILES := $(wildcard $(UNIT01_DIR)/*.md)

UNIT02_DIR:=$(shell readlink -f $(UNIT02_NAME_PATH))
UD02_FILES:=$(wildcard $(UNIT02_DIR)/*.md)

ACEPTA_EL_SCRIPT_DIR=$(shell readlink -f $(ACEPTA_EL_SCRIPT))
ACEPTA_EL_SCRIPT_FILES=$(wildcard $(ACEPTA_EL_SCRIPT_DIR)/*.md)

UNIT03_DIR:=$(shell readlink -f $(UNIT03_NAME_PATH))
UD03_FILES:=$(wildcard $(UNIT03_DIR)/*.md)

UNIT04_DIR:=$(shell readlink -f $(UNIT04_NAME_PATH))
UD04_FILES:=$(wildcard $(UNIT04_DIR)/*.md)

UNIT05_DIR:=$(shell readlink -f $(UNIT05_NAME_PATH))
UD05_FILES:=$(wildcard $(UNIT05_DIR)/*.md)

UNIT06_DIR:=$(shell readlink -f $(UNIT06_NAME_PATH))
UD06_FILES:=$(wildcard $(UNIT06_DIR)/*.md)

UNIT07_DIR:=$(shell readlink -f $(UNIT07_NAME_PATH))
UD07_FILES:=$(wildcard $(UNIT07_DIR)/*.md)

UNIT0X_DIR:=$(shell readlink -f $(UNIT0X_NAME_PATH))
UD0X_FILES:=$(wildcard $(UNIT0X_DIR)/*.md)

UNIT0Z_DIR:=$(shell readlink -f Unit0Z-Recover)
UD0Z_FILES:=$(wildcard $(UNIT0Z_DIR)/*.md)


# RULES

clean:
	@echo " [${BLUE} * Step : Clean ${RESET}] "
	@echo "${LIGHTBLUE} -- PDFS ${RESET}"
	rm -f PDFS/*.pdf
	rm -f PDFS/*.odt


files:
	@echo " [${BLUE} * Step : Files ${RESET}] "
	@echo "${LIGHTBLUE} * Creating folder [ PDFS ]${RESET}"
	mkdir -p PDFS

prog-didactica: files
	@echo " [ Step : prog-didactica ]"
	@echo " * [ PDF ] : Programacion Didactica ..."
	
	@cd ProgramacionDidactica/ && pandoc --template $(TEMPLATE_TEX_PD) $(PANDOC_OPTIONS) -o $(PDF_PATH)/ProgramacionDidactica_SOM.pdf ./PD_*.md

	@echo " * [ ODT ] : Programacion Didactica ..."
	@cd ProgramacionDidactica/ && pandoc -o $(PDF_PATH)/ProgramacionDidactica_SOM.odt ./PD_*.md 
	
	@echo " * [ PDF Result ] : $(PDF_PATH)/ProgramacionDidactica_SOM.pdf"
	evince $(PDF_PATH)/ProgramacionDidactica_SOM.pdf


unit-acepta-el-script: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} UD 01 - ${UNIT01_NAME_PATH} ${RESET}] "

	@for f in $(ACEPTA_EL_SCRIPT_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(ACEPTA_EL_SCRIPT) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done

unit-01: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} UD 01 - ${UNIT01_NAME_PATH} ${RESET}] "

	@for f in $(UD01_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT01_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done

unit-02: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 02 - Scripting ${RESET}] "

	@for f in $(UD02_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT02_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	

unit-03: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "

	@echo " [${LIGHTGREEN} Unit 03 - Procesos y Red ${RESET}] "

	@echo " [${LIGHTGREEN} Unit 03 - Procesos ${RESET}] "


	@for f in $(UD03_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT03_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	


unit-04: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 04 - Network and Process ${RESET}] "

	@for f in $(UD04_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT04_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    	done	

unit-05: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 05 - Users, Groups and Permissions ${RESET}] "
	@for f in $(UD05_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT05_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --highlight-style zenburn -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done	

unit-06: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 06 - FileSystems ${RESET}] "
	@for f in $(UD06_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT06_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --highlight-style zenburn -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done


unit-07: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 07 - Projects ${RESET}] "
	@for f in $(UD07_FILES); do \
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT07_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --highlight-style zenburn -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done

unit-0z: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 0Z - Recover 2022 ${RESET}] "

	@for f in $(UD0Z_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT0Z_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
        done	



unit-0x: clean files

	@echo " [${BLUE} * Step : Compiling PDF ${RESET}] "
	@echo " [${LIGHTGREEN} Unit 03 - Software and Updates ${RESET}] "

	@for f in $(UD0X_FILES); do \
		#echo $$f ;\
    	echo " - ${LIGHTYELLOW} Working${RESET} with: `basename $$f`";\
		cd $(UNIT0X_DIR) && pandoc $$f --template $(TEMPLATE_TEX_TASK) $(PANDOC_OPTIONS) --from markdown --listings -o $(PDF_PATH)/`basename $$f .md`.pdf ;\
    done	
