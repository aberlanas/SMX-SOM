---
title: Task 04 - CLIL - IT Vocabulary
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Pre-Requisites

Without Internet or online translators.

# IT Vocabulary 

Explain in english (with your own words), what is the meaning of the next words:

All of them are in an IT Context.

| IT Vocabulary |||| 
|---------------|--------|------|--------|
| Warning       | Advice | File | Folder |
| Error         | Found  | Configuration | Setup |
| Execution     | Stop   | Permission | Allow |
| Compressed    | Bold   | Italic     | Underscore |
| Dot           | Wide   | Web Page   | Keyboard |
| Mouse         | Pointer| Click      | Secondary Button |
| Command       | Executable | Printer | Service | 
| Charger       | BitCoin | BlockChain | BotNet |
| Dark Web      | Internet Of Things | Kernel | Provider |
| Network       | License | Phising  | eMail  | Social Network | 

\newpage

# Read Comprehension 

Lee el texto y luego contesta a las preguntas.

Title: The Impact of Artificial Intelligence on Healthcare

Artificial intelligence (AI) is revolutionizing the healthcare industry in ways that were once thought to be the stuff of science fiction. With the rapid advancement of technology, AI has found a crucial role in various aspects of healthcare, from diagnostic tools to patient care.

One of the primary areas where AI is making a significant impact is in medical diagnosis. Machine learning algorithms can analyze vast amounts of medical data to identify patterns and anomalies that may go unnoticed by human doctors. This has the potential to lead to earlier and more accurate diagnoses, which can be life-saving in conditions like cancer.

AI is also being used to enhance patient care. Chatbots and virtual nurses can provide round-the-clock assistance to patients, answering questions and reminding them to take their medications. Additionally, robotic surgery systems equipped with AI can assist surgeons in performing intricate procedures with greater precision.

In drug discovery, AI is speeding up the process of identifying potential new medications. By analyzing massive datasets, AI algorithms can predict how different compounds will interact with biological systems, accelerating the development of new drugs.

However, the adoption of AI in healthcare is not without challenges. Concerns about data privacy and security must be addressed to ensure patient information is protected. Furthermore, healthcare professionals need training to effectively utilize AI tools and interpret their outputs.

In conclusion, artificial intelligence is reshaping the landscape of healthcare, offering the potential for earlier diagnoses, improved patient care, and accelerated drug development. As technology continues to advance, it's likely that AI will play an even more prominent role in the future of healthcare.

## Preguntas

1. ¿Qué es lo que hace posible que la inteligencia artificial (AI) tenga un impacto significativo en el campo de la salud?
2. ¿En qué área de la atención médica se destaca la inteligencia artificial en cuanto al diagnóstico?
3. ¿Qué ventajas se mencionan en el texto sobre el uso de chatbots y enfermeras virtuales?
4. ¿Cómo puede la inteligencia artificial acelerar el proceso de descubrimiento de medicamentos?
5. ¿Cuáles son algunos de los desafíos asociados con la adopción de la inteligencia artificial en la atención médica, según el texto?
6. ¿Cuál es la conclusión principal del texto en cuanto al impacto de la inteligencia artificial en la salud?
