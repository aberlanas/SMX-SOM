---
title: Practica 02 - Averiguando la IP
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---


# Tarea: Averiguando la IP

Una de las primeras preguntas que se hacen l@s técnic@s de microinformática, l@s administrador@s de sistemas, e incluso l@s desarrollador@s para poder averiguar **qué está pasando** és:

*¿Cual es mi ip?*

La IP de un ordenador lo identifica de manera única en una Red de ordenadores (o debería). Y forma parte del protocolo de Internet para poder establecer comunicación **exitosa** con cualquier máquina disponible en la red de redes (*aka*: Internet).

Existen multitud de herramientas en los sistemas operativos que nos permite obtener la IP (o IPs) que tenemos actualmente asignadas en el Sistema.

Por ahora vamos a ver dos mecanismos:

- Uno gráfico (para enseñarle al cliente).
- Uno por consola (para nosotr@s).

## Gráfico

Busca en tu Escritorio Xfce4 donde puedes obtener información acerca de la dirección(es) IP asignadas y realiza manual paso a paso (para personas que no conocen nada), que les indique cómo pueden averiguar esa IP.

## Consola

Adjunto un trozo de un shell script. Complétalo con lo necesario para que muestre información detallada de todas las direcciones IP que tiene el Sistema Operativo GNU/LinuX habilitado. Por que si ejecutáis el script, tal y como os lo dejo, no funciona del todo bien.

```shell
#!/bin/bash

# This script is licensed under GPL v3 or Higher.

ip 

exit 0
```

Recordad que una vez se ha creado el fichero en texto plano que es el script, debéis darle permisos de ejecución.

```shell
chmod +x ./RUTA_RELATIVA_AL_SCRIPT.sh
```

Y luego ya lo podréis ejecutar:

```shell
./RUTA_RELATIVA_AL_SCRIPT.sh
```




