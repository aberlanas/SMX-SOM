---
title: Task - CLIL - Exam, part I
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Question 1 (Multiple Choice):

Which of the following best describes an Absolute Path in a Linux FileSystem?

- A) A path that starts from the root directory ("/") and specifies the full directory structure to a file or directory.
- B) A path that is relative to the user's home directory.
- C) A path that starts from the current working directory and specifies the location of a file or directory.
- D) A path that contains only file names without any directory information.

# Question 2 (Multiple Choice):

What is a Relative Path in Linux?

- A) A path that starts from the root directory ("/").
- B) A path that starts from the user's home directory.
- C) A path that is referring to the current working directory.
- D) A path that always points to the same file or directory regardless of the context.

# Question 3 (Written Response):

Imagine you are currently in the directory `/home/smx/Documents/SomExam/` in a Linux system. 
Write the relative path to the directory `/home/smx/Pictures/`.

# Question 4 (Written Response):

You are in the directory `/var/www/index/` on a Linux server, and you need to navigate to a file named index.html located in the directory `/var/www/html/website/`. 
Write the relative path to access this file.


# Question 5 (Multiple Choice):

Which command can be used to change the current working directory in Linux?

- A) `pwd`
- B) `cd`
- C) `ls`
- D) `cat`

# Question 6 (Written Response):

Using the Terminal get the Absolute Path to this file.

# Question 7 (Multiple Choice):

You are currently in the directory /home/smx/documents/. If you want to navigate to a subdirectory called images located within the documents directory, what relative(s) path should you use?

- A) `../images`
- B) `./images`
- C) `/home/smx/documents/images`
- D) `images/`

# Question 8 (Multiple Choice):

You are in the directory /var/www/ on a Linux server, and you need to access a file named page.html located in a subdirectory called public. 
What relative path will allow you to access this file?

- A) `./public/page.html`
- B) `../public/page.html`
- C) `/var/www/public/page.html`
- D) `public/page.html`

# Question 9 (Multiple Choice):

You are currently in the directory /home/user/music/rock/. If you want to go up two levels to the user directory, what is the correct relative path?

- A) `../../user`
- B) `../user`
- C) `/home/user`
- D) `user`

# Question 10 (Multiple Choice):

You are working in the directory /usr/local/bin/ and need to access a script named myscript.sh located in the parent directory (/usr/local/). What is(are) the correct(s) relative path(s) to reach this script?

- A) `../myscript.sh`
- B) `./myscript.sh`
- C) `/usr/local/myscript.sh`
- D) `myscript.sh`

