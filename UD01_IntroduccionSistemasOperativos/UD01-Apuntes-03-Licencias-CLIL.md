---
title: Software Licencing Basics
subtitle: Unit 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


# Software Licensing Basics

Software licensing is the legal framework that defines how software can be used and distributed. When users purchase software, they are often buying a license to use it under specific conditions, not the software itself. There are various types of licenses, each with different rights and restrictions. Below are the key concepts and most common types of software licenses.

## Key Concepts

* **License Agreement**: A legal contract between the software developer or owner and the user.
* **User Rights**: Defines what the user is allowed to do with the software (e.g., install, modify, share).
* **Proprietary Software**: Software owned by an individual or company, where the source code is not available to the public.
* **Open-Source Software**: Software whose source code is made freely available for anyone to use, modify, and distribute.

# Types of Software Licenses

## Proprietary License

Grants limited rights to the user.
The user cannot modify or redistribute the software.

*Examples*: Microsoft Windows, Adobe Photoshop.

## Freeware

Software that is free to use but often with restrictions on modification or redistribution.

*Example*: Google Chrome.

## Shareware

Software that is free for a trial period, after which a license fee is required. 

*Example*: WinRAR.

## Open Source License

Allows users to freely use, modify, and distribute the software.
Common licenses include GPL (General Public License) and MIT License.

*Example*: Linux Kernel, Mozilla Firefox.

## Public Domain

Software that is not copyrighted and can be freely used, modified, and distributed.

*Example*: SQLite

## Abandonware

Abandonware is a product, typically software, ignored by its owner and manufacturer, which can no longer be found for sale, and for which no official support is available and cannot be bough.

Within an intellectual rights contextual background, abandonware is a software (or hardware) sub-case of the general concept of orphan works. Museums and various organizations dedicated to preserving this software continue to provide legal access.

*Example*: Burgle Bros

## Intellectual Property

Intellectual property (IP) is a category of property that includes intangible creations of the human intellect. There are many types of intellectual property, and some countries recognize more than others. The best-known types are patents, copyrights, trademarks, and trade secrets. The modern concept of intellectual property developed in England in the 17th and 18th centuries. The term "intellectual property" began to be used in the 19th century, though it was not until the late 20th century that intellectual property became commonplace in most of the world's legal systems.

## Key Considerations

* **License Fees**: Some licenses require one-time or recurring payments to continue using the software.
* **Updates and Support**: Proprietary software often comes with professional support and regular updates.
* **Compliance**: It’s important to comply with the terms of the license to avoid legal issues.

# Task 01

Make a folder in you Home Directory (`$HOME`) with the name: `SOM/`, inside another folder named `Licenses/`. In this folder create the next structure:

```shell
SOM/Licenses/
            Propietary-Licenses/
                    - propietary.txt
                    - shareware.txt
                    - freeware.txt
            OpenSource-Licenses/
                    - GPL.txt
                    - BSD.txt
            Other/
                    - abandoware.txt
                    - IP.txt
```

The files with the extension : `.txt` will be replenish in the next task, for now create them empty. 

# Task 02 

For each file with *.txt* extension, copy and paste inside them the description previously given. 

# Task 03 

Make a plain text document, with the commands that has been used for create and add a command description for each one of them.



