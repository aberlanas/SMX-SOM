---
title: Examen 01 - Introducción y nano
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Examen: Introducción y nano

## Tarea 01

Usando el editor de texto `nano`, escribe un fichero de texto contestando a las siguientes preguntas.

 - ¿Qué Sistemas Operativos has utilizado?.
 - ¿Qué es el **texto plano** si hablamos de formatos de ficheros?
 - ¿Qué es un fichero (desde el punto de vista del Sistema Operativo?
 - Describe cual es la función de las *extensiones* en los nombres de los ficheros.
 - ¿Cómo se copia y pega una línea en `nano`? Describe con detalle el proceso.
 - ¿Cómo se desplaza directamente el curso a una determinada línea en `nano`? Describe con detalle el proceso.
 - ¿Cómo se ejecutaría la opción de *Guardar y Salir* en `nano`?
 
**Entrega**: Un fichero llamado: `UD01-Exam01-Task01.txt`
 
## Tarea 02

Traduce al castellano el siguiente texto:

*nano  is  a  small and friendly editor.  It copies the look and feel of*
*Pico, but is free software, and implements several features  that  Pico*
*lacks,  such as: opening multiple files, scrolling per line, undo/redo,*
*syntax coloring, line numbering, and soft-wrapping overlong lines.*

*When giving a filename on the command line, the cursor can be put on  a*
*specific line by adding the line number with a plus sign (+) before the*
*filename.*

No os preocupéis si no conocéis determinadas palabras, traducid lo que podáis o explicadme
de qué va el texto. La intención de esta prueba es determinar cual es el nivel de inglés que 
tenéis.

**Entrega**: Un fichero llamado: `UD01-Exam01-Task02.txt`