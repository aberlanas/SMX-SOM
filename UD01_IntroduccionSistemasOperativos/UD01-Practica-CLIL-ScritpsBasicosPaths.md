---
title: Exam - CLIL - Scripts and Paths
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# A little (exam) tasks 

Read all the questions and instructions before start.

First of all, create the next folder structure in your Home directory:

```shel

exam-paths/
          Pacific-ocean/
                    Australia/
                    Indonesia/
                    Malaysia/
                    Japan/
                    America/
          Atlantic-ocean/
                    America/
                    Europe/
                    Caribbean/
                    Africa/
          Indian-ocean
                    Madagascar/
                    Australia/
                    Africa/
          Southern-ocean
                    Africa/
                    America/
                    Australia/

```

All of this folders must be *case-sensitive* (UpperCase and lowercase letters **matters**).

This structure will be used along the exam.

## Task 1

Create a Script in the `Pacific-Ocean` folder that using **relative paths**, test for each subdirectory of Pacific if
this subdirectory is present in the other oceans (in all of them). For each one, display a message if the folder exists
with information about the Ocean that is currently being *explored* and nothing if not exists.

## Task 2

Create a Script in the `Atlantic-Ocean` folder that using **relative paths** create a file in Africa subdirectory,
named: `CaboDeBuenaEsperanza.txt`. Then copy it to the other `Africa` folders in the Oceans with the next sanity checks:

- Test if the file exists (yes, `CaboDeBuenaEsperanza.txt`, I know that is just created but everything is possible).
- And then, for each folder : test if the destination folder exists and is a directory.

For each operation or check, display a message using `echo` indicating the current operation.

