---
title: Practica 01 - Introducción y nano
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Tarea: Introducción y nano

Busca en Internet información acerca de los ficheros de texto plano.
Utilizando `nano`, redacta un fichero que conteste a las siguientes
preguntas:

 - ¿Qué significa *abstracción* si estamos hablando de Sistemas Operativos?
 - ¿Qué es el *kernel* de un Sistema Operativo?.
 - ¿Qué Sistemas Operativos has utilizado?.
 - ¿Qué es el **texto plano** si hablamos de formatos de ficheros?

