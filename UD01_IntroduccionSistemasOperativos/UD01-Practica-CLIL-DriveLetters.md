---
title: Task - CLIL - Drive Letters
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Reading 

**Title: The Origin of Drive Letters in Windows Systems**

In Microsoft Windows operating systems, the use of drive letters to represent storage devices, such as hard drives, partitions, and removable media, is a longstanding convention that dates back to the early days of personal computing.

This practice can be traced back to MS-DOS (Microsoft Disk Operating System), the precursor to Windows. MS-DOS used letters to identify different storage locations and devices, and this approach was carried over into the first versions of Windows. The concept behind assigning letters to drives was to provide a simple and intuitive way for users to access their various storage devices.

The drive letters typically start with "A:" and "B:" for floppy disk drives, if present. After that, hard drives and other storage devices are assigned letters starting from "C:" and going onwards in alphabetical order. This letter assignment was based on the order in which the storage devices were detected by the operating system during the boot process.

Over time, as computing technology evolved and the number of available drive letters became insufficient to accommodate all the devices connected to a computer, Windows introduced the concept of "mount points" and extended the drive letter assignment to additional partitions and network drives.

Today, while drive letters remain a fundamental part of Windows' file system organization, users also have the flexibility to assign and change drive letters as needed to suit their preferences and requirements. This system of assigning letters to drives has become a familiar and integral aspect of the Windows user experience.

# Questions

1. What was the original purpose of assigning drive letters in MS-DOS and early versions of Windows?
2. How were drive letters assigned in early Windows operating systems, and why did they start with "C:" for hard drives?
3. What challenges did Windows face as technology evolved, and how did they address the limitation of drive letters?
4. What is the significance of drive letters in the context of Windows' file system organization today?
5. What flexibility do Windows users have regarding drive letter assignment, and how has this flexibility evolved over time?
