---
title: Practica 02 - Averiguando la IP
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---


# Tarea: Averiguando la IP

Una de las primeras preguntas que se hacen l@s técnic@s de microinformática, l@s administrador@s de sistemas, e incluso l@s desarrollador@s para poder averiguar **qué está pasando** és:

*¿Cual es mi ip?*

La IP de un ordenador lo identifica de manera única en una Red de ordenadores (o debería). Y forma parte del protocolo de Internet para poder establecer comunicación **exitosa** con cualquier máquina disponible en la red de redes (*aka*: Internet).

Existen multitud de herramientas en los sistemas operativos que nos permite obtener la IP (o IPs) que tenemos actualmente asignadas en el Sistema.

Por ahora vamos a ver dos mecanismos:

- Uno gráfico (para enseñarle al cliente).
- Uno por consola (para nosotr@s).

## Gráfico

Busca en tu Escritorio Xfce4 donde puedes obtener información acerca de la dirección(es) IP asignadas y realiza manual paso a paso (para personas que no conocen nada), que les indique cómo pueden averiguar esa IP.

## Consola

Utilizando el comando `ip` averigua cuales son los argumentos que habría que pasarle para obtener los siguientes datos de tu conexion:

1. Obtener información de tu conexión y mostrarla utilizando colores.
2. Obtener información de las conexiones que utilizan IP en su versión 4 (IPv4).
3. Obtener si hay "enlace" en una conexión.
4. Obtener estadísticas de una conexión.

## Entrega

Redacta un documento que englobe el manual, así como una serie de capturas de pantalla donde se muestren las opciones de la Consola del comando `ip` que se piden. Expórtalo y súbelo a Aules con nombre:

```shell
SOM-UD01-Practica-02-NombreApellidos.pdf
```


