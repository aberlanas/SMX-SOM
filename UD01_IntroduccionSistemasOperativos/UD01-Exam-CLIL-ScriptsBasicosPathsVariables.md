---
title: Exam - CLIL - Scripts, Variables and Paths
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Exam with Paths and Variables

Read all the questions and instructions before start.

## Task 0

First of all, create the next folder structure in your Home directory **with a script**, you can use `$HOME` variable:

```shell
Seas/
    Mediterranean/
        Italy/
        Spain/
        France/
    China/
        China/
        Malaysia
        Vietnam/
        SouthCorea/
    Caribbean/
        USA/
        Mexico/
        Cuba/
    Northern/
        Germany/
        Denmark/
        Sweden/
        UnitedKingdom/
```

All of this folders must be *case-sensitive* (UpperCase and lowercase letters **matters**).

This structure will be used along the other tasks.

The first check of the script must be if the folder: `Seas` exists previously, and ask the user if wants to delete it. If the user answer is `yes` the script must delete all the structure and then create all the folders. If the answer is not, the script will try to make the structure folder.

* Name of the Script: `Exam1005-task00.sh`

\newpage
## Task 1

Code a Shell script that display all the Seas that are included in the `Seas` folder, and then ask the user for one of them. If the Sea chosen exists, the script must display all folders inside of it. If not, display an error message and do nothing.

* Name of the Script: `Exam1005-task01.sh`

### Useful Commands

* `ls PATH`
* `man test`
* `echo`
* `read`

## Task 2

Code a Shell script that ask the user for a Country, and then check the country in all the Seas, foreach one, display if the Country is present in this Sea Folder or not. If the country not exists in any Sea, the script must display this error message:

```shell
 * Error : Unknown country
```

* Name of the Script: `Exam1005-task02.sh`
