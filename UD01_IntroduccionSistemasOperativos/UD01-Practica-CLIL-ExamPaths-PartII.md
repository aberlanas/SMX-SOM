---
title: Task - CLIL - Absolute Paths
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

**Title: Exploring Relative Paths in File Systems**

In the realm of file systems and directory navigation, relative paths provide a flexible way to refer to files and directories based on the current working directory. Unlike absolute paths, which specify a complete route from the root directory, relative paths define locations relative to the directory where the user or program is currently located.

A relative path doesn't start with a root directory or drive letter but instead begins with a reference to a directory or file in relation to the current location. The most common elements used in relative paths are:

- `.` (dot): Represents the current directory. For example, `./file.txt` refers to a file named "file.txt" in the current directory.
- `..` (dot-dot): Represents the parent directory. Using `../file.txt` would refer to "file.txt" located in the parent directory of the current location.
- Directory names: You can specify directory names directly, such as `subdirectory/file.txt`, which points to "file.txt" in a subdirectory of the current location.

Relative paths are incredibly useful when navigating file systems within a specific context. For instance, in a command-line environment, using relative paths allows you to move between directories efficiently without worrying about the exact location of the files or directories relative to the root.

However, it's essential to note that relative paths are context-dependent. The same relative path may refer to different locations if the current working directory changes. This flexibility can be advantageous when scripting or working within specific directory structures but requires attention to directory organization and context.

In summary, relative paths in file systems provide a way to specify file and directory locations based on the current working directory. They use references like `.` and `..` to navigate within the file system efficiently. While they offer flexibility, users and developers must be aware of their context-dependent nature when using relative paths in file operations.


## Tarea

Traduce el texto, ampliando y explicando todos los conceptos expuestos.
