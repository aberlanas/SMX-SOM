---
title: Task - CLIL - Zorin OS - vars
subtitle: Unit 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "000000"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background7.pdf"
---

# Task 

Create a Plain text file with the name: zorin-os-17-core.var in your SOM/Recipes/ directory.

In this file write the content of the Variables and info explained at class.

```file
# Setting Variables
VARIABLE=VALUE

# Testing Variables

# Some useful information

# Dirty hacks

```
