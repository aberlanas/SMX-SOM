---
title: Introducción a los Sistemas Operativos
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-senia.pdf"
---


Introducción a los Sistemas Operativos
======================================

Introducción
------------

En este primer tema nos centraremos conocer bien ¿Qué es un *Sistema
Operativo*?, así como las funciones que tiene.

Veremos que existen multitud de Sistemas Operativos, cada uno adaptado
al propósito que deben cumplir.

¿Qué es un Sistema Operativo? {#objetivos-del-sistema-operativo}
-----------------------------

Desde el punto de vista del usuario, el sistema operativo consiste en
una serie de programas y funciones que ocultan los detalles del
hardware,ofreciéndole una vía sencilla y flexible de acceso al mismo,
teniendo dos objetivos fundamentales:

### Seguridad

El sistema operativo debe actuar contra cualquier manipulación extraña,
ya sea accidental o premeditada que pudiera dañar la información,
perjudicar a otros usuarios o provocar un funcionamiento indeseado del
sistema. Por ejemplo, hay ciertas instrucciones que pueden parar la
máquina y otras que realizan operaciones directamente sobre el hardware,
que debemos evitar que se utilicen por los programas. Para ello, algunos
sistemas proporcionan dos estados, llamados estado protegido (Sistema o
`Kernel`), en el cual se ejecuta el sistema operativo, y estado no
protegido (Usuario o User), que es el destinado a la ejecución de los
programas de usuario y de aplicación. De esta manera se impide que los
programas de los usuarios puedan tener contacto directo con el hardware,
o puedan forzar un incorrecto funcionamiento del sistema.

[file:ArquitecturaSistemaOperativo/SO\_Capas.PNG](ArquitecturaSistemaOperativo/SO_Capas.PNG)\

### Abstracción

La tendencia actual del software y de los lenguajes de programación, que
iremos viendo en otros cursos, es ocultar lo más posible los detalles de
más bajo nivel, intentando dar a los niveles superiores una visión más
sencilla, global y abstracta, ofreciéndoles operaciones para manipular
dichas estructuras ocultas, desconociendo por completo la gestión
interna de las mismas.

Sobre estas estructuras se construyen otras que abstraen a las
anteriores, y así sucesivamente.

Gracias a la abstracción, los sistemas operativos enmascaran los
recursos físicos, permitiendo su manejo con funciones más generales que
ocultan las básicas, constituyendo verdaderos recursos ficticios o
virtuales, que mejoran y son más potentes que los físicos.

Desde el punto de vista de un programa o usuario, la máquina física se
convierte, en una máquina extendida, que presenta la ventaja respecto a
la física de ofrecer más funciones de las que normalmente soportaría
esta última.

Entre las posibilidades de esto estarían:

-   Las carpetas compartidas.
-   Los usuarios en red.
-   Las impresoras compartidas.
-   Acceso a recursos ajenos al propio *hardware*.

[file:ArquitecturaSistemaOperativo/SO\_MaquinaExtendida.PNG](ArquitecturaSistemaOperativo/SO_MaquinaExtendida.PNG)\
A modo de resumen, podemos decir que el sistema operativo persigue
alcanzar la mayor eficiencia posible del hardware y facilitar el uso del
mismo a los usuarios y a las aplicaciones.

Aviso a Navegantes
------------------

Lo descrito anteriormente da lugar a una situación para la mayor parte
de los usuarios, que es la creencia de que *todo es mágia*.

![](./ArquitecturaSistemaOperativo/mago.jpg)\
Como futuros Administradores de Sistemas debemos conseguir comprender el
funcionamiento de dichos procesos, abstrayéndonos lo necesario en los
primeros momentos, pero profundizando cada vez más para comprender que
es `exactamente` lo que ocurre cuando se producen eventos como :

-   clicks de ratón.
-   enviar a imprimir.
-   crear carpetas.
-   formatear discos.

No seremos buenos profesionales si no tenemos conocimientos acerca de lo
que esas acciones desencadenan y cómo se realizan por parte del Sistema
Operativo.

**¿Por qué esto es tan importante?**

Porque muchas veces (*más de las que nos gustaría*), esos procesos
fallarán y nos devolveran (*o nó :-)*) mensajes de error que nos
permitirán averiguar *qué* está pasando y poco a poco aprenderemos
*cómo* arreglarlos.

A lo largo del curso nos veremos en más de una situación de estas
características. No os preocupéis si no sabéis como arreglar el problema
(*para eso estais aquí*). Lo que se valorará es vuestra capacidad de
analizarlo y plantear posibles soluciones, no la solución en si misma.

`\newpage`{=latex}

Funciones de los Sistemas Operativos
====================================

Las funciones de los sistemas operativos son diversas y han ido
evolucionando de acuerdo con los progresos que la técnica y la
informática han experimentado. Como principales funciones, podríamos
enumerar las siguientes:

### Gestión de procesos

Hay que diferenciar entre los conceptos programa y proceso. Un programa
es un ente pasivo, que cuando se carga en memoria y comienza a
ejecutarse, origina uno o varios procesos. Un **proceso** podríamos
definirlo, como *parte de un programa en ejecución*.

A lo largo de las unidades que vendrán, haremos muchos ejercicios para
la gestión de los procesos.

### Gestión de la memoria

La gestión de memoria, suele ir asociada a la gestión de procesos. Para
ejecutar un proceso es necesario asignarle unas direcciones de memoria
exclusivas para él y cargarlo en ellas, cuando el proceso finalice su
ejecución es necesario liberar las direcciones de memoria que estaba
usando.

![](./imgs/meme-chrome-ram.jpg)\

### Gestión de ficheros

Un fichero es una abstracción para definir una colección de información
no volátil. Su objetivo es proporcionar un modelo de trabajo sencillo
con la información almacenada en los dispositivos de almacenamiento.

Estos ficheros deben tener espacio asignado en los dispositivos, deben
estar protegidos entre ellos, deben organizarse según unos determinados
esquemas... todo esto es la gestión de ficheros.

Parece mucho más difícil de lo que és en realidad. Sin embargo el diablo
está en los detalles.

Una de las máximas que aparecerán a lo largo de todo el curso es:

*Todo en GNU/LinuX es un fichero*.

O sea, que todo lo que se gestiona por parte de los Sistemas Operativos,
incluido él mismo, son ficheros.

Si aprendemos a manejarnos con los ficheros, aprenderemos a gestionar
los Sistemas Operativos y por tanto los Ordenadores.

### Gestión de los dispositivos de E/S

La gestión de la entrada-salida (*aka* *E/S*) tiene como objetivo
proporcionar una interfaz de alto nivel de los dispositivos de E/S
sencilla de utilizar, tanto por parte de propio Sistema Operativo y los
procesos que se ejecutan en él, como por parte del usuario.

Veremos en este punto conceptos como:

-   Drivers (*controladores*).
-   Discos.
-   Impresoras.
-   Monitores.
-   Teclado y Ratón.

### Gestión de la red

El sistema operativo es el encargado de gestionar los distintos niveles
de red, los drivers (controladores) de los dispositivos involucrados en
la red, los protocolos de comunicación, las aplicaciones de red, etc.

Muchas de las prácticas que haremos a lo largo del curso tienen que ver
con este apartado, ya que en el mundo en el que vivimos, casi cualquier
dispositivo *necesita* de una conexión a Internet (o al menos a una red
local (*LAN*)).

### Protección y seguridad

Mecanismos para permitir o denegar el acceso a los usuarios y a sus
procesos a determinados recursos (ficheros, dispositivos de E/S, red,
etc.).

`\newpage`{=latex}

Tipos de Sistemas Operativos
============================

Existen muchas categorizaciones, pero una de las más comunes es la de
los servicios que ofrece.

[file:ArquitecturaSistemaOperativo/SO\_Tipos.PNG](ArquitecturaSistemaOperativo/SO_Tipos.PNG)\

### Según el número de usuarios

1.  Monousuarios

    Los sistemas operativos monousuarios son aquéllos que soportan a un
    usuario a la vez, sin importar el número de procesadores que tenga
    la computadora o el número de procesos o tareas que el usuario pueda
    ejecutar en un mismo instante de tiempo.

    Sistemas Operativos Monousuario:

    -   MS-DOS
    -   Windows 95
    -   Windows 98

2.  Multiusuario

    Los sistemas operativos multiusuario son capaces de dar servicio a
    más de un usuario a la vez, ya sea por medio de varias terminales
    conectadas a la computadora o por medio de sesiones remotas en una
    red de comunicaciones. No importa el número de procesadores en la
    máquina ni el número de procesos que cada usuario puede ejecutar
    simultáneamente.

    Sistemas Operativos Multiusuario:

    -   UNIX-GNU/LinuX
    -   Windows NT (en adelante)

### Sistemas Operativos Distribuidos

Un sistema distribuido se define como una colección de equipos
informáticos separados físicamente y conectados entre sí por una red de
comunicaciones distribuida; cada máquina posee sus componentes de
hardware y software de modo que el usuario percibe que existe un solo
sistema (no necesita saber qué cosas están en qué máquinas). El usuario
accede a los recursos remotos de la misma manera en que accede a
recursos locales ya que no percibe que existan varios ordenadores, sino
que solo es capaz de ver uno formado por todos los anteriores. Una
ventaja fundamental de los sistemas distribuidos, es que permiten
aumentar la potencia del sistema informático, de modo que 100
ordenadores trabajando en conjunto, permiten formar un único ordenador
que sería 100 veces más potente que un ordenador convencional.

Los sistemas distribuidos son muy confiables, ya que si un componente
del sistema se estropea otro componente debe de ser capaz de
reemplazarlo, esto se denomina **Tolerancia a Fallos**.

El tamaño de un sistema distribuido puede ser muy variado, ya sean
decenas de hosts (red de área local), centenas de hosts (red de área
metropolitana), y miles o millones de hosts (Internet); esto se denomina
escalabilidad. De hecho, si un ordenador formando por un sistema
distribuido se queda \"corto\" para las necesidades de la empresa, basta
con instalar más.

La computación distribuida ha sido diseñada para resolver problemas
demasiado grandes para cualquier supercomputadora y mainframe, mientras
se mantiene la flexibilidad de trabajar en múltiples problemas más
pequeños.

Esta forma de computación se conoce como **grid**. Los grandes retos de
cálculo de hoy en día, como el descubrimiento de medicamentos,
simulación de terremotos, inundaciones y otras catástrofes naturales,
modelización del clima/tiempo, grandes buscadores de internet, el
programa /[Seti\@Home/](http://setiweb.ssl.berkeley.edu/), etc. Son
posibles gracias a estos sistemas operativos distribuidos que permiten
utilizar la computación distribuida.

El modelo de computación de ciclos redundantes, también conocido como
*computación zombi*, es el empleado por aplicaciones como *Seti\@Home*,
consistente en que un servidor o grupo de servidores distribuyen trabajo
de procesamiento a un grupo de computadoras voluntarias a ceder
capacidad de procesamiento no utilizada. Básicamente, cuando dejamos
nuestro ordenador encendido, pero sin utilizarlo, la capacidad de
procesamiento se desperdicia por lo general en algún protector de
pantalla, este tipo de procesamiento distribuido utiliza nuestra
computadora cuando nosotros no la necesitamos, aprovechando al máximo la
capacidad de procesamiento. La consola PS3 también cuenta con una
iniciativa de este tipo.

Otro método similar para crear sistemas de supercomputadoras es el
**clustering**

Un **cluster** o racimo de computadoras consiste en un grupo de
computadoras de relativo bajo costo conectadas entre sí mediante un
sistema de red de alta velocidad (gigabit de fibra óptica por lo
general) y un software que realiza la distribución de la carga de
trabajo entre los equipos. Por lo general, este tipo de sistemas cuentan
con un centro de almacenamiento de datos único. Los clusters tienen la
ventaja de ser sistemas redundantes, si falla un equipo se resiente un
poco la potencia del cluster, pero los demás equipos hacen que no se
note el fallo.

Algunos sistemas operativos que permiten realizar **clustering** o
**grid**, son:

-   Amoeba
-   BProc
-   DragonFly BSD
-   Génesis
-   Kerrighed
-   Mosix/OpenMosix
-   Nomad
-   OpenSSI
-   Plurid

Un cluster que usamos habitualmente, es el que forma **Google**. Se
estima que en 2010 usaba unos 450.000 ordenadores, distribuidos en
varias sedes por todo el mundo y formando clusters en cada una de dichas
sedes.

Cada cluster de Google está formado por miles de ordenadores y en los
momentos en que se detecta que el sistema está llegando al límite de su
capacidad, se instalan cientos de ordenadores más en pocos minutos,
aumentado así la potencia de cada cluster. Estos equipos normalmente con
ordenadores x86 como los que solemos usar nosotros, tienen instalada
versiones especiales de Linux, modificadas por Google para que permitan
la formación de estos clusters.

[file:ArquitecturaSistemaOperativo/SO\_Google.PNG](ArquitecturaSistemaOperativo/SO_Google.PNG)\
En la imagen anterior podemos ver el primer servidor funcional que uso
**Google**. Como vemos, se basa en varios ordenadores instalados
conjuntamente, a los que se les retiró simplemente la caja externa para
dejar solo su contenido, a fin de aprovechar espacio en los armarios de
comunicaciones.


