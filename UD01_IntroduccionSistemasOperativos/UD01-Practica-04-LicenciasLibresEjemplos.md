---
title: Practica 04 - Licencias Libres
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---


Tarea : Ejemplos de Licencias Libres
=======================================

Vamos a buscar aplicaciones que tengan licencias catalogadas como
*libres* (no confundir con **gratis**), que podamos instalar en nuestra
casa y en varios Sistemas Operativos.

En la primera parte de la práctica, buscaremos la licencia de
determinados programas, mientras que en la segunda buscaremos programas
que tengan una determinada licencia.

Licencias de Programas
----------------------

Para cada uno de los programas listados a continuación, busca su
licencia en los menús o ficheros instalados del programa y adjunta una
captura donde se muestre la licencia.

Rellena de paso la siguiente tabla a medida que vayas buscando cada uno
de los programas.

  Programa           Licencia
  ------------------ ----------
  LibreOffice        
  Mozilla Firefox    
  Mousepad           
  VideoLan           
  Gimp               
  Scite              
  WireShark          
  Chromium Browser   

En caso de que en vuestro ordenador no haya algún programa de la lista,
busca alguna alternativa que ya tengas instalad@ y describe la licencia.
Intentad llegar a 8 programas, todos con las licencias en la categoría
de Libres que hemos visto en clase y en la teoría.

Programas con Licencias
-----------------------

Ahora el reto es diferente, se trata de buscar en Internet Software que
instalarías en casa o en una pequeña empresa que tenga las siguientes
licencias:

  Licencia                 Software
  ------------------------ ----------
  GPL                      
  GPL v3                   
  Mozilla Public License   
  BSD                      
