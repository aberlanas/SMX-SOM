---
title: Task - CLIL - Scripts, Variables and Paths
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Tasks with Paths and Variables

Read all the questions and instructions before start.


## Task 1

First of all, create the next folder structure in your Home directory **with a script**:

```shel

ocean-paths/
          Pacific-ocean/
                    Australia/
                    Indonesia/
                    Malaysia/
                    Japan/
                    America/
                    NewZealand/
          Atlantic-ocean/
                    America/
                    Europe/
                    Caribbean/
                    Africa/
                    Greenland/
                    Iceland/
          Indian-ocean
                    Madagascar/
                    Australia/
                    Africa/
                    Indonesia/
                    Arabia/
          Southern-ocean
                    Africa/
                    America/
                    Australia/
                    NewZealand/


```

All of this folders must be *case-sensitive* (UpperCase and lowercase letters **matters**).

This structure will be used along the other tasks.

\newpage
## Task 1

Code a Shell Script that ask the user for an Ocean, and if the Ocean exists, creates inside the Ocean
a file named: `sailing-this.txt`. If not exists, the script must display an error message.

### Exploring 

You can ask only for the name of the Ocean, and then concatenates the `-ocean` part of the folder name.

## Task 2

Code a Shell Script that ask the user for an Island or Continent name, and then count all occurrences
of this Islando or Continent in the *four* oceans. Displays a different message if the script is unable
to found the name in the Oceans.


