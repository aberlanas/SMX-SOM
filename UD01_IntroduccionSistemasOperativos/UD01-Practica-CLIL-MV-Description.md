---
title: Task - CLIL - VM Description
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Task 

Create a Plain text file with the name: zorin-os-17-core.txt in your SOM/Recipes/ directory.

In this file write the next info:

```file
VMNAME: 
OS:
NUM_HARDDISKS:
NUM_CORES:
RAM:
SIZE_HARDDISKS:
NETWORK_ADAPTERS:
NETWORK_CONFIGURATION:
ADMIN_USER:
ADMIN_PASSWORD:
```
