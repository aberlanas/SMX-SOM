---
title: Task - CLIL - Scripts and Paths
subtitle: Unidad 01
author: Angel Berlanas Vicente
header-includes: |
lang: es-ES
keywords: [SMX, SOM]
titlepage: true,
page-background: "../rsrc/backgrounds/background-senia.pdf"
page-background-opacity: 1
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "360049"
titlepage-rule-height: 0
titlepage-background: "../rsrc/backgrounds/background-title-task.pdf"
---

# Some scripts (Spanish)

Para todos los scripts, se agradecerán mensaje de estado que muestren cómo se está ejecutando.

## Script 01

Haz un script que muestre la fecha actual, a continuación cree un directorio en `/tmp/practicas/` llamado `fecha` y dentro un fichero que se llame `farmeo.txt`.

Nombre del Script: `script_path_1.sh`

## Script 02

Haz un script que compruebe que el directorio `/tmp/farmeando/` existe y en caso contrario que lo cree.

Nombre del Script: `script_path_2.sh`

## Script 03

Haz un script que si existe el directorio `/tmp/practicas` lo **copie** a `/tmp/farmeando/` y si no existe cualquiera de ellos que muestre un mensaje de error.

Nombre del Script: `script_path_3.sh`

## Script 04

Haz un script que compruebe que `/etc/apt/sources.list` existe y si existe que muestre las ultimas 5 líneas.

Nombre del Script: `script_path_4.sh`

## Script 05

Haz un script que compruebe que `/etc/issue` existe y si existe que muestre las 2 primeras lineas del fichero.

Nombre del Script: `script_path_5.sh`

## Script 06

Haz un script que compruebe que el directorio `/tmp/examen/de/farmeo` no existe y si no existe, que lo cree y luego copie el fichero `/etc/apt/sources.list` a su interior.

La ruta final ha de quedar:

```shell
/tmp/examen/de/farmeo/sources.list
```

Nombre del Script: `script_path_6.sh`

## Script 07

Haz un script que compruebe que el fichero `/tmp/examen/de/farmeo/sources.list` existe y si existe que muestre su contenido.

Nombre del Script: `script_path_7.sh`

## Script 08

Usando rutas **relativas**, haz un script que copie el fichero `sources.list` del ejercicio anterior a todos los directorios que lo preceden. Es decir que tiene que acabar en:

```shell
/tmp/
/tmp/examen/
/tmp/examen/de/
```

Nombre del Script: `script_path_8.sh`

## Script 09

Usando rutas **relativas**, haz un script que cree en el directorio personal del usuario los siguientes directorios (si no existen): 

* `pLevel1`
* `pLevel2`
* `pLevel3`
* `pLevel4/pLevel5`

Nombre del Script: `script_path_9.sh`

